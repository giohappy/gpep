"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

from setuptools import setup, find_packages
install_requires = []
setup(
    name='gpep',
    version='2.0.0',
    packages=['gpep_apps', 'gpep_apps.api', 'gpep_apps.status', 'gpep_apps.status.management', 'gpep_apps.status.management.commands', 
    'gpep_apps.config', 'gpep_apps.jobs', 'gpep_apps.jobs.management', 'gpep_apps.jobs.management.commands',
   'gpep_apps.mpas', 'gpep_apps.pepapi', 'gpep_apps.tests','gpep_apps.tests.api_tests', 'gpep_apps.tests.config_tests',
   'gpep_apps.tests.jobs_tests', 'gpep_apps.tests.mpas_tests', 'gpep_apps.static'],
    include_package_data=True,
    url='https://github.com/GitHubRGI/pep/tree/master/Modules/gpep',
    license='NA',
    author='Reinventing Geospatial, Inc.',
    author_email='',
    description='Gpep Module for NSG Mapproxy',
    install_requires=install_requires,
    classifiers=[
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 2.7",
        "Topic :: Internet :: WWW/HTTP :: WSGI",
        "Topic :: Scientific/Engineering :: GIS",
    ],
    zip_safe=False,
)
