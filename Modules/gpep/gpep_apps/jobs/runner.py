"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

from __future__ import absolute_import
import os
import shutil

from mapproxy.seed.gpep_seed import gpep_seed, TaskException

from celery import shared_task
from django.utils import timezone
from gpep_apps.config import gpepconfig

from gpep_apps.jobs import models
from gpep_apps.jobs.models import Task
from gpep_apps.jobs.export import export
from gpep_apps.pepapi import celery
from gpep_apps.pepapi.celery import app
from gpep_apps.api import named_locks
from gpep_apps.jobs import utils


def get_task(task_id):
    """
    Gets the task with the given id.
    :param task_id: the id to get the task for
    :return: the task
    """
    task = models.Task.objects.filter(pk=task_id).first()
    if not task:
        raise ValueError("Celery could not find a task with id {}".format(task_id))
    return task


def cancel_job(job_id):
    """
    Cancels the given job defined by job_id, cancelling all non-finished child tasks.

    rather than queueing a cancel command, immediately sends the cancel signal.
    :param job_id: Primary key of the job to be canceled.
    :return: boolean success
    """

    job = models.Job.objects.filter(pk=job_id).first()
    error_messages = []

    for task in models.Task.objects.filter(job=job_id):
        try:
            cancel_task(task.pk)
        except TaskException as e:
            error_messages.append(e.message)

    if error_messages:
        job.status = "failed"
        job.error_messages = error_messages
    else:
        job.status = "canceled"

    job.finished = True
    job.save()

    return True


def cancel_task(task_id):
    """
    Cancels the given task defined by task_id
    :param task_id: Primary key of the task to be canceled.
    :return: boolean success
    """
    task = get_task(task_id)
    with named_locks.getServiceLock(task.map_proxy_app_id):
        if not task.finished:
            # celery is always eager only if its always_eager settings is true
            def is_celery_always_eager():
                return hasattr(celery.settings, 'CELERY_ALWAYS_EAGER') and celery.settings.CELERY_ALWAYS_EAGER

            # if cancel_task is not being run in a unit test, then the celery_task_id should be a string of numbers
            # and letters.  If it's not, then throw an error.
            if task.celery_task_id is None and not is_celery_always_eager():
                task.status = 'failed'
                task.failure_message = 'Error: celery_task_id for Task {0} is None.'.format(str(task.id))
                task.save()

                raise TaskException(task.failure_message)
            else:
                app.control.revoke(task.celery_task_id, None, True)

                task.status = 'canceled'
                task.finished = True
                task.save(update_fields=['status', 'finished'])

    return True


@shared_task
def seed_package(service_yaml_file, seed_yaml_file, task_id, filename, lock_dir=None):
    """
    Launches a new seed process, with @seed_processes number threads.
    :param service_yaml_file: path to YAML file of the service to be outputted, or temp yaml if using one.
    :param seed_yaml_file: path to yaml file for the seed.yaml of this export (output of write_seed_package_file)
    :param task_id: id of task for tis seed job
    :param final path to output file (either .zip for compact cache, or .gpkg file for geopackage)
    :return: boolean success
    """
    max_repeat = gpepconfig.instance().max_repeat
    Task.objects.filter(id=task_id).update(started=True, start_date=timezone.now())
    task = get_task(task_id)

    try:
        gpep_seed(
            seeding_options_dict=seed_yaml_file,
            map_proxy_conf_dict=service_yaml_file,
            progress_file=task.progress_file,
            progress_log_file=task.progress_log_file,
            max_repeat=max_repeat,
            conf_dir=lock_dir
        )

        task.set_complete()
        extension = filename.split('.')[-1]
        if extension == 'zip':
            cache_to_zip = os.path.splitext(filename)[0]
            utils.remove_file_locks(cache_to_zip)
            utils.make_zipfile(filename, cache_to_zip)
            shutil.rmtree(cache_to_zip)

    except TaskException as e:
        task.status = 'failed'
        task.failure_message = e
        task.save()

    return True


@shared_task
def seed_cache(seed_processes, yaml_file, seed_yaml, task_id):
    """
    Launches a new seed process, with @seed_processes number threads.
    :param seed_processes: number of threads to use while seeding
    :param yaml_file: source yaml dict
    :param seed_yaml: seed yaml dict
    :param task_id: id of task model to store status in
    :return: boolean success
    """
    max_repeat = gpepconfig.instance().max_repeat
    yaml_file = yaml_file if yaml_file else {}
    Task.objects.filter(id=task_id).update(started=True, start_date=timezone.now())
    task = get_task(task_id)

    try:
        # Search through all of the caches that will be seeded and ensure that none of them are imported geopackages
        cache_set = task.map_proxy_app.get_caches(key__in=[seed for seed in seed_yaml['seeds']], cache__contains='type')
        for cache in cache_set:
            cache_config = cache.cache
            if cache_config['type'] == 'geopackage' and cache_config.get('imported') is True:
                raise TaskException('Services imported via geopackage do not support seeding or cleaning.')

        gpep_seed(
            seeding_options_dict=seed_yaml,
            map_proxy_conf_dict=yaml_file,
            progress_file=task.progress_file,
            progress_log_file=task.progress_log_file,
            concurrency=seed_processes,
            max_repeat=max_repeat,
        )

        task.set_complete()
    except TaskException as e:
        task.status = 'failed'
        task.failure_message = e
        task.save()

    return True


@shared_task
def clean_cache(yaml_file, clean_yaml, task_id):
    """
    Removes old tiles from the cache, using arguments stored in the clean seed .yaml file.
    :param yaml_file: name of source yaml file
    :param clean_yaml: name of the seed yaml file
    :param task_id: django task model associated with this job
    :return: boolean success (True)
    """
    Task.objects.filter(id=task_id).update(started=True, start_date=timezone.now())
    task = get_task(task_id)

    try:
        gpep_seed(
            seeding_options_dict=clean_yaml,
            map_proxy_conf_dict=yaml_file,
            progress_file=task.progress_file,
            progress_log_file=task.progress_log_file,
            cleaning=True,
        )

        task.set_complete()
    except TaskException as e:
        task.status = 'failed'
        task.failure_message = e
        task.save()

    return True


@shared_task
def export_service(map_proxy_app_id, task_id):
    """
    Exports the app with the given name
    :param app_name: the name of the service to export
    :param task_id: id of the this task
    :return: boolean success
    """
    Task.objects.filter(id=task_id).update(started=True, start_date=timezone.now())
    task = get_task(task_id)

    try:
        export(map_proxy_app_id, task)
        task.set_complete()
    except TaskException as e:
        task.status = 'failed'
        task.failure_message = e
        task.save()

    return True
