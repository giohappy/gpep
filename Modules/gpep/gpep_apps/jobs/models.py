"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

from __future__ import unicode_literals

import logging
import os
import pytz
import time
import sys

from datetime import datetime
from django.db import models
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from tzlocal import get_localzone

from gpep_apps.config import gpepconfig
from gpep_apps.jobs import utils
from gpep_apps.mpas.models import MapProxyApp, Layer
from django.db.models.signals import pre_delete
from django.dispatch import receiver

log = logging.getLogger('django')

JOB_TYPE_CHOICES = (
    ('seed', 'seed'),
    ('package', 'package'),
    ('clean', 'clean'),
    ('export', 'export')
)

STATUS_CHOICES = (
    ('inProgress', 'In Progress'),
    ('complete', 'Complete'),
    ('canceled', 'Canceled')
)

PACKAGE_TYPE_CHOICES = (
    ('ESRI_4326', 'ESRI_4326'),
    ('NSG_4326', 'NSG_4326'),
    ('NSG_3395', 'NSG_3395'),
    ('NW_3857', 'NW_3857'),
    ('STANDARD', 'STANDARD')
)

CACHE_FORMAT_CHOICES = (
    ('compact1', 'compact1'),
    ('compact2', 'compact2'),
    ('geopackage', 'geopackage')
)

SRS_TYPE_CHOICES = (
    ('EPSG:4326', 'EPSG:4326'),
    ('EPSG:3395', 'EPSG:3395'),
    ('EPSG:3857', 'EPSG:3857')
)


class Job(models.Model):
    """Stores all the parameters for creating a Job."""
    job_type = models.CharField(choices=JOB_TYPE_CHOICES, max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    started_at = models.DateTimeField(null=True, blank=True)
    started = models.BooleanField(default=False)
    finished = models.BooleanField(default=False)
    error_messages = models.TextField(blank=True)
    logs = models.TextField(blank=True)
    status = models.TextField(choices=STATUS_CHOICES, default=STATUS_CHOICES[0][0], max_length=255)
    result_filename = models.TextField(blank=True)

    def get_related_object(self):
        return getattr(self, str(self.job_type), None)

    def __str__(self):
        return '{}: {}'.format(self.id, self.job_type)

    def get_status(self):
        if not self.finished:
            tasks = Task.objects.filter(job=self.pk)
            self.finished = True
            self.status = "complete"
            # assume finished, anything not finished will automatically set it to false, set the status
            # and kick out (there has to be a way better way to do this)
            for task in tasks:
                if task.get_progress().get('percent') != 100 or task.finished is False:
                    self.finished = False
                    self.status = task.status
                    break
            self.save()
        return self.status

    def get_map_proxy_apps(self):
        ids = self.task_set.all().values('map_proxy_app__id')
        return MapProxyApp.objects.filter(id__in=ids)

    def get_max_zoom_if_consistent(self):
        task_set = self.task_set.all()
        if task_set.exists():
            compare_target = task_set.first().get_max_zoom_if_consistent()
            # If all tasks have the same max zoom levels, then return their common zoom level
            if all(task.get_max_zoom_if_consistent() == compare_target for task in task_set):
                return compare_target

        return None

    def get_min_zoom_if_consistent(self):
        task_set = self.task_set.all()
        if task_set.exists():
            compare_target = task_set.first().get_min_zoom_if_consistent()
            # If all tasks have the same min zoom levels, then return their common zoom level
            if all(task.get_min_zoom_if_consistent() == compare_target for task in task_set):
                return compare_target

        return None

    def delete(self):
        for task in self.task_set.all()[1:]:
            task.delete()
        super(Job, self).delete()


class BoxAndZoomJob(models.Model):
    """Stores parameters that are necessary for any jobs that involve a box and zoom levels."""
    min_x = models.FloatField()
    max_x = models.FloatField()
    min_y = models.FloatField()
    max_y = models.FloatField()
    min_zoom = models.IntegerField()
    max_zoom = models.IntegerField()


class Export(models.Model):
    """Stores fields necessary for an Export job."""
    job = models.OneToOneField(Job)


class Seed(models.Model):
    """Stores fields necessary for an Seed job."""
    job = models.OneToOneField(Job)


class Package(models.Model):
    """Stores fields necessary for an Package job."""
    job = models.OneToOneField(Job)
    package_type = models.CharField(choices=PACKAGE_TYPE_CHOICES, max_length=255)
    cache_format = models.CharField(default='geopackage', choices=CACHE_FORMAT_CHOICES, max_length=255)
    fetch_tiles = models.BooleanField(default=False)
    srs = models.CharField(choices=SRS_TYPE_CHOICES, max_length=255)


class Clean(models.Model):
    """Stores fields necessary for an Clean job."""
    job = models.OneToOneField(Job)
    date = models.DateField()


class Task(models.Model):
    """Stores all the data necessary for starting a task."""
    job = models.ForeignKey(Job)
    map_proxy_app = models.ForeignKey('mpas.MapProxyApp', blank=True, null=True, on_delete=models.SET_NULL)
    map_proxy_app_name = models.CharField(max_length=255)
    started = models.BooleanField(default=False)
    finished = models.BooleanField(default=False)
    progress_file = models.TextField(blank=True)
    progress_log_file = models.TextField(blank=True)
    start_date = models.DateTimeField(auto_now_add=True)
    status = models.TextField(choices=STATUS_CHOICES, default=STATUS_CHOICES[0][0], blank=True)
    celery_task_id = models.CharField(max_length=255, blank=True, null=True)
    # this field is now not just for geopackages. it's for whatever file type the task is creating (geopackage for
    # packaging, pep file for exporting)
    result_filename = models.CharField(blank=True, max_length=255)
    failure_message = models.TextField(blank=True)

    @classmethod
    def create(cls, job, map_proxy_app):
        """ Used to construct task objects and set their
        map_proxy_app_name. This is necessary because currently,
        after a mapproxyapp is deleted, the apps name can no
        longer be accessed from the app. However we still want to
        display the service name for related tasks that have
        already been completed for that service. """
        task = cls(job=job, map_proxy_app=map_proxy_app)
        task.map_proxy_app_name = task.map_proxy_app.name
        task.save()
        return task

    def get_progress(self):
        if self.finished:
            return {'percent': 100, 'eta': ''}
        elif os.path.isfile(self.progress_log_file):
            if self.status != 'failed':
                self.status = 'inProgress'
                self.save()
            with open(self.progress_log_file) as opened_file:
                lines = opened_file.readlines()
                if lines and len(lines) > 0:
                    split_line = lines[-1].split()
                    if hasattr(self.job, 'export'):
                        # the format string for export log file lines is '{0} tiles compressed ({1}%) ETA: {2}\n',
                        # (see export_service in runner.py)
                        progress_percentage = float(split_line[3][1:-2])
                    else:
                        progress_percentage = float(split_line[2].replace('%', ''))

                    try:
                        # using a try catch here because sometimes mapproxy logs 'NA' for the
                        # eta, and this can't be converted to a naive_date
                        naive_date = datetime.strptime(split_line[-1], '%Y-%m-%d-%H:%M:%S')
                        local_date = get_localzone().localize(naive_date)
                        utc_date = local_date.astimezone(pytz.utc)
                    except ValueError:
                        utc_date = ''
                    return {'percent': progress_percentage, 'eta': utc_date}

        return {'percent': 0, 'eta': ''}

    def set_complete(self):
        self.finished = True
        if self.status != "failed":
            self.status = "complete"
        self.save()

    def get_max_zoom_if_consistent(self):
        tasklayers_set = self.tasklayers_set.all()
        if tasklayers_set.exists():
            compare_target = tasklayers_set.first().max_zoom
            # If all layers have the same max zoom levels, then return their common zoom level
            if all(tasklayer.max_zoom == compare_target for tasklayer in tasklayers_set):
                return compare_target

        return None

    def get_min_zoom_if_consistent(self):
        tasklayers_set = self.tasklayers_set.all()
        if tasklayers_set.exists():
            compare_target = tasklayers_set.first().min_zoom
            # If all layers have the same min zoom levels, then return their common zoom level
            if all(tasklayer.min_zoom == compare_target for tasklayer in tasklayers_set):
                return compare_target

        return None

    def get_layers(self):
        layer_ids = self.tasklayers_set.all().values('layer__id')
        return Layer.objects.filter(id__in=layer_ids)

    def get_task_number_for_app(self):
        return [task for task in Task.objects.filter(map_proxy_app=self.map_proxy_app)].index(self)

    # @TODO: make this a model field
    def get_log_file_path(self):
        return os.path.join(gpepconfig.instance().log_dir, '{}_{}.log'.format(type(self), self.id))

    def get_logger(self):
        # @TODO: revise this to have another type other than 'task'
        logging.basicConfig(filename=self.get_log_file_path(),
                            filemode='a',
                            format='{} %(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s'.format(self.id),
                            datefmt='%H:%M:%S',
                            level=logging.INFO)
        return logging.getLogger('.'.join((
            self.map_proxy_app.name, self.job.job_type, str(self.get_task_number_for_app()))))

    # This gets called by the pre_delete signal, ensuring that it's called both when task is deleted directory directly
    # by calling Task.delete, and when it's deleted indirectly, such as when a parent of the task object is deleted
    def _delete(self):
        from gpep_apps.jobs import runner

        # Additional check to see if a celery task exists for the job before attempting to cancel it
        if not self.finished and runner.get_task(self.id).celery_task_id is not None:
            runner.cancel_task(self.id)

        # @TODO replace this with better logic.  Should have access to "is_task_fully_spun_down()" function
        # to check if the file is ready to be deleted.  this try-cache is convoluted

        # remove PEP and GeoPackage files from exporting and packaging jobs
        # keep trying until deletion is successful
        while(True):
            try:
                if os.path.exists(self.result_filename):
                    os.remove(self.result_filename)
            except WindowsError as e:
                log.info("Deleting file failed.  Probably being used by MapProxy still.  Trying again in 5 seconds.")
                log.info(str(e))
                time.sleep(5)
                continue
            except:
                log.error("Unexpected error: " + str(sys.exc_info()[0]))
                break
            finally:
                break


# A task is deleted, catch the delete signal for that task, and run that task's _delete() cleanup function
@receiver(pre_delete, sender=Task, dispatch_uid="task_delete")
def task_delete(sender, instance, using, **kwargs):
    instance._delete()


@receiver(post_save, sender=Task, dispatch_uid="get_log_files")
def get_log_files(sender, instance, created, **kwargs):
    """
    Generates the name of the log files used by Task
    :param sender: where the function was called from
    :param instance: the instance of task
    :param created: if the instance is created
    :param kwargs: any extra arguments
    """
    if not created:
        return
    log_dir = gpepconfig.instance().log_dir
    filename = '{0}_{1}_task_{2}_progress'.format(instance.map_proxy_app.name, instance.job.job_type, instance.id)
    instance.progress_file = utils.get_unique_filename(log_dir, filename, 'tmp')
    instance.progress_log_file = utils.get_unique_filename(log_dir, filename, 'log')
    try:
        with open(instance.progress_file, 'a'):
            pass
    except IOError:
        instance.progress_file = ''
    try:
        with open(instance.progress_log_file, 'a'):
            pass
    except IOError:
        instance.progress_log_file = ''
    instance.save()


class TaskLayers(BoxAndZoomJob):
    """Associates jobs.models.Task objects to mpas.models.Layer objects."""
    task = models.ForeignKey(Task)
    layer = models.ForeignKey('mpas.Layer')

    def get_bbox(self):
        return [self.min_x, self.min_y, self.max_x, self.max_y]

    def get_levels(self):
        return [x for x in range(self.min_zoom, self.max_zoom+1)]
