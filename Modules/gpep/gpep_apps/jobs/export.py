"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import os
import zipfile

from django.utils import timezone
from gpep_apps.config import gpepconfig
from gpep_apps.jobs.utils import get_unique_filename
from mapproxy.seed.gpep_seed import TaskException
from gpep_apps.jobs.models import Task, MapProxyApp


def run_export_job(export_model):
    """
    Runs the task for each task in the job
    :param export_model: jobs.models.Export model to pull the tasks from
    """
    export_model.status = "inProgress"
    for task in export_model.task_set.all():
        start_job(map_proxy_app=task.map_proxy_app, task=task)


def start_job(map_proxy_app, task):
    """
    Starts the given task
    :param map_proxy_app: the task's service
    :param task: jobs.models.Task model that specifies parameters
    :return: True
    """
    from gpep_apps.jobs.runner import export_service
    log = task.get_logger()
    log.debug("Starting export task for service {}".format(map_proxy_app.name))

    config = gpepconfig.instance()
    unique_filename = get_unique_filename(config.output_dir, map_proxy_app.name, 'pep')
    try:
        with open(unique_filename, 'a'):
            # open file to create it
            pass
    except IOError:
        log.error('pep file location taken by another process')
        unique_filename = ''
    task.result_filename = unique_filename
    task.save()

    celery_task = export_service.delay(map_proxy_app.id, task.id)
    Task.objects.filter(id=task.id).update(celery_task_id=celery_task.id)
    return True


def export(map_proxy_app_id, task):
    """
    Exports the map proxy app with the given name
    :param app_name: name of the map proxy app to export
    :param task: stores details about the task
    :return: True
    """
    log_file_line_length = 100
    config = gpepconfig.instance()
    total_tiles = 0

    app_name = MapProxyApp.objects.get(id=map_proxy_app_id).name

    try:
        for root, dirs, files in os.walk(os.path.join(config.cache_dir, app_name)):
            if 'tile_locks' in root:
                continue
            total_tiles += len(files)
        tiles_between_update = total_tiles / log_file_line_length if total_tiles > log_file_line_length else 1
    except (IOError, OSError) as e:
        e.message = 'Error walking the cache directory.'
        raise TaskException(e)

    try:
        # http://stackoverflow.com/questions/1855095/how-to-create-a-zip-archive-of-a-directory
        with zipfile.ZipFile(task.result_filename, 'w', zipfile.ZIP_DEFLATED, allowZip64=True) as export_file:
            app_yaml_name = '{}.yaml'.format(app_name)
            # This is writing the service yaml file to the pep file. Writestr is used for writing strings rather than
            # files.
            export_file.writestr(app_yaml_name, gpepconfig.make_generic_paths(os.path.join(config.app_dir,
                                                                                           app_yaml_name)))

            if total_tiles == 0:
                return True
            current_compressed_tiles = 0
            format_string = '{0} tiles compressed ({1}%) ETA: {2}\n'

            def update_export_log(finished=False):
                with open(task.progress_log_file, 'a') as progress_log:
                    if current_compressed_tiles == 0:
                        progress_log.write('Beginning exporting task on service {0}\nTotal tiles to compress: {1}\n'
                                           .format(app_name, total_tiles))
                    percentage = current_compressed_tiles / float(total_tiles) * 100

                    # if exporting a service while seeding tiles to service at same time, the percentage
                    # can go above 100, as tiles are being added after the initial tile count.  This ensures that the
                    # percentage can only get to 100 when the export process is fully complete
                    if percentage >= 100:
                        if finished:
                            percentage = 100
                        else:
                            percentage = 99

                    eta = 0 if int(percentage) == 0 else (timezone.now() - task.start_date) / int(percentage)
                    progress_log.write(format_string.format(current_compressed_tiles, percentage, eta))

            update_export_log()
            for root, dirs, files in os.walk(os.path.join(config.cache_dir, app_name)):
                if 'tile_locks' in root:
                    continue
                arc_root = root[len(config.cache_dir)+1:]
                for unzipped_file in files:
                    export_file.write(os.path.join(root, unzipped_file), os.path.join(arc_root, unzipped_file))
                    current_compressed_tiles += 1
                    if current_compressed_tiles % tiles_between_update == 0:
                        update_export_log()

            # now that exporting is completely finished, update the progress log to be complete with 100%
            update_export_log(finished=True)
        return True

    except (IOError, OSError) as e:
        e.message = 'Error creating and writing to the zip (pep) file.'
        raise TaskException(e)
