"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import os
import runner
import utils
import copy
import uuid
import re

from gpep_apps.config import gpepconfig
from gpep_apps.jobs.models import Task, TaskLayers
from gpep_apps.mpas import model_creator, models
from mapproxy.srs import SRS, get_epsg_num, merge_bbox
from gpep_apps.api import named_locks
from gpep_apps.jobs.utils import get_default_gpep_grid, get_default_esri_grid
from gpep_apps.api.gpep_exception import GPEPException

ESRI_4326, NSG_4326, NSG_3395, NW_3857, STANDARD = ['ESRI_4326', 'NSG_4326', 'NSG_3395', 'NW_3857', 'STANDARD']


def run_package_job(job_model):
    """
    Runs all of the tasks under a package job
    :param job_model: the job model to pull the tasks from
    """

    job_model.status = "inProgress"
    package_job = job_model.get_related_object()
    cache_format = package_job.cache_format

    extension = 'zip' if cache_format.startswith('compact') else 'gpkg'
    basename = 'job{job_id}_{hash}.{ext}'.format(job_id=job_model.id, hash=str(uuid.uuid4()), ext=extension)
    filename = os.path.join(gpepconfig.instance().output_dir, basename)

    job_model.result_filename = filename
    job_model.save()

    tasks = job_model.task_set.all()

    # This section will only be reached if no layers across all tasks in the job has invalid layer names
    for task in tasks:
        layers = task.get_layers()

        start_job(layers=layers, srs=package_job.srs, package_type=package_job.package_type,
                  cache_format=package_job.cache_format, task=task, filename=filename)


def merge_dict_lists(dict_list):
    '''
    Merges a list of dictionaries together into one dict.
    :param dict_list: List of dictionaries to be merged.
    :return: Single dictionary representing all dictionaries in dict_list merged together.
    '''
    def merge_dicts(dest, source):
        "merge source into dest"
        for section in dest:
            if type(dest[section]) is dict:
                dest[section].update(source[section])

    import copy
    merged = copy.deepcopy(dict_list[0])
    for dictionary in dict_list:
        merge_dicts(merged, dictionary)
    return merged


# "map_proxy_app" is not used and should be removed once all external usages are identified
def start_job(layers, srs, package_type, cache_format, task, filename):
    """
    Starts a new package task
    :param map_proxy_app: service to grab layer from
    :param layers: layers queryset
    :param srs: output srs list
    :param package_type: the type of package to package into
    :param cache_format: string representing format of output cache for package jobs.  Can be 'geopackage' or
                'compact'.
    :param task: the parent task
    :param filename: the name of the geopackage file to be written to
    :return: boolean success
    """
    log = task.get_logger()

    if(cache_format.startswith('compact') and len(layers) > 1):
        error_message = 'GPEP only supports packaging a single layer into a compact cache.  You have attempted ' \
                        'to package {} layers.'.format(len(layers))
        task.job.status = 'failed'
        task.status = 'failed'
        task.job.error_messages = error_message
        task.failure_message = error_message
        task.save()
        task.job.save()
        raise GPEPException(error_message)

    log.debug("Starting Package Job")

    ############################################################################

    def has_layers_in_this_job(app):
        """
        Return true if the given MapProxy app contains any layers relevant to the current job in the above
        start_job.  Return False otherwise.
        """
        return any(layer.map_proxy_app.name == app.name for layer in layers)

    def get_packaged_dicts(list_of_apps):
        """
        Packages and returns all the needed service dicts and seed yamls together
        :param list_of_apps: list of all relevant map proxy app objects
        :return: tuple containing the service dict list and the seed yaml list
        """
        # ...then, we have to process each mapproxy app with its respective set of layers separately
        for map_proxy_app in list_of_apps:
            # FIXME very slow, leads to slowdowns when starting jobs.  is this the best way?
            # make a deep copy of the mapproxy. we mess it up by adding caches, modifying layers and sources

            map_proxy_app = model_creator.create_map_proxy_app_from_dict(map_proxy_app.to_dict(), map_proxy_app.name)

            # Remove extraneous tile sources and caches (with disabled storage) from the app.
            # FIXME: This is a hack-y fix to make packaging work until we can
            # FIXME: refactor the packaging code to decouple it from
            # FIXME: app creation, etc.
            for layer in map_proxy_app.get_layers():
                layer.set_tile_sources(None)
                layer.save()

            # delete all reprojection caches.
            # Not good, what if a layer we want to seed is defined under a reprojection cache?
            # This happens when packaging a service imported via compact cache.

            # for cache in map_proxy_app.get_caches():
            #     if cache.disable_storage and cache.source_source is None:
            #         cache.delete()
            map_proxy_app.save()

            # get a list of only the layers pertinent to the current mapproxy app being looped on ...
            layers_in_this_app = [layer for layer in layers if map_proxy_app.get_layers(name=layer.name).first() and
                                  layer.map_proxy_app.name == map_proxy_app.name]

            # then build a dictionary which associates layer id's with the version of those layers in the deep copied
            # mapproxy app
            temp_layers = {layer.id: map_proxy_app.get_layers(name=layer.name).first() for layer in layers_in_this_app}

            service_dict = create_service_dict(map_proxy_app, package_type, cache_format, srs, temp_layers, task,
                                               filename)

            if 'layers' in service_dict:
                del service_dict['layers']

            bboxes_per_layer = [TaskLayers.objects.filter(task=task, layer_id=layer_id)[0].get_bbox()
                                for layer_id in temp_layers.keys()]
            combined_bbox = reduce(merge_bbox, bboxes_per_layer)
            service_name_prepender = StringPrepender(map_proxy_app.name)
            if package_type == 'NW_3857':
                service_dict['grids'][service_name_prepender.prepend(get_default_gpep_grid('3857'))]['bbox'] = \
                    list(SRS(4326).transform_bbox_to(SRS(3857), combined_bbox))
            elif package_type == 'ESRI_4326':
                service_dict['grids'][service_name_prepender.prepend(get_default_esri_grid('4326'))]['bbox'] = \
                    list(combined_bbox)

            seed_yaml_file = create_seed_package_dict(temp_layers, task)

            # push the two generated yaml dicts, so we can zip them all together later
            service_dict_list.append(service_dict)
            seed_yaml_file_list.append(seed_yaml_file)

        return service_dict_list, seed_yaml_file_list

    #######################################################################

    service_dict_list = []
    seed_yaml_file_list = []

    # get a list of mapproxy apps with layers relevant to this job...
    map_proxy_apps = [app for app in models.MapProxyApp.objects.filter(is_copy=False) if has_layers_in_this_job(app)]

    service_dict_list, seed_yaml_file_list = get_packaged_dicts(map_proxy_apps)

    merged_service_dict = merge_dict_lists(service_dict_list)
    merged_seed_yaml_dict = merge_dict_lists(seed_yaml_file_list)

    # not doing obj version for seed_package (not necessary)
    celery_task = runner.seed_package.delay(merged_service_dict, merged_seed_yaml_dict, task.id, filename)
    Task.objects.filter(id=task.id).update(celery_task_id=celery_task.id)
    return True


def create_service_dict(map_proxy_app, package_type, cache_format, srs, layers, task, filename):
    """
    Creates a service yaml file for exporting the given layers (in the given package type/srs)
    to the correct geopackage output type. overwrites existing file, so make sure a temp file is used.
    :param map_proxy_app: map proxy app to create the service dict from
    :param package_type: profile of geopackage to export
    :param srs: srs to output geopackage in
    :param layers: layers to include in the geopackage
    :param task: the parent task
    :param filename: the name of the geopackage file to be written to
    :return: dict with all the services
    """
    def add_service_name_guards(in_service_dict):
        """
        Given an input service dict, this prepends keys throughout the dictionary with the name of the Map Proxy App,
        which avoids namings conflicts between sources, grids, caches, and layers on different mapproxy apps.
        :param in_service_dict: Input dictionary/yaml representing a service configuration
        :return: The new service_dict where the keys are prepended.
        """
        service_name_prepender = StringPrepender(map_proxy_app.name)

        # add service name to the beginning of keys to avoid naming conflicts
        out_service_dict = copy.deepcopy(in_service_dict)
        sections = ['grids', 'caches', 'sources']
        for section in sections:
            out_service_dict[section] = {}

        for section in sections:
            if section in in_service_dict:
                for key in in_service_dict[section]:
                    new_key = service_name_prepender.prepend(key)
                    out_service_dict[section][new_key] = in_service_dict[section][key]

        for key in out_service_dict['sources']:
            if 'grid' in out_service_dict['sources'][key]:
                out_service_dict['sources'][key]['grid'] = service_name_prepender.prepend(
                    out_service_dict['sources'][key]['grid'])

        for key in out_service_dict['caches']:
            if 'grids' in out_service_dict['caches'][key]:
                out_service_dict['caches'][key]['grids'] = \
                    [service_name_prepender.prepend(grid) for grid in out_service_dict['caches'][key]['grids']]

            if 'sources' in out_service_dict['caches'][key]:
                out_service_dict['caches'][key]['sources'] = \
                    [service_name_prepender.prepend(source) for source in
                     out_service_dict['caches'][key]['sources']]

        return out_service_dict

    # generates new caches and adds them to map_proxy_app
    _generate_caches(map_proxy_app, layers, package_type, cache_format, srs, task, filename)

    map_proxy_app.get_services().wms = None
    map_proxy_app.get_services().wmts = None
    map_proxy_app.get_services().tms = None
    map_proxy_app.save()

    service_dict = map_proxy_app.to_dict()
    return add_service_name_guards(service_dict)


def _generate_caches(map_proxy_app, layers, package, cache_format, srs, task, filename):
    """
    Enables a cache for each source with the provided SRS
    :param map_proxy_app: base app to generate caches for
    :param layers: layers to generate caches for
    :param package: GeoPackage package type, accepts 'NW','NSG','STANDARD'.
    :param cache_format: Specifies cache format to create.  'compact' or 'geopackage'
    :param srs: SRS to package to. in the form of 'EPSG:4326'.
    :param task: the parent task
    :param filename: the name of the geopackage file to be written to
    """
    # If srs is a list, gets the least of all of them
    srs = sorted(srs)[0] if isinstance(srs, list) and srs else srs

    for orig_id, layer in layers.iteritems():
        source = layer.get_source()
        if isinstance(source, models.Cache):
            base_source_name = source.key.rsplit('_cache', 1)[0]
        elif isinstance(source, models.SourceCommons):
            base_source_name = source.key
        else:
            assert source is None
            continue

        # set package name
        cache_name = '{}_package'.format(base_source_name)

        # set table name
        if package == NW_3857:
            table_name = 'tiles'
        else:
            table_name = str(source.key)

        if package == ESRI_4326:
            grid = utils.get_default_esri_grid(str(get_epsg_num(srs)))
        else:
            grid = utils.get_default_gpep_grid(str(get_epsg_num(srs)))

        shortname = os.path.basename(filename)

        task.result_filename = filename
        task.save()

        taskLayer = TaskLayers.objects.filter(task=task, layer_id=orig_id)[0]
        bbox = taskLayer.get_bbox()
        levels = taskLayer.get_levels()
        cache = create_cache(map_proxy_app,
                             source,
                             grid,
                             package,
                             cache_format,
                             table_name,
                             bbox,
                             cache_name,
                             shortname,
                             levels)
        layer.set_source(cache)


def create_cache(map_proxy_app, source, grid, package, cache_format, table_name, bbox, cache_name, filename, levels):
    """
    Generates a cache with the given parameters
    :param map_proxy_app: the app to create the cache inside of
    :param source: source for the new cache
    :param grid: grid name
    :param package: package type
    :param cache_format: cache format.  'compact' or 'geopackage'
    :param table_name: table name
    :param bbox: bounding box
    :param cache_name: name of the cache to use
    :param filename: the filename to go into cache.cache
    :param levels: list of levels to cache
    :return: models.Cache object filled in with the given parameters
    """

    # ensure all parameters are padded with service name
    service_name_prepender = StringPrepender(map_proxy_app.name)
    table_name = service_name_prepender.prepend(table_name)
    cache_name = service_name_prepender.prepend(cache_name)
    grid = service_name_prepender.prepend(grid)

    inner_cache = {
        'table_name': table_name,
        'profile':    package,
        'bbox':       bbox,
        'bbox_srs':  'EPSG:4326',
        'levels':     False,
        'limited_levels': levels
    }

    if cache_format == 'geopackage':
        inner_cache.update({
            'type':       'geopackage',
            'directory':  gpepconfig.instance().output_dir,
            'filename': filename
        })
    elif cache_format.startswith('compact'):
        full_filepath = os.path.join(gpepconfig.instance().output_dir, filename)
        inner_cache.update({
            'type': 'compact',
            'version': int(cache_format[-1]),  # 'compact1' -> 1, 'compact2' -> 2
            'directory': os.path.splitext(full_filepath)[0]  # remove zip extension for folder name
        })
    else:
        raise GPEPException('Invalid package format: {}'.format(cache_format))

    cache = dict(sources=[source.key], format='image/png', request_format='image/png',
                 meta_size=[1, 1], grids=[grid], cache=inner_cache)
    return model_creator.create_cache(app=map_proxy_app, key=cache_name, value=cache)


def create_seed_package_dict(layers, task):
    """
    Creates a seed yaml file used for seed tasks.
    :param layers: the layers to be seeded (if None, defaults to all) (type: list)
    :return: boolean success
    """
    coverages = {}
    seeds = {}

    for orig_id, layer in layers.iteritems():
        service_name_prepender = StringPrepender(layer.map_proxy_app.name)

        source = layer.get_source_name()
        coverage_name = service_name_prepender.prepend(layer.generate_coverage_name())
        coverages[coverage_name] = {
            'bbox': TaskLayers.objects.filter(task=task, layer_id=orig_id)[0].get_bbox(),
            'srs': 'EPSG:4326'
        }
        seed_name = layer.generate_seed_name()
        seeds[seed_name] = {
            'caches': [source],
            'levels': TaskLayers.objects.filter(task=task, layer_id=orig_id)[0].get_levels(),
            'coverages': [coverage_name]
        }

    return {'seeds': seeds, 'coverages': coverages}


class StringPrepender:
    """
    Used to prepend a string with another string.  Has the capability to avoid duplicating the prefix, such that if
    you did attempted to prepend something which already has the desired prefix, an additional prefix will not be added.
    """

    def __init__(self, prefix, disallow_duplicate_prepending=True, separator="__"):
        """
        :param prefix: String prefix to use
        :param disallow_duplicate_prepending: If True, don't allow a prefix to be added twice.
        :param separator: String to use as separator between base string and prefix.
        """
        self.prefix = prefix + separator
        self.disallow_duplicate_prepending = disallow_duplicate_prepending

    def prepend(self, string):
        """
        Returns string with the prefix attached.
        :param string: Base string to use.
        :return: Will return the inputed string with a prefix attached attached at the beginning, which was specified
                 upon creation of the StringPrepender object.
        """
        if self.disallow_duplicate_prepending and string.startswith(self.prefix):
            return string
        else:
            return '{}{}'.format(self.prefix, string)
