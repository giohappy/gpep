"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import logging
import os
import re
import shutil
import sqlite3
import uuid
import zipfile

from io import BytesIO
from requests import ConnectionError
from xml.etree.ElementTree import ParseError

from django.core.cache import cache as django_cache

from gpep_apps.api.gpep_exception import GPEPException
from gpep_apps.config import gpepconfig
from gpep_apps.jobs import utils
from gpep_apps.mpas import model_creator, models

from mapproxy.cache import compact
from mapproxy.client.http import HTTPClientError
from mapproxy.script.conf.utils import download_capabilities
from mapproxy.srs import SRS
from mapproxy.util.ext.wmsparse import parse_capabilities


SOURCE_ON_ERROR_DICT = {404: {'response': 'transparent', 'cache': True},
                        'other': {'response': 'transparent', 'cache': True}}


def get_map_proxy_error_file_path():
    """
    Generates a new temporary file path and creates it
    :return: a path to a new file that has been created
    """
    path = utils.get_unique_filename(gpepconfig.instance().log_dir)
    with open(path, 'a+'):
        pass
    return path


def check_and_update_projections(map_proxy_app, supported_srs, log):
    """
    Ensures that all of the sources only support projections that we support
    :param map_proxy_app: the service to update the projections for
    :param supported_srs: the srses that are supported
    :param log: logger
    """
    if map_proxy_app.type == 'wms' or map_proxy_app.type == 'wmts':
        for source in map_proxy_app.get_sources():
            projections = source.supported_srs if map_proxy_app.type == 'wms' else models.Grid.objects.filter(
                map_proxy_app=map_proxy_app, key=source.grid).values_list('srs', flat=True)
            projections = list(set(projections).intersection(supported_srs))
            if projections:
                if map_proxy_app.type == 'wms':
                    source.supported_srs = projections
                    source.save()
            else:
                os.remove(map_proxy_app.file_location)
                log.error('The {} service does not contain one of the compatible projections {}'
                          .format(map_proxy_app.name, supported_srs))
                raise ValueError('{0} does not contain a compatible projection'.format(map_proxy_app.name))


def add_geopackage_service(name, geopackage):
    """
    Creates a map_proxy_app from the given geopackage with the given name
    :param name: name to give to the new model
    :param geopackage: data to create the map proxy app from
    :return: the created model
    """
    cache_dir = os.path.join(gpepconfig.instance().cache_dir, name)
    if not os.path.exists(cache_dir):
        os.mkdir(cache_dir)

    # Write the database out to disk
    filename = os.path.join(cache_dir, '{}.gpkg'.format(name))
    with open(filename, 'wb+') as f:
        for chunk in geopackage.chunks():
            f.write(chunk)

    # Find the tables in the database
    db = sqlite3.connect(filename)

    cur = db.execute('SELECT table_name, data_type, srs_id FROM gpkg_contents')
    table_names = cur.fetchall()

    # Generates the yaml path using the name given to the file
    yaml_path = os.path.join(gpepconfig.instance().app_dir, '{}.yaml'.format(name))

    # Creates the dict to create the yaml model from
    caches = {}
    layers = []
    grids = {}
    folder_name, geopackage_name = os.path.split(filename)
    for table_name, data_type, srs_id in table_names:
        try:
            cur = db.execute('SELECT srs_id, min_x, min_y, max_x, max_y FROM gpkg_tile_matrix_set '
                             'WHERE table_name = "{}"'.format(table_name))
        except db.OperationalError as e:
            raise GPEPException(e.message)

        srs_id, min_x, min_y, max_x, max_y = cur.fetchone()
        bbox = [min_x, min_y, max_x, max_y]

        try:
            cur = db.execute('SELECT min_x, min_y, max_x, max_y FROM gpkg_contents '
                             'WHERE table_name = "{}"'.format(table_name))
        except db.OperationalError as e:
            raise GPEPException(e.message)

        min_x, min_y, max_x, max_y = cur.fetchone()
        coverage_bbox = [min_x, min_y, max_x, max_y]

        cur = db.execute('SELECT zoom_level, pixel_x_size FROM gpkg_tile_matrix '
                         'WHERE table_name = "{}" ORDER BY zoom_level'.format(table_name))
        zoom_level, pixel_x_size = cur.fetchone()
        custom_grid_name = '{}_grid'.format(table_name)

        min_res = pixel_x_size * (2 ** zoom_level)

        # ensures the bbox coordinates utilize EPSG:4326 (latitude longitude)
        if srs_id != 4326:
            bbox = SRS(srs_id).transform_bbox_to(SRS(4326), bbox)
            bbox = list(bbox)  # binding errors if bbox passsed as tuple
            # necessary to convert overall coverage to 4326 for frontend
            coverage_bbox = SRS(srs_id).transform_bbox_to(SRS(4326), coverage_bbox)
            coverage_bbox = list(coverage_bbox)

        grids[custom_grid_name] = {
            'srs': 'EPSG:{}'.format(srs_id),
            'bbox': bbox,
            'bbox_srs': 'EPSG:4326',
            'min_res': min_res,
            'origin': 'nw'
        }

        base_name = '{}'.format(table_name)
        cache_name = '{}_cache'.format(base_name)
        layer_name = '{}_layer'.format(base_name)
        caches[cache_name] = {
                'grids': [custom_grid_name],
                'cache': {
                    'type': 'geopackage',
                    'filename': geopackage_name,
                    'table_name': table_name,
                    'directory': folder_name,
                    'coverage_bbox': coverage_bbox,
                    'bbox_srs': 'EPSG:4326',
                    'imported': True,
                }
            }
        layers.append({
            'name': layer_name,
            'title': layer_name,
            'source': cache_name
        })
    model_dict = {
        'layers': layers,
        'caches': caches,
        'sources': [],
        'grids': grids,
        'base': gpepconfig.instance().template_file
    }

    # Creates the yaml model from the created dict.
    model = model_creator.create_map_proxy_app_from_dict(model_dict, name, path=yaml_path, pull_in_base=True)
    model.save_to_disk()
    return model


def import_compact_cache_zip(service_name, compact_zip_path):
    def locate_compact_cache_root(path):
        """
        Given a folder with a compact cache somewhere inside, return a path to the root point of the compact
        cache.  This will be the location where the L00, L01, Lxx... folders are.
        """
        for root, dirs, files in os.walk(path):
            for file in files:
                if file.endswith('.bundle'):
                    cache_root = os.path.join(root, file, os.pardir, os.pardir)
                    return os.path.realpath(cache_root)
        raise GPEPException('No root located for compact cache in {}'.format(path))

    def locate_file(path, filename):
        for root, dirs, files in os.walk(path):
            if filename in [file.lower() for file in files]:
                return os.path.realpath(os.path.join(root, filename))
        return False

    log = logging.getLogger(service_name)
    config = gpepconfig.instance()

    # extract compact cache into temporary folder map_cache folder where the other caches are
    cache_dir = os.path.join(gpepconfig.instance().cache_dir, service_name)
    cache_dir_all_layers = os.path.join(cache_dir, '_alllayers')
    tmp_cache_dir = os.path.join(cache_dir, str(uuid.uuid4()))
    if not os.path.exists(cache_dir):
        os.mkdir(cache_dir)
    with zipfile.ZipFile(compact_zip_path, 'r', allowZip64=True) as cache_zip:
        cache_zip.extractall(tmp_cache_dir)

    # after cache extracted to tmp folder, move over the bundle files to actual cache folder
    os.renames(locate_compact_cache_root(tmp_cache_dir), cache_dir_all_layers)
    # ... also move over conf.cdi and conf.xml files
    try:
        shutil.move(locate_file(tmp_cache_dir, 'conf.xml'), cache_dir)
    except IOError:
        log.warn('No conf.xml found while importing compact cache.')
    try:
        shutil.move(locate_file(tmp_cache_dir, 'conf.cdi'), cache_dir)
    except IOError:
        log.warn('No conf.cdi found while importing compact cache.')
    shutil.rmtree(tmp_cache_dir)

    # clear existing mapproxy app if exists, create yaml path
    models.MapProxyApp.objects.filter(name=service_name).delete()
    yaml_path = os.path.join(config.app_dir, '{}.yaml'.format(service_name))

    # Setup map proxy app creation parameters
    base = config.template_file

    # set primary grid / custom parsed grid
    custom_grids = {}
    conf_xml = locate_file(cache_dir, 'conf.xml')
    if conf_xml:
        # parse custom grid from conf.xml if there is one
        parsed_conf_xml = compact.ConfXMLParser(conf_xml)
        PARSED_GRID_NAME = 'PARSED_GRID_FROM_CONF_XML'
        custom_grids = {
            PARSED_GRID_NAME: parsed_conf_xml.get_grid()
        }
        primary_grid = PARSED_GRID_NAME
        compact_cache_version = parsed_conf_xml.get_compact_version()
    else:
        primary_grid = 'NSG_GPEP_DEFAULT_EPSG4326'
        compact_cache_version = 1
        log.warn("No conf.xml found while importing cache.  Assuming {} grid.".format(primary_grid))
        log.warn("No conf.xml found while importing cache.  Assuming compact cache version 1.")

    cache_options = {
        'base_dir': cache_dir,
        'primary_grid': primary_grid,
        'no_grids': True,  # use base_dir as actual location of where cache is
        'primary_storage_enabled': True,
        'cache_type': 'compact',
        'image_format': 'mixed',
        'request_image_format': 'image/png',
        'compact_cache_version': compact_cache_version,
        'custom_grids': custom_grids
    }

    map_proxy_app = model_creator.create_map_proxy_app_from_gvs_compact_cache_v1(service_name=service_name,
                                                                                 yaml_path=yaml_path,
                                                                                 base=base,
                                                                                 cache_options=cache_options)

    print "The {0} service will begin caching tiles on demand.".format(service_name)

    map_proxy_app.save()
    map_proxy_app.save_to_disk(temp=False)

    django_cache.delete('services')
    return map_proxy_app


def add_map_proxy_app(service_name, service_url, cache_service=False):
    """
    Adds a new map proxy app instance and conf file using the given service name and url. If any map proxy apps with the
    same name are already running, then delete those app(s) first.

    :param service_name: Name of the service to add as a map proxy app.
    :param service_url: URL of the service to add as a map proxy app.
    :param cache_service: Optional, defaults to False. Whether or not the given service should be cached by the created
                          app. This is required to be True if the given service's type is WMTS.

    :return: A map proxy app model generated from the given information.
    """
    log = logging.getLogger(service_name)
    config = gpepconfig.instance()

    cap = get_and_parse_capabilities(service_url, log, (not config.strict_validation))

    # WMTS service may not be added unless caching is enabled
    if cache_service is False and cap.service == 'WMTS':
        error_message = 'WMTS services must have caching enabled.'
        log.error(error_message)
        raise ValueError(error_message)

    models.MapProxyApp.objects.filter(name=service_name).delete()

    yaml_path = os.path.join(config.app_dir, '{}.yaml'.format(service_name))

    # Setup map proxy app creation parameters
    base = config.template_file

    if config.cache_type == 'compact':
        primary_grid = utils.get_default_esri_grid(config.default_projection)
    else:
        primary_grid = utils.get_default_gpep_grid(config.default_projection)

    source_options = {
        'primary_grid': primary_grid,
        'on_error_dict': SOURCE_ON_ERROR_DICT
    }

    cache_options = {
        'base_dir': os.path.join(config.cache_dir, service_name),
        'primary_grid': primary_grid,
        'primary_storage_enabled': cache_service,
        'cache_type': config.cache_type,
        'image_format': config.cache_image_format,
        'compact_cache_version': config.compact_cache_version,
        'file_cache_dir_layout': config.file_cache_dir_layout
    }

    map_proxy_app = model_creator.create_map_proxy_app_from_capabilities(capabilities=cap, service_name=service_name,
                                                                         service_url=service_url, yaml_path=yaml_path,
                                                                         base=base, source_options=source_options,
                                                                         cache_options=cache_options)

    if cache_service:
        utils.create_cache_folder(map_proxy_app, log)
        print "The {0} service will begin caching tiles on demand.".format(service_name)

    map_proxy_app.save()
    map_proxy_app.save_to_disk(temp=False)

    django_cache.delete('services')
    return map_proxy_app


def get_and_parse_capabilities(service_url, log, insecure_download=False):
    """
    Download (if web source) or open (if file source) and parse a service capabilities document located at the given
    service URL.

    :param service_url: The location of the web source/file to download/open and parse.
    :param log: The logger to write log entries with.
    :param insecure_download: Optional, defaults to False. Whether or not the capabilities document should be downloaded
                              securely or not, if service url point to web source. Not applicable if service url points
                              to a file.

    :return: A parsed capabilities object.
    """
    # download or open the capabilities file
    try:
        if service_url.startswith(('http://', 'https://')):
            cap_file = download_capabilities(service_url, insecure_download)
        else:
            cap_file = open(service_url, 'rb')

        cap_doc = cap_file.read()

    except Exception as ex:
        # MapProxy's http exception is caught and a ConnectionError exception is raised to
        # signal to the view that connectivity issues exist between the service and MapProxy.
        # If the response_code type is None, then the service could not be reached.
        if type(ex) is HTTPClientError and HTTPClientError(ex).response_code is None:
            raise ConnectionError(ex)
        else:
            log.error(ex)
            return

    # parse the capabilities file into a dict
    try:
        cap = parse_capabilities(BytesIO(cap_doc))

    except (ParseError, ValueError) as ex:
        log.error(ex)
        log.error(cap_doc[:1000] + ('...' if len(cap_doc) > 1000 else ''))
        cap = None

    finally:
        cap_file.close()

    return cap


def check_for_pep_compliance(zip_file):
    """
    Checks the given zip file for pep compliance
    :param zip_file: the zip file to check
    :return: tuple with the name of the folder and the name of the yaml file
    """
    folder_names = set()
    file_names = []
    for file in zip_file.filelist:
        if '\\' in file.filename or '/' in file.filename:
            path = os.path.relpath(file.filename)
            folder_names.add(path.split(os.sep)[0])
        else:
            file_names.append(file.filename)
    if len(folder_names) != 1:
        raise GPEPException('PEP file must have exactly one directory for a cache')
    if len(file_names) != 1:
        raise GPEPException('PEP file is malformed. There can only be one .yaml file in the root.')
    return folder_names.pop(), file_names[0]


def import_pep(service_name, pep_file):
    """
    Imports the given pep file
    :param service_name: the name to use as the name of the new service
    :param pep_file: the pep file to import
    :return: model created from the pep file
    """
    app_dest_path = os.path.join(gpepconfig.instance().app_dir, '{}.yaml'.format(service_name))

    move_cache_folder = False  # necessary if the folder name inside the zip already is in the cache
    folder_dest_path = os.path.join(gpepconfig.instance().cache_dir, service_name)
    app_source_path, folder_name, folder_source_path = None, None, None
    with zipfile.ZipFile(pep_file, 'r', allowZip64=True) as zip_file:
        folder_name, file_name = check_for_pep_compliance(zip_file)

        if os.path.exists(os.path.join(gpepconfig.instance().cache_dir, folder_name)):
            # if cache already exists with the same name as the name of the folder in the zip file
            folder_source_path = utils.get_unique_folder(gpepconfig.instance().cache_dir)
            move_cache_folder = True
        else:
            # the folder is just placed in the cache directory
            folder_source_path = gpepconfig.instance().cache_dir

        for file_path in zip_file.filelist:
            path = os.path.relpath(file_path.filename)
            if os.sep in path or '.' not in path:
                file_location = os.path.join(folder_source_path, path)
                if not os.path.exists(os.path.split(file_location)[0]):
                    os.makedirs(os.path.split(file_location)[0])
                zip_file.extract(file_path, folder_source_path)
            else:
                app_source_path = zip_file.extract(file_path, gpepconfig.instance().temp_dir)
    if move_cache_folder or folder_name != service_name:
        shutil.move(os.path.join(folder_source_path, folder_name), folder_dest_path)
        if move_cache_folder:
            shutil.rmtree(folder_source_path)
    with utils.open_file(app_dest_path, 'a') as app_out:
        with utils.open_file(app_source_path, 'r') as app_in:
            for line in app_in:
                # need to change the service_name in all the file paths.
                line = re.sub(r'<GPEP:HOME>/map_cache/[0-9a-zA-Z_]+/', '<GPEP:HOME>/map_cache/{}/'.format(
                    service_name), line)
                line = re.sub(r'<GPEP:HOME>\\map_cache\\[0-9a-zA-Z_]+\\', '<GPEP:HOME>\\\\map_cache\\\\{}\\\\'.format(
                    service_name), line)
                app_out.write(line.replace('<GPEP:HOME>', gpepconfig.ConfigModelCreator().GPEP_HOME))
    os.remove(app_source_path)
    return model_creator.create_map_proxy_app_from_file(app_dest_path, pull_in_base=False)
