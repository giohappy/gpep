"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import os
import shutil

from gpep_apps.config import gpepconfig
from gpep_apps.jobs.utils import get_default_gpep_grid
from django.core.cache import cache as django_cache


def disable_caching(map_proxy_app):
    """
    Disables the caches for the given map proxy app.
    :param map_proxy_app: the service to disable the cache for.
    """
    for cache in map_proxy_app.get_caches():
        cache.set_disable_storage(True)

    map_proxy_app.save()
    map_proxy_app.save_to_disk()


def enable_caching(map_proxy_app):
    """
    Enables the caches for the given map proxy app if it has a WMTS service.
    :param map_proxy_app: the service to enable the caches for.
    """
    if map_proxy_app.get_services().wmts:
        default_projection = get_default_gpep_grid(gpepconfig.instance().default_projection)
        for cache in map_proxy_app.get_caches():
            if default_projection in cache.grids:
                cache.set_disable_storage(False)

        map_proxy_app.save()
        map_proxy_app.save_to_disk()


def cleanup_file(folder, file_pattern, map_proxy_app):
    """
    Utility method delete files in the folder with the given file_pattern.
    :param folder: folder to clean up
    :param file_pattern: file pattern to delete
    :param map_proxy_app: the service that generated the file
    """
    # only format the string if it needs formatting
    if '{}' in file_pattern or '{0}' in file_pattern:
        file_pattern = file_pattern.format(map_proxy_app.name)
    try:
        os.remove(os.path.join(folder, file_pattern))
    except OSError:
        pass
        # @TODO: fix this. figure out if this is an issue when deleting new services

        # raise APIException('Unable to clean the {0} file'.format(file_pattern))
        # self.log.exception('Unable to clean the {0} file'.format(file_pattern))


def delete_cache_folder(map_proxy_app, log):
    """
    Removes cache folder from cache directory
    :param map_proxy_app: the service's cache folder that you want to delete
    :param log: logger
    :return: boolean success
    """
    cache_folder = os.path.join(gpepconfig.instance().cache_dir, map_proxy_app.name)
    if not os.path.exists(cache_folder):
        return True
    try:
        # ignore_errors=True makes rmtree more reliable, ignore lccks/safeguards, deletes no matter what
        shutil.rmtree(cache_folder, ignore_errors=True)
        return True
    except OSError:
        log.exception('The {0} cache folder could not be deleted.'.format(map_proxy_app.name))
        return False


def delete_map_proxy_app(map_proxy_app, log):
    """
    Deletes map_proxy_app, as well as all associated caches and .yaml files.
    :param map_proxy_app: service to delete
    :param log: logger
    :return: True if successful
    """
    log.debug('Deleting Map Proxy App')

    map_proxy_app.cancel_all_tasks()

    config = gpepconfig.instance()

    cleanup_file(config.clean_seed_dir, '{}_clean.yaml', map_proxy_app)
    cleanup_file(config.clean_seed_dir, '{}_seed.yaml', map_proxy_app)
    cleanup_file(config.clean_seed_dir, '{}_pinfo.yaml', map_proxy_app)
    cleanup_file(config.temp_dir, '{}_temp.yaml', map_proxy_app)
    cleanup_file(config.log_dir, '{}_clean.log', map_proxy_app)
    cleanup_file(config.log_dir, '{}_seed.log', map_proxy_app)
    cleanup_file(config.log_dir, '{}_progress', map_proxy_app)
    cleanup_file(config.log_dir, '{}_progress.tmp', map_proxy_app)
    cleanup_file(config.output_dir, '{}.pep', map_proxy_app)

    delete_cache_folder(map_proxy_app, log)
    map_proxy_app.delete()
    # @TODO: implement gpkg_sqlserver deletions
    django_cache.delete('services')
    return True
