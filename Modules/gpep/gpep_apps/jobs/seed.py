"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import os
import runner

from gpep_apps.config import gpepconfig
from gpep_apps.jobs.models import Task, TaskLayers
from gpep_apps.mpas import model_creator
from gpep_apps.api import named_locks


def run_seed_job(job_model):
    """
    Runs the seed tasks for each task in the model
    :param job_model: the model to pull the tasks from
    """

    job_model.status = "inProgress"
    seed_job = job_model.get_related_object()
    for task in job_model.task_set.all():
        with named_locks.getServiceLock(task.map_proxy_app.id):
            start_job(
                map_proxy_app=task.map_proxy_app, layers=task.get_layers(),
                num_cache_processes=2, task=task)


def start_job(map_proxy_app, layers, num_cache_processes, task):
    """
    Seeds the cache and writes the seed file with the parameters entered into the dialog box.
    :param map_proxy_app: the service whose cache is being seeded
    :param num_cache_processes: the number of cache processes that should be run for this seeding
    :param task: the parent task
    :return: boolean success
    """
    log = task.get_logger()
    log.debug("Starting Seed Job")
    map_proxy_app = model_creator.create_map_proxy_app_from_dict(map_proxy_app.to_dict(), map_proxy_app.name)
    temp_layers = {layer.id: map_proxy_app.get_layers(name=layer.name).first() for layer in layers}

    seed_yaml = create_seed_dict(temp_layers, task)
    mapproxy_yaml = create_yaml_dict(map_proxy_app)

    map_proxy_app.save_to_disk()

    celery_task = runner.seed_cache.delay(num_cache_processes, mapproxy_yaml, seed_yaml, task.id)
    Task.objects.filter(id=task.id).update(celery_task_id=celery_task.id)

    return True


def create_yaml_dict(map_proxy_app):
    """
    Creates a temporary yaml for seeding, exporting, etc.
    :param map_proxy_app: app to create temp dict from
    :return: temp yaml as a dict
    """
    for cache in map_proxy_app.get_caches():
        if not cache.disable_storage:
            if not cache.cache:
                cache.cache = {}
            cache.cache['directory'] = os.path.join(gpepconfig.instance().cache_dir, map_proxy_app.name, cache.key)
            cache.cache['directory_layout'] = cache.cache.get('directory_layout', 'tms')
            cache.cache['type'] = cache.cache.get('type', 'file')
            cache.save()
    return map_proxy_app.to_dict()


def create_seed_dict(layers, task):
    """
    Creates a seed yaml file used for seed tasks.
    :param layers: the django layers to be seeded (if None, defaults to all)
    :param task: the django task object that represents the task this seed dict is for
    :return: seed yaml as a dict
    """
    coverages = {}
    seeds = {}

    for orig_id, layer in layers.iteritems():
        coverage_name = layer.generate_coverage_name()
        coverages[coverage_name] = {'bbox': TaskLayers.objects.filter(task=task, layer_id=orig_id)[0].get_bbox(),
                                    'srs': 'EPSG:4326'}
        source_name = layer.get_source_name()
        seeds[source_name] = {'caches': [source_name],
                              'coverages': [coverage_name],
                              'levels': TaskLayers.objects.filter(task=task, layer_id=orig_id)[0].get_levels()}
    return {'seeds': seeds, 'coverages': coverages}
