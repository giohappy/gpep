"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import logging
import os

from django.core.management.base import BaseCommand

from gpep_apps.config import gpepconfig
from gpep_apps.mpas import model_creator
from gpep_apps.mpas.models import MapProxyApp

log = logging.getLogger('django')


class Command(BaseCommand):
    help = 'Populate the db with the existing mapproxy apps'

    def handle(self, *args, **options):

        config = gpepconfig.instance()

        log.info("\nPopulated the database with the following map proxy apps:")

        yaml_dir = os.path.join(config.app_dir)
        for filename in os.listdir(yaml_dir):
            if ".yaml" in filename:
                service_name = filename[:filename.rfind('.')]
                if not MapProxyApp.objects.filter(name=service_name).exists():
                    try:
                        yaml_path = os.path.join(yaml_dir, filename)
                        map_proxy_app = model_creator.create_map_proxy_app_from_file(yaml_path, pull_in_base=True)
                        map_proxy_app.save()
                        log.info("created {0}".format(service_name))
                    except Exception as e:
                        log.info("Error in yaml file for service {0}:\n {1}".format(service_name, e))
                else:
                    log.info("service already exists {0}".format(service_name))
