"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import logging
import os
import sys

from django.core.management.base import BaseCommand
from django.core.management import call_command
from django.conf import settings

from gpep_apps.pepapi.celery import app as celery_app

log = logging.getLogger('django')


class Command(BaseCommand):
    help = 'Delete all the migrations and the sqlite db and then rebuild them. also it adds a superuser'

    def handle(self, *args, **options):

        base_dir = settings.BASE_DIR
        keep_files = ['__init__.py', '__init__.pyc']
        apps = ['jobs', 'api', 'mpas', 'config', 'status']

        # kill all celery instances to make sure that nothing is holding onto the db
        if 'LINUX' in sys.platform.upper():
            os.system("ps auxww | grep 'celery worker' | awk '{print $2}' | xargs kill -9")
        else:
            os.system("taskkill /F /im celery.exe")

        # delete the migrations folders if exist

        log.info("\n -----DELETE ALL MIGRATIONS----- \n")
        for app in apps:
            cur_dir = os.path.join(base_dir, app, 'migrations')
            if os.path.isdir(cur_dir):
                for filename in os.listdir(cur_dir):
                    if filename not in keep_files:
                        file_path = os.path.join(cur_dir, filename)
                        try:
                            os.remove(file_path)
                            log.info("deleted {0}".format(file_path))
                        except OSError:
                            os.removedirs(file_path)
                            log.info("deleted {0}".format(file_path))
            else:
                os.makedirs(cur_dir)
                with open(os.path.join(cur_dir, '__init__.py'), "w") as init_file:
                    init_file.write('')

                log.info("created {0}".format(cur_dir))
                log.info("created {0}".format(os.path.join(cur_dir, '__init__.py')))

        log.info("\n -----DELETE DB.SQLITE3----- \n")
        db_path = os.path.join(base_dir, "db.sqlite3")
        if os.path.exists(db_path):
            try:
                os.remove(db_path)
                log.info("deleted{0}".format(db_path))
            except:
                log.error("FAILED: could not delete db.sqlite3")
                return

        log.info("\n -----MAKE MIGRATIONS----- \n")
        call_command("makemigrations")

        log.info("\n -----MIGRATE----- \n")
        call_command("migrate")

        # purges all jobs from message broker celery is hooked up to
        log.info('\n -------Purging Celery of All Jobs----------\n')
        celery_app.control.purge()

        log.info("\n -------CREATE CACHE TABLE-------")
        call_command('createcachetable')
