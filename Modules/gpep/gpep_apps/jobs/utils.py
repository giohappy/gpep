"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import os
import uuid
import zipfile
import fnmatch
import shutil

from gpep_apps.api.gpep_exception import GPEPException
from gpep_apps.config import gpepconfig


def get_default_gpep_grid(default_projection):
    """
    Gets the pre-defined grid name for projection
    :param default_projection: output srs
    :return: pre-defined grid name for projection
    """
    return 'NSG_GPEP_DEFAULT_EPSG{0}'.format(default_projection)


def get_default_esri_grid(default_projection):
    """
    Gets the pre-defined grid name for Esri projection used in compact caches.
    :param default_projection:  Output SRS.
    :return: Pre-defined grid name for the projection.
    """
    return 'ESRI_DEFAULT_EPSG{0}'.format(default_projection)


def get_unique_filename(input_path, name=None, extension=''):
    """
    Returns a unique filename for the layer name given in the directory defined by path.
    if path does not exist attempts to create it. If name is none, will generate a uuid
    :param input_path: path to put file in
    :param name: name to give to the file. defaults to a uuid
    :param extension: file extension
    :return: filename
    """
    if name is None:
        name = uuid.uuid4()
    if extension and not extension.startswith('.'):
        extension = '.{}'.format(extension)
    path = os.path.abspath(input_path)  # resolves issues with relative paths.
    if not os.path.exists(path):
        os.makedirs(path, 0766)  # if path doesnt exist create it

    i = 0
    filename = os.path.join(path, "{0}{1}".format(name, extension))
    while os.path.exists(filename):
        filename = os.path.join(path, "{0}{1}{2}".format(name, i, extension))
        i += 1
    return filename


def create_cache_folder(map_proxy_app, log):
    """
    Creates a new folder for the service name in the cache directory when caching begins.
    :param map_proxy_app: the app to create the cache folder for
    :param log: where to put any logging that happens
    :return: path to the created folder or None if the creation failed
    """
    cache_folder = os.path.join(gpepconfig.instance().cache_dir, map_proxy_app.name)
    if os.path.exists(cache_folder):
        return cache_folder
    try:
        os.mkdir(cache_folder)
        return cache_folder
    except OSError:
        log.exception('The {0} cache folder could not be created.'.format(map_proxy_app.name))
        return None


def get_unique_folder(input_path, name=None):
    """
    Returns a unique folder name for the layer name given in the directory defined by path.
    if path does not exist attempts to create it.
    :param input_path: path to put file in
    :param name: name for the folder. If None, generates uuid
    :return: filename
    """
    path = os.path.abspath(input_path)  # resolves issues with relative paths.
    if not os.path.exists(path):
        os.makedirs(path, 0766)  # if path doesnt exist create it

    if name is None:
        name = uuid.uuid4().hex
    i = 0
    filename = os.path.join(path, name)
    while os.path.exists(filename):
        filename = os.path.join(path, "{0}{1}".format(name, i))
        i += 1

    return filename


class open_file(object):
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs

    def __enter__(self):
        self.file = None
        try:
            self.file = open(*self.args, **self.kwargs)
            return self.file
        except (IOError, OSError) as e:
            raise GPEPException(e)

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.file:
            self.file.close()


def make_zipfile(output_filename, source_dir):
    """
    Zips up an entire directory, no file size limits (uses zip64)
    :param output_filename: path to output zip file
    :param source_dir: directory to zip
    :return:
    """
    relroot = source_dir
    with zipfile.ZipFile(output_filename, "w", zipfile.ZIP_DEFLATED, allowZip64=True) as zip:
        for root, dirs, files in os.walk(source_dir):
            # add directory (needed for empty dirs)
            zip.write(root, os.path.relpath(root, relroot))
            for file in files:
                filename = os.path.join(root, file)
                if os.path.isfile(filename):  # regular files only
                    arcname = os.path.join(os.path.relpath(root, relroot), file)
                    zip.write(filename, arcname)


def remove_file_locks(directory):
    """
    Removes all file locks (*.lck files) in a directory, recursively.
    :param directory: The directly to delete locks from.
    """

    # remove tile locks directory
    tile_locks_directory = os.path.join(directory, 'tile_locks')
    if os.path.isdir(tile_locks_directory):
        shutil.rmtree(tile_locks_directory)

    # remove any leftover .lck files
    for root, dirnames, filenames in os.walk(directory):
        for filename in fnmatch.filter(filenames, '*.lck'):
            path = os.path.join(root, filename)
            os.remove(path)
