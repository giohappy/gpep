import sys
import os
from exceptions import NotImplementedError

try:
    from osgeo import ogr, osr, gdal
except:
    sys.exit('ERROR: cannot find GDAL/OGR modules')


class Formats:
    """
    Defines the different formats supported by DataSource
    """
    WFS, GPKG, SpatiaLite, PostGIS, GeoJSON = range(5)


class DataSource:
    """ A DataSource represents an input or output of geospatial data.

    It's used as a wrapper around the lower level GDAL OGR API drivers to support the clipping, translation, and
    read/writing of feature data from one datasource into another.

    You could, for instance, connect to WFS, write from there to PostGIS, then write from that PostGIS
    to a GeoPackage. The below example goes dierctly from WFS to a GeoPackage.

    Example:
        source = DataSource()
        source.openWFS('https://demo.boundlessgeo.com/geoserver/ows')

        if not source.is_opened():
            raise Exception("Failed to open source datasource.")

        dest = DataSource()
        dest.openGPKG('path/to/geopackage/file')

        if not dest.is_opened():
            raise Exception("Failed to open destination datasource.")

        # check DataSource.write_to docs for specifying certain layers and a bounding box
        source.write_to(dest_datasource=dest, epsg=4326)

        # ensure all results flushed to disk, close file so others can open it
        dest_datasource.close()



        # Note:  You can also use the "with" statement
        with DatSource() as source:
            source.openWFS('https://demo.boundlessgeo.com/geoserver/ows')

            # ... do stuff with the datasource

        # exiting the above with statement automatically closes the datasource.

    """

    def __init__(self):
        self.ogr_ds = None  # lower level GDAL OGR datasource
        self._setup_wfs_configs()  # required for large wfs requests
        self.format = None  # file format (postgis, gpkg, sqlite...)

    def __enter__(self):
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        self.close()

    def is_opened(self):
        """
        Used after DataSource.openXXX to see if the datasource was successfully opened.  Check example doc in the class
        docstring.
        :return: True if a lower level GDAL OGR datasource is successfully opened, False otherwise.
        """
        return self.ogr_ds is not None

    def close(self):
        """
        Close the datasource, ensure all results are flushed to disk.
        """
        if self.ogr_ds:
            self.ogr_ds.Destroy()
        self.ogr_ds = None

    def _setup_wfs_configs(self):
        # Speeds up querying WFS capabilities for services with alot of layers
        gdal.SetConfigOption('OGR_WFS_LOAD_MULTIPLE_LAYER_DEFN', 'NO')

        # Set config for paging. Works on WFS 2.0 services and WFS 1.0 and 1.1 with some other services.
        gdal.SetConfigOption('OGR_WFS_PAGING_ALLOWED', 'YES')
        gdal.SetConfigOption('OGR_WFS_PAGE_SIZE', '10000')

        gdal.SetConfigOption('GDAL_HTTP_UNSAFESSL', 'YES')

    def openWFS(self, url):
        """
        Open a Web Feature Service.
        :param url {String}: Path to the base portion of a WFS service (the part before the question mark).
        """
        driver = ogr.GetDriverByName('WFS')
        self.ogr_ds = driver.Open('WFS:' + url)
        self.format = Formats.WFS

    def openSpatiaLite(self, filepath):
        """
        Open an SQLite database using the SpatiaLite extension for storing and retreiving geographical objects.
        :param filepath {String}: Path on the filesystem to the sqlite file.  eg: 'C:/tmp/features.sqlite'
        :return:
        """
        self.ogr_ds = Util._open_file_with_driver(ogr.GetDriverByName('SQLite'), filepath)
        self.format = Formats.SpatiaLite

    def openPostGIS(self, database, user, password, host='localhost', port='5432'):
        """
        Connect to a PostgreSQL Database with the PostGIS extension installed.
        :param database {String}: name of the database
        :param user {String}: name of a user with access to the database
        :param password {String}: password for the above user
        :param host {String}: Address where server can be located. Might be 'localhost' or an IP address.
        :param port {String or Integer}: Port to look for at the above host location.
        """

        connectionString = "PG:dbname='{database}' host='{host}' port='{port}' user='{user}' password='{password}'" \
            .format(database=database, host=host, port=port, user=user, password=password)

        self.ogr_ds = ogr.Open(connectionString)
        self.format = Formats.PostGIS

    def openGPKG(self, filepath):
        """
        Open a GeoPackage file.

        :param filepath {String}: Path to GeoPackage file, may or may not already exist.
        """
        self.ogr_ds = Util._open_file_with_driver(ogr.GetDriverByName('GPKG'), filepath)
        self.format = Formats.GPKG

    def openGeoJSON(self, filepath):
        """
        Opens a GeoJSON file.
        NOTE: Updating data in an existing GeoJSON file is not supported, if an existing GeoJSON
        filepath is specified then that file will be opened in READ ONLY mode.  However, if filepath points to
        a file which does not yet exist, a new file will be opened in WRITE mode.

        :param filepath {String}: Filepath to create GeoJSON file at.
        """
        self.ogr_ds = Util._open_file_with_driver(ogr.GetDriverByName('GeoJSON'), filepath)
        self.format = Formats.GeoJSON

    def get_names(self):
        """
        :return: A list of string names for each layer.
        """
        return [layer.GetName() for layer in self.ogr_ds]

    def get_names_and_titles(self):
        """
        Retrieves a list of dictionaries of names and titles for each layer for WFS XML.
        :return: List of string layer titles. Example output:
            [{'name': 'OverloadedMoney', 'title': 'Big Money Big Man'},
             ['name': 'MoneyShort', 'title': 'Small Money Small Man'}
            ]
        """
        if self.format not in [Formats.WFS, ]:
            raise RuntimeError('Layer titles can only be retreived from WFS.')

        names_and_titles = []
        layer = self.ogr_ds.GetLayerByName('WFSLayerMetadata')
        feature_count = layer.GetFeatureCount()

        for i in xrange(1, feature_count + 1):  # feature feature is at index 1
            feature = layer.GetFeature(i)
            names_and_titles.append({'title': feature.GetField('title'), 'name': feature.GetField('layer_name')})
        return names_and_titles

    def write_to(self, dest_datasource, epsg, layer_names=None, bbox=None):
        """
        Writes layer data from self into the dest_datasource. layer_names can be used to specify which layers
        to write, and bbox can be used to only copy features within the bbox's range.

        :param dest_datasource {DataSource}: Datasource to write data into.
        :param layer_names {Dictionary or None}: If none, copy all layers from self's datasource to dest_datasource,
            keeping the names unchanged.  Otherwise:

            Dictionary mapping from layer names in the source datasource to layer names in the
            dest_datasource.  For example, suppose you use this mapping:

            {'white': 'like_a_snowflake_in_the_darkness', 'black': 'his_dream_crumbled_like_burnt_charcoal'}

            In this case, the layer 'white' in self's datasource will be renamed to 'like_a_snowflake_in_the_darkness'
            and written out to the dest_datasource.  Likewise for 'black' in self's datasource, which will be written
            out to dest datasource as a layer named 'his_dream_crumbled_like_burnt_charcoal'.

        :param epsg {Integer}: EPSG features should be written out as.
        :param bbox {Dictionary}: Set to None if it is desired to copy all data. Otherwise, specify
            north/south/east/west bounds.  For example:
            {'north': 90, 'south': -90, 'east': 180, 'west': -180}
        """
        if not self.is_opened():
            raise RuntimeError("The source datasource is not opened.")
        elif not dest_datasource.is_opened():
            raise RuntimeError("The destination datasource is not opened.")
        elif dest_datasource.format == Formats.WFS:
            raise RuntimeError("WFS cannot be written to.")
        elif bbox and self.format not in [Formats.SpatiaLite, Formats.PostGIS]:
            raise NotImplementedError('Only PostGIS and SpatiaLite sources support specifying a bbox.')

        if layer_names:
            # if layer names defined, use mapping of source names to output names
            for src_layer_name, dest_layer_name in layer_names.iteritems():
                source_layer = self.ogr_ds.GetLayerByName(src_layer_name)
                self._copy_layer(dest_datasource.ogr_ds, source_layer, dest_layer_name, epsg, bbox, False)
        else:
            # otherwise layer_names is None, then just copy all layers, keeping their source names unchanged
            for source_layer in self.ogr_ds:
                name = source_layer.GetName()
                self._copy_layer(dest_datasource.ogr_ds, source_layer, name, epsg, bbox, False)

    def _copy_layer(self, dest_ds, in_layer, new_layer_name, dest_epsg, bbox=None, clip_to_bbox=True):
        """
        Copies in_layer into destination datasource dest_ds, with a new name new_layer_name, and transformed to
        dest_epsg.  Only features intersecting bbox will be copied, and if clip_to_bbox is set to True the features
        themselves will be clipped to fit within bbox.

        :param dest_ds: GDAL OGR DataSource
        :param in_layer: {osgeo.ogr.Layer} which is written out to dest_ds
        :param new_layer_name: {String} Name to give output layer
        :param dest_epsg: {Integer} EPSG to store features in.  Feature will be transformed to this EPSG if necessary.
        :param bbox {Dictionary}: Set to None if it is desired to copy all data. Otherwise, specify
            north/south/east/west bounds.  For example:
            {'north': 90, 'south': -90, 'east': 180, 'west': -180}
        :param clip_to_bbox {Boolean}: If True, will clip all features to be within the bounding box.  If False,
            entire features which partially intersect with the bbox will be copied to the destination datasource.
        """

        inSpatialRef = in_layer.GetSpatialRef()
        in_epsg = int(inSpatialRef.GetAttrValue('AUTHORITY', 1))

        # output SpatialReference
        dest_epsg = int(dest_epsg)
        outSpatialRef = osr.SpatialReference()
        outSpatialRef.ImportFromEPSG(dest_epsg)

        # TODO consider putting this back in, could speed up performance in certain cases
        # if input and output EPSG Codes match, no transformation is required, simply use CopyLayer.
        # if dest_epsg == in_epsg and not bbox:
        #     dest_ds.CopyLayer(in_layer, new_layer_name, ["OVERWRITE=YES"])
        #     dest_ds = None
        #     return

        # ensure the correct type of datasource is used
        if not dest_ds.__module__ + "." + dest_ds.__class__.__name__ == 'osgeo.ogr.DataSource':
            raise ValueError("dest_ds is the wrong type of datasource.  You must use \'osgeo.ogr.DataSource.\'")

        # create the CoordinateTransformation
        coordTrans = osr.CoordinateTransformation(inSpatialRef, outSpatialRef)

        # create the output layer
        outLayer = dest_ds.CreateLayer(new_layer_name, outSpatialRef, geom_type=in_layer.GetGeomType())

        # add fields
        inLayerDefn = in_layer.GetLayerDefn()
        for i in range(0, inLayerDefn.GetFieldCount()):
            fieldDefn = inLayerDefn.GetFieldDefn(i)
            outLayer.CreateField(fieldDefn)

        # get the output layer's feature definition
        outLayerDefn = outLayer.GetLayerDefn()

        if bbox:
            # a bbox geometry is created which matches the epsg of the features within the input layer.
            # the actual bounds are specified in 4326 but then converted to dest_epsg within the geometry.
            # This is required, as we will later use this bbox_geometry as a spatial filter on our in_layer, and
            # the EPSG of a layer and its spatial filter must match.
            bbox_geometry = Util.create_bbox_geometry(north=bbox['north'],
                                                      south=bbox['south'],
                                                      east=bbox['east'],
                                                      west=bbox['west'],
                                                      input_epsg=4326,
                                                      output_epsg=in_epsg)
            in_layer.SetSpatialFilter(bbox_geometry)

        inFeature = in_layer.GetNextFeature()
        while inFeature:
            geom = inFeature.GetGeometryRef()  # get the input geometry
            geom.Transform(coordTrans)  # reproject the geometry

            if bbox and clip_to_bbox:
                # clip parts of feature outside the bbox if clipping is turned on
                geom = geom.Intersection(bbox_geometry)

            outFeature = ogr.Feature(outLayerDefn)  # create a new feature
            outFeature.SetGeometry(geom)  # set the geometry and attribute
            for i in range(0, outLayerDefn.GetFieldCount()):
                outFeature.SetField(outLayerDefn.GetFieldDefn(i).GetNameRef(), inFeature.GetField(i))
            outLayer.CreateFeature(outFeature)

            # dereference the features and get the next input feature
            outFeature = None
            inFeature = in_layer.GetNextFeature()

        # Save and close the output datasource
        dest_ds = None


class Util:
    @staticmethod
    def _open_file_with_driver(driver, filepath):
        """
        Convenience utility for opening a file with a basic driver.  If the file specified by filepath already
        exists, the file will be opened in update mode.  If it doesn't exist, a new file will be created.
        Useful for geopackages and sqlite databases.

        :param driver: gdal.ogr driver, use gdal.ogr.GetDriverByName('driver name') to pass in
        :param filepath {String}: Path to file, like 'C:/tmp/swag.sqlite' or r'C:/tmp/yolo.gpkg'.
        :return: Returns an ogr.DataSource object.
        """
        if os.path.isfile(filepath):
            # passing 1 as second parameters opens in update mode, so layers can be added
            return driver.Open(filepath, 1)
        else:
            # if out file doesn't already exist, then new datasource must be created
            return driver.CreateDataSource(filepath)

    @staticmethod
    def create_bbox_geometry(north, south, east, west, input_epsg, output_epsg):
        """
        Creates a square geometry specified by north/south/east/west bounds.  The bounds are specified in the coordinate
        system based on the input_epsg, and the square geometry is created in the output_epsg.

        :param north {Float}:  Northern extent of the bbox
        :param south {Float}:  Southern extent of the bbox.
        :param east  {Float}:  Eastern extent of the bbox.
        :param west: {Float}:  Western extent of the bbox.
        :param input_epsg {Integer}: EPSG the north/south/east/west parameters are specified in.
        :param output_epsg {Integer}: EPSG the bbox geometry is created with.
        :return: The created bbox geometry, as a linear ring.
        """

        north = float(north)  # max y
        south = float(south)  # min y
        east = float(east)  # max x
        west = float(west)  # min x

        oRing = ogr.Geometry(ogr.wkbLinearRing)

        # the north/south/east/west coordinates are specified according to the input_epsg
        input_ref = Util.create_spatial_ref(input_epsg)
        oRing.AssignSpatialReference(input_ref)

        # form a square bounding box
        oRing.AddPoint_2D(west, south)  # bottom left
        oRing.AddPoint_2D(west, north)  # top left
        oRing.AddPoint_2D(east, north)  # top right
        oRing.AddPoint_2D(east, south)  # bottom right
        oRing.AddPoint_2D(west, south)  # bottom left

        out_ref = Util.create_spatial_ref(output_epsg)
        oRing.TransformTo(out_ref)

        # spatial filter geometries have to be polygons
        polygon = ogr.Geometry(ogr.wkbPolygon)
        polygon.AddGeometry(oRing)

        return polygon

    @staticmethod
    def create_spatial_ref(epsg):
        """
        Creates an ogr.SpatialReference object give the epsg.
        :param input_epsg {Integer}: source epsg number
        :param output_epsg {Integer}: destination epsg number
        :return: Spatial Reference object.
        """
        epsg = int(epsg)
        spatial_ref = osr.SpatialReference()
        spatial_ref.ImportFromEPSG(epsg)
        return spatial_ref

    # TODO maybe remove, not needed. can use geom.TranformTo(spatial_ref) to transform geometries
    @staticmethod
    def create_spatial_transformer(in_epsg, out_epsg):
        """
        Creates an osr.CoordinateTransformation object, can be used to transform object in in_epsg to an equivalent
        object in out_epsg.
        :param in_epsg {Integer}:  number like 4326 or 3857
        :param out_epsg {Integer}: number like 4326 or 3857
        :return: transformation object
        """
        in_epsg = int(in_epsg)
        out_epsg = int(out_epsg)

        in_spatial_ref = osr.SpatialReference()
        in_spatial_ref.ImportFromEPSG(in_epsg)

        out_spatial_ref = osr.SpatialReference()
        out_spatial_ref.ImportFromEPSG(out_epsg)

        return osr.CoordinateTransformation(in_spatial_ref, out_spatial_ref)


if __name__ == '__main__':
    # some sample code that can be used to test the functionality of this library

    # bbox = {'north': 90, 'south': -90, 'east': 180, 'west': -180}
    # bbox = {'north': 85.06, 'south': -85.06, 'east': 180, 'west': -180}

    # world bounds in meters
    # bbox = {'north': 20048966.10, 'south': -20048966.10, 'east': 20026376.39, 'west': -20026376.39}

    # eastern united states
    bbox = {'north': 40, 'south': 33, 'east': -77, 'west': -82}
    TMP_NAME = 'states13'
    layer_names = {'usa:states': TMP_NAME}

    # usa water points
    # layer_names = {'WFS_geonames:Hydro_Points': 'Hydro_Points'}

    # amsterdam
    # bbox = {'north': 53.7, 'south': 50.75, 'east': 7.22, 'west': 3.2}
    # layer_names = {'restwarmte:liggingindustrieco2': 'tea'}

    with DataSource() as source:
        # source.openWFS('http://geodata.nationaalgeoregister.nl/restwarmte/wfs')
        source.openWFS('https://demo.boundlessgeo.com/geoserver/ows')
        # source.openWFS('https://services.nationalmap.gov/arcgis/services/WFS/geonames/MapServer/WFSServer')

        if not source.is_opened():
            raise Exception("Failed to open source datasource")

        titles = source.get_names_and_titles()

        dest = DataSource()
        # dest.openPostGIS('postgres', 'postgres', 'root', 'localhost', '5432')
        dest.openSpatiaLite(r'C:/ProgramData/GPEP/tmp/intermediate.sqlite')
        # dest.openGeoJSON(r'C:/ProgramData/GPEP/tmp/states2.json')

        if not dest.is_opened():
            raise Exception("Failed to open dest datasource")

        source.write_to(dest, 3857, layer_names)

        final_dest = DataSource()
        # final_dest.openGeoJSON(r'C:\Users\Dominic Napoleon\gitlab\opensource-gpep\
        # testing-gdal\usa_easter_states_wfs_postgis_gpkg_17.gpkg')
        final_dest.openGeoJSON(r'C:/ProgramData/GPEP/tmp/states5.json')

        dest.write_to(final_dest, 3857, {TMP_NAME: TMP_NAME}, bbox)

        # def write_to(self, dest_datasource, epsg, layer_names=None, bbox=None):
        # def write_to(self, dest_datasource, epsg, layer_names=None, bbox=None):

        # copy all layers for now
        # for source_layer in self.ogr_ds:
        #     self._copy_layer(dest_datasource.ogr_ds, source_layer, "tea6", 4326, bbox)

        # restwarmte:liggingindustrieco2
        # self._copy_layer(dest_datasource.ogr_ds, self.ogr_ds.GetLayerByName("usa:states"),
        # "STATES_clipped", 4326, bbox)

        # example:
