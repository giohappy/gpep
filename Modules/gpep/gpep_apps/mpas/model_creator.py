"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import copy
import os
import yaml

from mapproxy.script.conf.sources import sources
from mapproxy.script.conf.caches import caches
from mapproxy.script.conf.grids import grids
from mapproxy.script.conf.layers import layers

from gpep_apps.api.gpep_exception import GPEPException
from gpep_apps.mpas import models


def create_foreign_keys(in_dict=None, item_list=None):
    """
    Creates the parameters to pass into
    :param in_dict: the parameters to start off with
    :param item_list: the parameters to generate and add in this format
        (destination, function=None, params={})
        destination: where to place value in in_dict
        function: function to call with the given params
        params: params to pass to the function or place in key if function is None
    :return: in_dict with parameters added from item_list
    """
    if in_dict is None:
        in_dict = {}
    if item_list is None:
        return in_dict
    for item in item_list:
        dest = item[0]
        funct = item[1] if len(item) > 1 else None
        params = item[2] if len(item) > 2 else {}
        if funct is None:
            in_dict[dest] = params
        elif in_dict.get(dest) is not None:
            in_dict[dest] = funct(in_dict[dest], **params)
    return in_dict


def create_model(model_type, data=None, save_model=True):
    """
    Creates an instance of a model of the given type with the given data
    :param model_type: the class of the model to create
    :param data: the data to put inside of the model
    :param save_model: if the model should be saved to the database
    :return: the model instance
    """
    if data is None:
        data = {}
    model = model_type(**data)
    if save_model:
        model.save()
    return model


def create_list(in_list, creator=None, **kwargs):
    """
    For each item in the list or dict of items, creates a model for that item
    :param in_list: the list or dict to create the models from
    :param creator: creator method to use when creating elements in the list
    :param kwargs: extra arguments to pass into the creator function
    """
    if in_list is None:
        return
    if isinstance(in_list, list):
        for list_item in in_list:
            creator(value=list_item, **kwargs)
    elif isinstance(in_list, dict):
        for list_item in in_list:
            creator(key=list_item, value=in_list[list_item], **kwargs)
    else:
        raise GPEPException('{} is not of type list'.format(in_list))


def yaml_path_to_dict(path):
    """
    Creates a dict from the yaml file at the given path
    :param path: location of the yaml file to dictify
    :return: dict of data in yaml file
    """
    return yaml.safe_load(file(path).read())


def create_map_proxy_app_from_file(path, pull_in_base=False):
    """
    Creates a map proxy app from a yaml file
    :param path: location of the yaml file
    :param pull_in_base: if the dict should be updated with the values in the base file
    :return: map proxy app model object
    """
    yaml_dict = yaml_path_to_dict(path)
    name = os.path.splitext(os.path.basename(path))[0]
    return create_map_proxy_app_from_dict(yaml_dict=yaml_dict, name=name, path=path, pull_in_base=pull_in_base)


def create_map_proxy_app_from_capabilities(capabilities, service_name, service_url, yaml_path, base=None,
                                           source_options={}, cache_options={}):
    """
    Creates a map proxy app from a given parsed capabilities object. Sources and caches are created for the app with the
    given source_options and cache_options, respectively (contents are outlined in Map Proxy sources.py and caches.py
    functions).

    :param capabilities: The parsed capabilities object for the desired service
    :param service_name: The name of the service that the given capabilities originated from.
    :param service_url: The URL of the service that the given capabilities originated from.
    :param yaml_path: The directory to store the new Map Proxy app's files (e.g. config).
    :param base: Optional, defaults to None. Map Proxy config YAML to use as the base config for the new Map Proxy app.
    :param source_options: Optional, defaults to empty dict. Configuration options for sources configuration (see
                           MapProxy sources.py).
    :param cache_options: Optional, defaults to empty dict. Configuration options for caches configuration (see MapProxy
                          caches.py).

    :return: A map proxy app model object
    """

    base_grids = {}
    if base:
        cap_dict = yaml_path_to_dict(base)
        if 'grids' in cap_dict:
            base_grids = cap_dict['grids']

        cap_dict['base'] = os.path.abspath(base)
    else:
        cap_dict = {}

    if capabilities.service == 'WMTS':
        update_dicts(cap_dict, {'grids': grids(capabilities, base_grids)})

    title = capabilities.metadata()['title']
    wms_service_info = {
        'md': {'title': title},
        # There is a possibility for there to be multiple grids with the same srs so use a set first to gather only
        # unique srs and then convert to a list.
        'srs': list({grid['srs'] for grid in base_grids.values()})
    }
    wmts_service_info = {
        'md': {'title': title}
    }
    update_dicts(cap_dict, {'services': {'wms': wms_service_info, 'wmts': wmts_service_info}})
    if 'grids' in cap_dict:
        all_grid_names = cap_dict['grids'].keys()
    else:
        all_grid_names = []
    update_dicts(cap_dict, {'sources': sources(capabilities, all_grid_names, source_options)})
    update_dicts(cap_dict, {'caches': caches(cap_dict['sources'], base_grids.keys(), cache_options)})
    update_dicts(cap_dict, {'layers': layers(capabilities, cap_dict['caches'], all_grid_names,
                                             cache_options.get('primary_grid'))})

    app = create_map_proxy_app_from_dict(yaml_dict=cap_dict, name=service_name, path=yaml_path)
    app.external_url = service_url
    return app


def create_map_proxy_app_from_gvs_compact_cache_v1(service_name, yaml_path, base=None,
                                                   source_options={}, cache_options={}, conf_xml=None):
    # pull in base grids and set the base option of yaml dictionary
    base_grids = {}
    if base:
        cap_dict = yaml_path_to_dict(base)
        if 'grids' in cap_dict:
            base_grids = cap_dict['grids']

        cap_dict['base'] = os.path.abspath(base)
    else:
        cap_dict = {}

    update_dicts(base_grids, cache_options['custom_grids'])

    primary_cache = {
        '{}_cache{}'.format(service_name, ''): {
            'grid': cache_options['primary_grid']
        }
    }

    update_dicts(cap_dict,
                 {'caches': caches(primary_cache, base_grids.keys(), cache_options, primary_cache_has_source=False)})

    update_dicts(cap_dict, {
        'layers': [{
            'name': 'layer',
            'title': 'layer',
            'source': '{}_cache_{}'.format(service_name, 'NSG_GPEP_DEFAULT_EPSG4326')
        }]
    })

    app = create_map_proxy_app_from_dict(yaml_dict=cap_dict, name=service_name, path=yaml_path)
    return app


def update_dicts(og_dict, new_dict):
    """
    Updates or adds the keys from the new_dict into og_dict
    :param og_dict: the dict to update
    :param new_dict: the values to put into the old dict
    """
    for key, value in new_dict.iteritems():
        if key in og_dict and isinstance(og_dict[key], dict) and isinstance(value, dict):
            update_dicts(og_dict[key], value)
        elif value is not None:
            og_dict[key] = value


def verify_map_proxy_app(map_proxy_app_dict):
    """
    Verifies that the dict's internal source pointers exist in the dict
    :param map_proxy_app_dict: the dict to verify
    """
    assert isinstance(map_proxy_app_dict, dict)
    caches = map_proxy_app_dict.get('caches', {})
    sources = map_proxy_app_dict.get('sources', {})
    layers = map_proxy_app_dict.get('layers', [])

    def verify_layers(layers):
        for layer in layers:
            assert isinstance(layer, dict)
            for layer_source in layer.get('sources', []):
                assert layer_source in caches or layer_source in sources
            if 'layers' in layer:
                verify_layers(layer['layers'])
    verify_layers(layers)

    for cache in caches.values():
        assert isinstance(cache, dict)
        for source in cache.get('sources', {}):
            assert source in caches or source in sources


def initialize_sources(yaml_model, yaml_dict):
    if 'caches' in yaml_dict:
        for cache_key, cache in yaml_dict.get('caches').iteritems():
            if 'sources' in cache:
                model_source = yaml_model.get_caches(key=cache_key).first()
                if model_source is not None:
                    model_source.set_source(models._get_source_model_from_str(yaml_model, cache['sources'][0]))

    def init_layers(inner_dict):
        for layer in inner_dict.get('layers', []):
            if layer.get('sources'):
                model_source = models.Layer.objects.filter(
                    map_proxy_app=yaml_model, name=layer.get('name'), title=layer.get('title')).first()
                model_source.set_source(models._get_source_model_from_str(yaml_model, layer['sources'][0]))
            if 'layers' in layer:
                init_layers(layer)
    init_layers(yaml_dict)


def create_map_proxy_app_from_dict(yaml_dict, name='default_name', path=None, pull_in_base=False):
    """
    Creates a map proxy app object from the given yaml_dict
    :param yaml_dict: dict to create the model from
    :param name: name to give to the model
    :param path: path to the yaml model
    :param pull_in_base: if the 'base' path in the dict should be pulled in
    :return: map proxy app model object
    """
    if pull_in_base:
        base = yaml_dict.get('base')
        base_dict = yaml_path_to_dict(base)
        update_dicts(yaml_dict, base_dict)
    verify_map_proxy_app(yaml_dict)
    dict_copy = copy.deepcopy(yaml_dict)
    globals_list = create_globals(yaml_dict.get('globals'))
    services = create_services(yaml_dict.get('services'))
    data_dict = {
        'file_location': path, 'name': name, 'base': yaml_dict.get('base'),
        'globals': globals_list, 'services': services, 'is_copy': path is None
    }
    yaml_model = create_model(models.MapProxyApp, data_dict)
    create_list(yaml_dict.get('caches'), creator=create_cache, app=yaml_model)
    create_list(yaml_dict.get('layers'), creator=create_layer, app=yaml_model)
    create_list(yaml_dict.get('grids'), creator=create_grid, app=yaml_model)
    create_list(yaml_dict.get('sources'), creator=create_source, app=yaml_model)
    initialize_sources(yaml_model, dict_copy)
    return yaml_model


def create_grid(value, app, key=None):
    """
    Creates a grid instance
    :param value: dict of item
    :param app: map proxy app instance
    :param key: key for this object in the dict
    :return: grid model instance
    """
    args = (('key', None, key), ('map_proxy_app', None, app))
    return create_model(models.Grid, create_foreign_keys(value, args))


def create_coverage(value):
    """
    Creates a coverage model object
    :param value: dict with values to put in object
    :return: coverage object
    """
    return create_model(models.Coverage, value)


def create_http_opts(value):
    """
    Creates a http opts model object
    :param value: dict with values to put in object
    :return: http opts object
    """
    return create_model(models.HTTPOpts, value)


def create_wms_opts(value=None):
    """
    Creates a wms opts model object
    :param value: dict with values to put in object
    :return: whs opts object
    """
    return create_model(models.WMSOpts, value)


def create_source_image(value):
    """
    Creates a source image model object
    :param value: dict with values to put in object
    :return: source image object
    """
    return create_model(models.SourceImage, value)


def create_map_server_opts(value):
    """
    Creates a map server opts model object
    :param value: dict with values to put in object
    :return: map server opts object
    """
    return create_model(models.MapServerOpts, value)


def create_image_opts(value):
    """
    Creates a image opts model object
    :param value: dict with values to put in object
    :return: image opts object
    """
    return create_model(models.ImageOpts, value)


def create_source(value, app, key=None):
    """
    Creates a source object
    :param value: dict with values to put in object
    :param app: map proxy app instance
    :param key: key for this object in the dict
    :return: source object of type specified in value['type']
    """
    args = (('map_proxy_app', None, app), ('key', None, key), ('coverage', create_coverage))
    value = create_foreign_keys(value, args)
    source_type = value.get('type')
    if source_type == 'wms':
        args = (('wms_opts', create_wms_opts), ('image', create_source_image))
        model = models.WMSSource
    elif source_type == 'mapserver':
        args = (('wms_opts', create_wms_opts), ('image', create_source_image),
                ('mapserver', create_map_server_opts))
        model = models.MapServerSource
    elif source_type == 'tile':
        args = (('http', create_http_opts),)
        model = models.TileSource
    elif source_type == 'mapnik':
        args = (('image', create_image_opts),)
        model = models.MapnikSource
    elif source_type == 'debug':
        args = []
        model = models.DebugSource
    else:
        raise GPEPException('{} is not a valid source type'.format(source_type))
    keys = create_foreign_keys(value, args)
    return create_model(model, keys)


def create_layer(value, app, parent=None):
    """
    Creates a layer object
    :param value: dict with values to put in object
    :param app: map proxy app instance
    :param parent: parent layer
    :return: layer object
    """
    layers = value.get('layers')
    if layers is not None:
        del(value['layers'])
    args = (('parent_layer', None, parent),
            ('map_proxy_app', None, app),
            ('layers', create_list, {'creator': create_layer, 'parent': 'model'}))
    source = None
    tile_sources = None
    if value.get('sources'):
        source = value.get('sources')[0]
        del(value['sources'])
    if value.get('source'):
        source = value.get('source')
        del(value['source'])
    if 'tile_sources' in value:
        tile_sources = value.get('tile_sources')
        del(value['tile_sources'])
    model = create_model(models.Layer, data=create_foreign_keys(value, args))
    create_list(layers, parent=model, creator=create_layer, app=app)
    if source:
        source_model = models._get_source_model_from_str(app, source, verify=False)
        if source_model:
            model.set_source(source_model)
    if tile_sources:
        model.set_tile_sources(tile_sources)
    return model


def create_watermark(value):
    """
    Creates a watermark object
    :param value: dict with values to put in object
    :return: watermark object
    """
    if value is None:
        return
    return create_model(models.Watermark, data=value)


def create_cache(value, app, key=None):
    """
    Creates a cache object
    :param value: dict with values to put in object
    :param app: map proxy app instance
    :param key: key for this object in the dict
    :param verify: if the model's get source should be verified
    :return: cache object
    """
    for cache in app.get_caches(key=key):
        cache.delete()
    source = None
    if value.get('sources'):
        source = value['sources'][0]
        del(value['sources'])
    args = (('map_proxy_app', None, app), ('key', None, key), ('watermark', create_watermark))
    out_model = create_model(models.Cache, data=create_foreign_keys(value, args))
    if source:
        source_model = models._get_source_model_from_str(app, source, verify=False)
        if source_model:
            out_model.set_source(source_model)
    return out_model


def create_service_demo(value):
    """
    Creates a service demo object
    :param value: dict with values to put in object
    :return: service demo object
    """
    return create_model(models.DemoService, data=create_foreign_keys(value))


def create_kml_service(value):
    """
    Creates a kml service object
    :param value: dict with values to put in object
    :return: kml service object
    """
    return create_model(models.KMLService, data=create_foreign_keys(value))


def create_tms_service(value):
    """
    Creates a tms service object
    :param value: dict with values to put in object
    :return: tms service object
    """
    return create_model(models.TMSService, data=create_foreign_keys(value))


def create_wmts_service(value):
    """
    Creates a wmts service object
    :param value: dict with values to put in object
    :return: wmts service object
    """
    return create_model(models.WMTSService, data=create_foreign_keys(value))


def create_wms_service(value):
    """
    Creates a wms service object
    :param value: dict with values to put in object
    :return: wms service object
    """
    return create_model(models.WMSService, data=create_foreign_keys(value))


def create_services(value):
    """
    Creates services for each service type
    :param value: dict with values to put in object
    :return: base service object
    """
    args = (('demo', create_service_demo), ('kml', create_kml_service),
            ('tms', create_tms_service), ('wmts', create_wmts_service),
            ('wms', create_wms_service))
    data = create_foreign_keys(value, args)
    model = create_model(models.Service, data=data)
    return model


def create_globals(value):
    """
    Creates a globals object
    :param value: dict with values to put in object
    :return: globals object
    """
    args = (('http', create_http_opts),)
    return create_model(models.Global, data=create_foreign_keys(value, args))
