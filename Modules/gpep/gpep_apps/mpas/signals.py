"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import os
import logging

from django.db.models.signals import post_delete
from django.dispatch import receiver
from django.core.cache import cache

from models import MapProxyApp
from gpep_apps.api.gpep_exception import GPEPException


# Anything that affects the response of the service_list endpoint should invalidate
# the services cache
@receiver([post_delete], sender=MapProxyApp, dispatch_uid='invalidate_services_cache')
def invalidate_services_cache(sender, instance, **kwargs):
    cache.delete('services')


@receiver(post_delete, sender=MapProxyApp, dispatch_uid='delete_app_yaml')
def delete_app_yaml(sender, instance, **kwards):

    # Currently, mapproxyapps are deep copied for tasks. This signal handler runs for each mapproxyapp that is deleted
    # (including deep copies). The deep copies don't have their file_location field set. Therefore, if this field is
    # not set no extra work should be done.
    if instance.file_location is None:
        return

    try:
        if os.path.exists(instance.file_location):
            os.remove(instance.file_location)
    except OSError:
        log = logging.getLogger(str(instance.id))
        log.exception('The file {0} could not be deleted. Please check permissions on the Apps folder.'
                      .format(instance.file_location))
        raise GPEPException('A file could not be deleted. Please check permissions on the Apps folder.')
