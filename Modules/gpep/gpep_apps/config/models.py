"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

from __future__ import unicode_literals

import os
import sys

from django.db import models


MODE_CHOICES = [
    ['integrated', 'integrated'],
    ['standalone', 'standalone']
]

CACHE_TYPE_CHOICES = [
    ['file', 'file'],
    ['sqlite', 'sqlite'],
    ['mbtiles', 'mbtiles'],
    ['geopackage', 'geopackage'],
    ['gpkg_sqlserver', 'gpkg_sqlserver'],
    ['couchdb', 'couchdb'],
    ['riak', 'riak']
]

FILE_CACHE_DIR_LAYOUT_CHOICES = [
    ['tc', 'tc'],
    ['tms', 'tms'],
    ['quadkey', 'quadkey'],
    ['arcgis', 'arcgis']
]

CACHE_IMAGE_FORMAT_CHOICES = [
    ['image/png', 'image/png'],
    ['image/jpeg', 'image/jpeg'],
    ['mixed', 'mixed'],
    [None, 'none']
]

PROJECTION_CHOICES = [
    ['4326', '4326'],
    ['3857', '3857'],
    ['3395', '3395']
]

FRONTEND_CHOICES = [
    ['gpep', 'gpep'],
    ['mapproxy', 'mapproxy']
]

LOG_LEVEL_CHOICES = [
    ['critical', 'critical'],
    ['error', 'error'],
    ['warn', 'warn'],
    ['warning', 'warning'],
    ['info', 'info'],
    ['debug', 'debug'],
    ['notset', 'notset']
]


def is_linux():
    """
    Returns true if running on linux, false if not. Useful for testing
    :return: boolean true if linux, false if not
    """
    return 'LINUX' in sys.platform.upper()


def get_defaults():
    """
    Gets the default locations of files and folders on your current OS
    :return: list with gpep cache location, config folder name, mapproxy location
    """
    if is_linux():
        return r'/opt/pep', 'conf', r'/opt/pep/bin/mapproxy-seed'
    else:
        return r'C:\ProgramData\GPEP', 'config', r'C:\Python27\Scripts\mapproxy-seed.exe'

path, conf, mapproxy_seed = get_defaults()


class Config(models.Model):
    """Django model that stores all of the values in the config file."""
    app_dir = models.TextField(default=os.path.join(path, 'apps'))
    log_dir = models.TextField(default=os.path.join(path, 'log'))
    temp_dir = models.TextField(default=os.path.join(path, 'temp'))
    cache_dir = models.TextField(default=os.path.join(path, 'map_cache'))
    clean_seed_dir = models.TextField(default=os.path.join(path, 'seed'))
    template_dir = models.TextField(default=os.path.join(path, 'templates'))
    output_dir = models.TextField(default=os.path.join(path, 'export'))
    template_file = models.TextField(default=os.path.join(path, 'templates', 'default.yaml'))
    cache_type = models.TextField(default='file', choices=CACHE_TYPE_CHOICES)
    compact_cache_version = models.PositiveSmallIntegerField(default=1)
    file_cache_dir_layout = models.TextField(default='tms', choices=FILE_CACHE_DIR_LAYOUT_CHOICES)
    cache_image_format = models.TextField(default='image/png', choices=CACHE_IMAGE_FORMAT_CHOICES)
    default_projection = models.TextField(default='4326')
    db_conn_file = models.TextField(default=os.path.join(path, conf, 'dbconnfile.cfg'))
    strict_validation = models.BooleanField(default=True)
    log_level = models.TextField(default='info', choices=LOG_LEVEL_CHOICES)
    default_basemap_title = models.TextField(default='OpenStreetmap_Mapnik')
    default_basemap_url = models.TextField(default='http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png')
    default_basemap_max_zoom_level = models.IntegerField(default=18)
    max_repeat = models.IntegerField(default=10)
    on_error = models.BooleanField(default=True)
