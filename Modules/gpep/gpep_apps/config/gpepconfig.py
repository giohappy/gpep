"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import ConfigParser
import logging
import os
import yaml

from gpep_apps.api.gpep_exception import GPEPException
from gpep_apps.config import models


def instance():
    """
    Gets the GPEPConfig instance. If one doesn't exist, create it.
    :return: the GPEPConfig instance
    """
    gpep_model = models.Config.objects.all().first()
    if not gpep_model:
        gpep_model = ConfigModelCreator().create_model()
    return gpep_model


class ConfigModelCreator(object):
    def __init__(self):
        """
        Initializes a ConfigModelCreator object
        """
        self.log = logging.getLogger(__name__)
        self.linux = models.is_linux()
        self.GPEP_HOME = r'/opt/pep' if self.linux else r'C:\ProgramData\GPEP'
        self.PYTHON_HOME = r'/usr/bin' if self.linux else r'C:\Python27\Scripts'
        self.APP_EXTENSION = '' if self.linux else '.exe'
        default_config_location = r'/opt/pep/conf/pep.cfg' if self.linux else r'C:\ProgramData\GPEP\config\pep.cfg'
        self._config_file = os.environ.get('GPEP_CONF', default_config_location)

    def create_model(self):
        """
        Parses config file to set gpepconfig attributes
        """
        config = ConfigParser.RawConfigParser()
        if not os.path.exists(self._config_file):
            raise GPEPException('Expected config file at {}'.format(self._config_file))
        try:
            config.read(self._config_file)
        except ConfigParser.Error:
            self.log.exception('Could not parse config file')
            raise GPEPException('Could not parse config file')

        out_dict = dict()
        out_dict['app_dir'] = self.get_option(config, 'Directories', 'apps')
        out_dict['log_dir'] = self.get_option(config, 'Directories', 'logs')
        out_dict['temp_dir'] = self.get_option(config, 'Directories', 'temp')
        out_dict['cache_dir'] = self.get_option(config, 'Directories', 'cache')
        out_dict['clean_seed_dir'] = self.get_option(config, 'Directories', 'seed')
        out_dict['template_dir'] = self.get_option(config, 'Directories', 'templates')
        out_dict['output_dir'] = self.get_option(config, 'Directories', 'output')

        out_dict['template_file'] = self.get_option(config, 'Files', 'default_yaml')
        out_dict['db_conn_file'] = self.get_option(config, 'Files', 'db_conn_file')

        out_dict['cache_type'] = self.get_option(config, 'Caches', 'cache_type')
        out_dict['compact_cache_version'] = self.get_option(config, 'Caches', 'compact_cache_version')
        out_dict['file_cache_dir_layout'] = self.get_option(config, 'Caches', 'file_cache_dir_layout')
        out_dict['cache_image_format'] = self.get_option(config, 'Caches', 'cache_image_format')

        out_dict['default_projection'] = self.get_option(config, 'Services', 'default_projections')
        out_dict['max_repeat'] = self.get_option(config, 'Services', 'max_repeat')
        out_dict['on_error'] = self.get_option(config, 'Services', 'skip_failed_tiles')

        out_dict['strict_validation'] = self.get_option(config, 'Security', 'strict_validation')

        out_dict['log_level'] = self.get_option(config, 'Features', 'log_level')

        out_dict['default_basemap_title'] = self.get_option(config, 'BaseLayers', 'default_basemap_title')
        out_dict['default_basemap_url'] = self.get_option(config, 'BaseLayers', 'default_basemap_url')
        out_dict['default_basemap_max_zoom_level'] = self.get_option(config, 'BaseLayers',
                                                                     'default_basemap_max_zoom_level')

        out_dict = {key: value for key, value in out_dict.iteritems() if value is not None}
        model = models.Config.objects.create(**out_dict)
        self.set_logging(model)
        return model

    def get_option(self, config, section, option):
        """
        Uses try block to get an option from the config. logs error and defaults if there's no value
        :param config: config parser object
        :param section: section to get
        :param option: option to get
        :return: value from config
        """
        try:
            if section in ['Directories', 'Files']:
                config_string = config.get(section, option)
                return config_string.format(
                    GPEP_HOME=self.GPEP_HOME, PYTHON_HOME=self.PYTHON_HOME,
                    APP_EXTENSION=self.APP_EXTENSION, SEP=os.sep)
            if option in ['import-cache', 'packaging']:
                return config.getboolean(section, option)
            else:
                return config.get(section, option)
        except ConfigParser.NoOptionError:
            return None

    def set_logging(self, model):
        """
        Disable all logging levels below the desired level
        """
        log_level_name = model.log_level.upper()
        log_level_int = logging.getLevelName(log_level_name)
        # numeric values of levels are 0 (notset), 10 (debug), 20 (info), 30 (warning), 40 (error) & 50 (critical)
        if log_level_int > 0:
            log_level_int -= 10
        logging.disable(log_level_int)


def findAndReplaceValues(d, find, replace):
    """
    Recursively searches the first parameter, d, for all string values within, and does a find and replace operation on
    each of those strings with the second and third parameters. All instances matching find in the string will be
    replaced. d can be a dictionary or a list.
    :param: d: The input list or dictionary.
    :param: find: The string to be searched for. All matches in all strings will be replaced.
    :param: replace: The string to replace found instances of the pattern with.
    :return: d, but with all the replacements performed.
    """
    if isinstance(d, dict):
        for k, v in d.iteritems():
            d[k] = findAndReplaceValues(v, find, replace)
    elif isinstance(d, list):
        for i in range(0, len(d)):
            d[i] = findAndReplaceValues(d[i], find, replace)
    elif isinstance(d, basestring):
        d = d.replace(find, replace)
    return d


def make_generic_paths(yaml_path):
    """
    Replaces paths in the yaml file where it points to this program's storage location with generic paths.
    Necessary to use yaml load/dump with a variable with a colon, so that yaml will wrap this string in quotes
    which is necessary because of issues between linux and windows.
    :param yaml_path: the yaml file that you want to apply the replacements to
    :return: the text of the yaml file with the replacements
    """
    try:
        with open(yaml_path, 'r') as yaml_file:
            updated_yaml = findAndReplaceValues(yaml.load(yaml_file), ConfigModelCreator().GPEP_HOME, '<GPEP:HOME>')
            return yaml.safe_dump(updated_yaml)
    except IOError:
        log = logging.getLogger(os.path.splitext(os.path.basename(yaml_path))[0])
        log.error('Error: could not open service yaml file {}'.format(yaml_path))


def get_frontend_static_resource_path_development():
    """
    :return: Directory path to where the frontend files are, in a dev environment.
    """
    return os.path.realpath(os.path.dirname(__file__) + "/../../../../Client/app")


def get_frontend_static_resource_path_production():
    """
    :return: Directory path to static resource folder, where frontend files are served from when installed on Windows.
        This is on a production environment.
    """
    return os.path.realpath(os.path.dirname(__file__) + "/../static/app")
