import unittest
import shutil
import tempfile
import os
import sqlite3
from sys import platform

from gpep_apps.jobs import feature_drivers


# only run the feature tests on Windows
@unittest.skipUnless(platform.startswith('win'), 'feature_drivers.py currently only work on Windows.')
class FeatureDriversTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        """Used to pull down data from wfs to spatialite db once.  Then, we use that spatialite db for all our tests
        for bandwidth efficiency."""
        cls.READONLY_SPATIALITE_FILEPATH = os.path.join(tempfile.mkdtemp(), 'tmp.sqlite')
        cls.READONLY_SPATIALITE_DATASOURCE = feature_drivers.DataSource()  # only used to read from, never written to
        cls.populate_readonly_spatialite_from_wfs()
        cls.USA_STATES_LAYER_NAMES = {'usa:states': 'usa:states'}

    def setUp(self):
        self.test_dir = tempfile.mkdtemp()
        self.GPKG_FILEPATH = os.path.join(self.test_dir, 'tmp.gpkg')  # temporary file for geopackage testing
        self.SQLITE_FILEPATH = os.path.join(self.test_dir, 'tmp.sqlite')  # temporary file for spatialite testing

        self.source_datasource = feature_drivers.DataSource()
        self.dest_datasource = feature_drivers.DataSource()

    def tearDown(self):
        self.source_datasource.close()
        self.dest_datasource.close()
        shutil.rmtree(self.test_dir, ignore_errors=True)

    @classmethod
    def tearDownClass(cls):
        cls.READONLY_SPATIALITE_DATASOURCE.close()
        os.remove(cls.READONLY_SPATIALITE_FILEPATH)

    @classmethod
    def populate_readonly_spatialite_from_wfs(cls):
        """
        Initiatally populate spatialite database.  This way, we only have to pull from wfs once, saving
        bandwidth and time.
        """
        wfs_datasource = feature_drivers.DataSource()
        wfs_datasource.openWFS('https://demo.boundlessgeo.com/geoserver/ows')
        cls.READONLY_SPATIALITE_DATASOURCE.openSpatiaLite(cls.READONLY_SPATIALITE_FILEPATH)
        wfs_datasource.write_to(cls.READONLY_SPATIALITE_DATASOURCE, 4326, {'usa:states': 'usa:states'})

    def create_usa_gpkg_from_spatialite(self, layers_names={'usa:states': 'usa:states'}, bbox=None, epsg=4326):
        """Creates a gpkg database populate with vector USA states from a spatialite database"""
        self.source_datasource.openSpatiaLite(self.READONLY_SPATIALITE_FILEPATH)
        self.dest_datasource.openGPKG(self.GPKG_FILEPATH)
        self.source_datasource.write_to(self.dest_datasource, epsg, layer_names=layers_names, bbox=bbox)

    def verify_all_states_present_in_sqlite_database(self, filepath):
        """Ensure all 50 states in sqlite database"""
        with sqlite3.connect(filepath) as db:
            cur = db.execute('SELECT state_abbr FROM \'usa:states\' ORDER BY state_abbr')
            results = cur.fetchall()
            self.assertEquals(len(results), 52)  # usa has 52 states

            # ensure all unique state abbreviation codes are present, 52 rows in sqlite database, one for each states
            states = ''.join([state[0] for state in results])
            expected_states = u'AKALARAZCACOCTDCDEFLGAHIIAIDILINKSKYLAMAMDMEMIMNMOMSMTNCNDNENHNJNMNVNYOHOKORPAPRRISCS' \
                              u'DTNTXUTVAVTWAWIWVWY'
            self.assertEquals(states, expected_states)

    def test_wfs_to_spatialite(self):
        """Tests writing from spatialite to gpkg file"""
        # all the states should be written out already to the readonly spatialite database
        self.verify_all_states_present_in_sqlite_database(self.READONLY_SPATIALITE_FILEPATH)

    def test_spatialite_to_gpkg(self):
        """Tests writing from spatialite to gpkg file"""
        self.create_usa_gpkg_from_spatialite()
        self.verify_all_states_present_in_sqlite_database(self.GPKG_FILEPATH)

        with sqlite3.connect(self.GPKG_FILEPATH) as db:
            cur = db.execute('SELECT table_name, data_type FROM gpkg_contents WHERE table_name=\'usa:states\'')
            results = cur.fetchall()
            self.assertEquals(results, [('usa:states', 'features')])

    def test_open_bad_and_good_datasources(self):
        """Calling is_opened after opening bad datasource should return false"""

        # bad wfs
        self.source_datasource.openWFS('https://demo.boundlessgeo.com/SILKY_DEEP_PURPLE')
        self.assertFalse(self.source_datasource.is_opened())

        # good wfs
        self.source_datasource.openWFS('https://demo.boundlessgeo.com/geoserver/ows')
        self.assertTrue(self.source_datasource.is_opened())

    def test_renaming_usa_layers(self):
        """Create a GeoPackage where the names have been changed"""
        self.create_usa_gpkg_from_spatialite({'usa:states': 'whoah_buddy'})
        self.create_usa_gpkg_from_spatialite({'usa:states': 'whoah_pal'})

        with sqlite3.connect(self.GPKG_FILEPATH) as db:
            cur = db.execute('SELECT table_name FROM gpkg_contents')
            results = cur.fetchall()
            self.assertEquals(results, [('whoah_buddy',), ('whoah_pal',)])

    def test_create_gpkg_with_bounds(self):
        """Create a geopackage with features only in a certain area"""
        self.create_usa_gpkg_from_spatialite(bbox={'north': 40, 'south': 33, 'east': -77, 'west': -82})

        with sqlite3.connect(self.GPKG_FILEPATH) as db:
            cur = db.execute('SELECT state_abbr FROM \'usa:states\'')
            results = cur.fetchall()

            # we only target 11 states on the east coast
            self.assertEquals(len(results), 11)

    def test_create_gpkg_reprojected(self):
        self.create_usa_gpkg_from_spatialite(bbox={'north': 40, 'south': 33, 'east': -77, 'west': -82}, epsg=3857)

        with sqlite3.connect(self.GPKG_FILEPATH) as db:
            cur = db.execute('SELECT srs_id FROM gpkg_contents')

            # ensure epsg is 3857
            self.assertEquals(cur.fetchall(), [(3857,)])

            # ensure all 11 states on the east coast as specified by bounding box are still written out
            cur.execute('SELECT Count(*) FROM \'usa:states\'')
            self.assertEquals(cur.fetchall(), [(11,)])

    def test_create_then_read_geojson(self):
        """Tests that geojson can be written to and read from.

        Writes features into a GeoJSON file, then transfers data from that into a SpatiaLite database. That
        spatialite database is then checked for all features originally written into the GeoJson."""
        geojson_filepath = os.path.join(tempfile.mkdtemp(), 'geo.json')

        # create geojson
        self.source_datasource.openSpatiaLite(self.READONLY_SPATIALITE_FILEPATH)
        self.dest_datasource.openGeoJSON(geojson_filepath)
        self.source_datasource.write_to(self.dest_datasource, 4326, layer_names=self.USA_STATES_LAYER_NAMES)

        self.source_datasource.close()
        self.dest_datasource.close()

        # transfer data from geojson back into a spatialite database...
        self.source_datasource.openGeoJSON(geojson_filepath)
        self.dest_datasource.openSpatiaLite(self.SQLITE_FILEPATH)
        self.source_datasource.write_to(self.dest_datasource, 4326, layer_names=self.USA_STATES_LAYER_NAMES)

        # ...and ensure all the states are written back out
        self.verify_all_states_present_in_sqlite_database(self.SQLITE_FILEPATH)

        self.source_datasource.close()
        self.dest_datasource.close()
        os.remove(geojson_filepath)
