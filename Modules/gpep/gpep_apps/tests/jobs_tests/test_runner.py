"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import mock

from gpep_apps.jobs import runner
from gpep_apps.tests import test_utils

from django.test import override_settings


class TestRunner(test_utils.JobTest):
    def test_get_task_bad_id(self):
        with self.assertRaises(ValueError):
            runner.get_task(91577765)

    def test_get_task(self):
        task = self.create_task()
        return_value = runner.get_task(task.id)
        self.assertEqual(task, return_value)

    @override_settings(CELERY_ALWAYS_EAGER=True)
    def test_cancel_job_no_tasks(self):
        """Attempt to cancel a job without any tasks."""
        job = self.create_job()
        self.assertEqual(0, len(job.task_set.all()))
        runner.cancel_job(job.id)

    @override_settings(CELERY_ALWAYS_EAGER=True)
    @mock.patch('celery.app.control.Control.revoke')
    def test_cancel_task(self, revoke_mock):
        task = self.create_task()
        task.finished = False
        task.save()
        with mock.patch('logging.Logger.debug'):
            runner.cancel_task(task.id)
        task.refresh_from_db()
        self.assertEqual('canceled', task.status)

    @override_settings(CELERY_ALWAYS_EAGER=True)
    def test_cancel_task_bad_id(self):
        """Tests attempting to cancel a task with a bad id."""
        with self.assertRaises(ValueError):
            runner.cancel_task(12345678123)

    @override_settings(CELERY_ALWAYS_EAGER=True)
    def test_cancel_finished_task(self):
        """Test canceling a finished task."""
        task = self.create_task()
        task.finished = True
        task.save()
        runner.cancel_task(task.id)
        task.refresh_from_db()
        self.assertNotEqual('canceled', task.status)
