"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import mock
import os

from gpep_apps.jobs import models
from gpep_apps.tests import test_utils
from shutil import copyfile


class ModelsTest(test_utils.CacheManagerTest):
    def setUp(self):
        super(ModelsTest, self).setUp()

        self.map_proxy_app = test_utils.generate_map_proxy_app('nc')
        self.seed_base_job = models.Job.objects.create(job_type='seed')
        self.seed_job = models.Seed.objects.create(job=self.seed_base_job)

        self.package_base_job = models.Job.objects.create(job_type='package')
        self.package_job = models.Package.objects.create(
            job=self.package_base_job, package_type='NSG_GEODETIC', fetch_tiles=False, srs=[])

        self.clean_base_job = models.Job.objects.create(job_type='clean')
        self.clean_job = models.Clean.objects.create(job=self.clean_base_job, date='2016-10-10')

        self.unfinished_job = models.Job.objects.create(job_type='seed')

    def test_job_to_str(self):
        self.assertEqual('{}: clean'.format(self.clean_base_job.id), str(self.clean_base_job))

    def test_get_related_object(self):
        self.assertEqual(self.seed_job, self.seed_base_job.get_related_object())
        self.assertEqual(self.package_job, self.package_base_job.get_related_object())
        self.assertEqual(self.clean_job, self.clean_base_job.get_related_object())
        self.assertIsNone(self.unfinished_job.get_related_object())

    def test_get_map_proxy_apps(self):
        self.assertEqual(0, len(self.seed_base_job.get_map_proxy_apps()))

    def test_get_task_progress_percentage_no_log_file(self):
        no_log_file_task = models.Task.objects.create(
            job=self.package_base_job, map_proxy_app=self.map_proxy_app, finished=False)
        no_log_file_task.progress_log_file = ''
        no_log_file_task.save()
        self.assertEqual({'percent': 0, 'eta': ''}, no_log_file_task.get_progress())

    def test_get_task_progress_percentage_marked_finished(self):
        finished_task = models.Task.objects.create(
            job=self.package_base_job, map_proxy_app=self.map_proxy_app, finished=True)
        self.assertEqual({'percent': 100, 'eta': ''}, finished_task.get_progress())

    def test_get_task_progress_percentage_log_finished(self):
        # finished field now altered by the celery task, not task.get_progress()
        finished_task = models.Task.objects.create(
            job=self.package_base_job, map_proxy_app=self.map_proxy_app, finished=True)
        copyfile(test_utils.COMPLETED_LOG_FILE, finished_task.progress_log_file)
        self.assertEqual({'percent': 100, 'eta': ''}, finished_task.get_progress())
        self.assertTrue(finished_task.finished)

    def test_get_task_progress_percentage_in_progress(self):
        in_progress_task = models.Task.objects.create(
            job=self.package_base_job, map_proxy_app=self.map_proxy_app, finished=False)
        copyfile(test_utils.IN_PROGRESS_LOG_FILE, in_progress_task.progress_log_file)
        self.assertEqual(test_utils.IN_PROGRESS_LOG_COMPLETE_PERCENTAGE, in_progress_task.get_progress()['percent'])
        self.assertEqual("inProgress", self.package_base_job.get_status())

    def test_get_task_progress_percentage_na_eta(self):
        in_progress_task = models.Task.objects.create(
            job=self.package_base_job, map_proxy_app=self.map_proxy_app, finished=False)
        with open(in_progress_task.progress_log_file, 'w') as log_file:
            log_file.write('\n\n[12:32:03]  0  57.27% -81.56250, 34.30714, -78.75000,'
                           ' 36.59789 (8 tiles) ETA: N/A\n')
        progress = in_progress_task.get_progress()
        self.assertEqual(test_utils.IN_PROGRESS_LOG_COMPLETE_PERCENTAGE, progress['percent'])
        self.assertEqual('', progress['eta'])

    def test_task_delete_result_filename_exists(self):
        task = models.Task.objects.create(
            job=self.package_base_job, map_proxy_app=self.map_proxy_app, finished=True)
        result_filename = os.path.join(self.config_mock.output_dir, 'result.txt')
        task.result_filename = result_filename
        task.save()
        with open(task.result_filename, 'w') as result:
            result.write('this is the result')
        self.assertTrue(os.path.exists(task.result_filename))
        task.delete()
        self.assertFalse(os.path.exists(task.result_filename))

    @mock.patch("{}.open".format(models.__name__))
    def test_get_log_files_unable_to_open_files(self, open_mock):
        open_mock.side_effect = IOError
        task = models.Task.objects.create(job=self.package_base_job, map_proxy_app=self.map_proxy_app)
        self.assertTrue(open_mock.called)
        self.assertEqual('', task.progress_file)
        self.assertEqual('', task.progress_log_file)
