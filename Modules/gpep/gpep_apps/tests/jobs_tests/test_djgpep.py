"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import mock
import os
import zipfile

from gpep_apps.api.gpep_exception import GPEPException
from gpep_apps.jobs import djgpep, utils
from gpep_apps.mpas import models
from gpep_apps.tests import test_utils

from mapproxy.util.ext.wmsparse.parse import WMS_SERVICE_NAME


class DJGPEPTest(test_utils.CacheManagerTest):
    @test_utils.MockConfigCommand(error_message='this is the error text to display')
    def test_add_map_proxy_app_bad_return_val(self, config_command_mock):
        error_text = 'this is the error text to display'
        url = 'this is the url'
        try:
            djgpep.add_map_proxy_app('service_name', url)
            self.fail('APIException should have been raised')
        except GPEPException as e:
            pass
        self.assertIn(url, e.detail)
        self.assertIn(error_text, e.detail)

    @mock.patch("gpep_apps.mpas.models.MapProxyApp.type")
    def test_check_and_update_projections_different_type(self, type_mock):
        """Tests that check and update projections doesn't crash or do anything with an unsupported type."""
        self.create_yaml_model()
        type_mock.return_value = 'not_a_real_type'
        # self.yaml_model.save()
        model_dict = self.yaml_model.to_dict()
        log_mock = mock.Mock()
        djgpep.check_and_update_projections(self.yaml_model, [], log_mock)
        self.assertFalse(log_mock.exception.called)
        self.assertDictContainsSubset(model_dict, self.yaml_model.to_dict())

    def test_check_and_update_projections_no_supported_srses(self):
        """Tests that check and update projections fails when no supported srses."""
        self.create_yaml_model()
        log_mock = mock.Mock()
        with self.assertRaises(ValueError):
            djgpep.check_and_update_projections(self.yaml_model, [], log_mock)
        self.assertTrue(log_mock.error.called)

    def test_add_duplicate_map_proxy_app(self):
        models.MapProxyApp.objects.all().delete()
        with mock.patch('logging.Logger.info'):
            djgpep.add_map_proxy_app('nc', test_utils.NC_URL)
            djgpep.add_map_proxy_app('nc', test_utils.NC_URL)
        self.assertEqual(1, len(models.MapProxyApp.objects.all()))

    @staticmethod
    def create_zip_file(zip_path, num_files=1, num_dirs=1):
        zf = zipfile.ZipFile(zip_path, 'w')
        zip_folder = os.path.dirname(zip_path)
        for dir in range(num_dirs):
            dir_path = os.path.join(zip_folder, 'dir{}'.format(dir))
            os.makedirs(dir_path)
            zf.write(dir_path, 'dir{}'.format(dir))
        for file in range(num_files):
            file_path = os.path.join(zip_folder, '{}.yaml'.format(file))
            with open(file_path, 'a'):
                pass
            zf.write(file_path, '{}.yaml'.format(file))
        zf.close()

    def test_import_pep_malformed_pep_file_no_directories(self):
        models.MapProxyApp.objects.all().delete()
        zip_folder = utils.get_unique_folder(self.config_mock.temp_dir)
        os.mkdir(zip_folder)
        zip_path = os.path.join(zip_folder, 'temp.zip')
        self.create_zip_file(zip_path, 1, 0)
        with zipfile.ZipFile(zip_path) as zip_file:
            try:
                djgpep.check_for_pep_compliance(zip_file)
                self.fail("Must raise a validation error")
            except GPEPException as e:
                self.assertEqual("PEP file must have exactly one directory for a cache", e.detail[0])

    def test_import_pep_malformed_pep_file_multiple_directories(self):
        models.MapProxyApp.objects.all().delete()
        zip_folder = utils.get_unique_folder(self.config_mock.temp_dir)
        os.mkdir(zip_folder)
        zip_path = os.path.join(zip_folder, 'temp.zip')
        self.create_zip_file(zip_path, 1, 2)
        with zipfile.ZipFile(zip_path) as zip_file:
            try:
                djgpep.check_for_pep_compliance(zip_file)
                self.fail("Must raise a validation error")
            except GPEPException as e:
                self.assertEqual("PEP file must have exactly one directory for a cache", e.detail[0])

    def test_import_pep_malformed_pep_file_multiple_yaml_files(self):
        models.MapProxyApp.objects.all().delete()
        zip_folder = utils.get_unique_folder(self.config_mock.temp_dir)
        os.mkdir(zip_folder)
        zip_path = os.path.join(zip_folder, 'temp.zip')
        self.create_zip_file(zip_path, 2, 1)
        with zipfile.ZipFile(zip_path) as zip_file:
            try:
                djgpep.check_for_pep_compliance(zip_file)
                self.fail("Must raise a validation error")
            except GPEPException as e:
                self.assertEqual('PEP file is malformed. There can only be one .yaml file in the root.', e.detail[0])

    def test_get_and_parse_capabilities_remote_wms(self):
        """Test downloading and parsing a well-formed WMS capabilities file that is located remotely."""
        log_mock = mock.Mock()
        cap = djgpep.get_and_parse_capabilities(test_utils.NC_URL, log_mock)
        self.assertIsNotNone(cap)
        log_mock.error.assert_not_called()
        self.assertEqual(cap.service, WMS_SERVICE_NAME)
        self.assertGreater(cap.layers(), 0)

    @mock.patch('mapproxy.script.conf.utils.download_capabilities')
    def test_get_and_parse_capabilities_bad_url(self, download_cap_mock):
        """Test downloading and parsing a capabilities file located at an invalid url."""
        log_mock = mock.Mock()
        cap = djgpep.get_and_parse_capabilities(mock.Mock(), log_mock)
        self.assertIsNone(cap)
        log_mock.error.assert_called()

    @mock.patch('mapproxy.util.ext.wmsparse.parse.parse_capabilities')
    def test_get_and_parse_capabilities_bad_capabilities(self, parse_cap_mock):
        """Test downloading and parsing a malformed capabilities file that is located locally."""
        log_mock = mock.Mock()
        cap = djgpep.get_and_parse_capabilities(test_utils.INVALID_CAP_WT_URL, log_mock)
        self.assertIsNone(cap)
        log_mock.error.assert_called()
