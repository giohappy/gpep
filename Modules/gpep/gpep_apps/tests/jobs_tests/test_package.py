"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

from gpep_apps.jobs import package, utils
from gpep_apps.mpas import model_creator
from gpep_apps.tests import test_utils

import nose


class PackageTest(test_utils.JobTest):
    def test_integration_nc(self):
        self._package_test('nc')

    # TODO: Find WMTS for all tests. Replace 'WMTS_PLACEHOLDER' with the service name.
    @nose.tools.nottest
    def test_integration_WMTS_PLACEHOLDER(self):
        self._package_test('WMTS_PLACEHOLDER')

    def test_update_no_sources(self):
        model_creator.create_map_proxy_app_from_file(test_utils.NC_PATH).get_grids()
        # TODO: Find WMTS for all tests. Replace 'WMTS_PLACEHOLDER' with the service name.
        # model_creator.create_map_proxy_app_from_file(test_utils.WMTS_PLACEHOLDER_PATH).get_grids()

    def test_write_seed_package_file(self):
        self.create_yaml_model()
        boxAndZoom = {'min_x': 0, 'min_y': 1, 'max_x': 0, 'max_y': 1, 'min_zoom': 2, 'max_zoom': 2}
        seed_package_dict = package.create_seed_package_dict(
            {layer.id: layer for layer in self.yaml_model.get_layers()}, self.create_task(self.yaml_model,
                                                                                          **boxAndZoom))

        expected_dict = {
            'coverages': {
                'app__coverage_Orthoimagery_Latest': {
                    'srs': 'EPSG:4326',
                    'bbox': [0.0, 1.0, 0.0, 1.0]
                }
            },
            'seeds': {
                'Orthoimagery_Latest_package': {
                    'coverages': ['app__coverage_Orthoimagery_Latest'],
                    'levels': [2],
                    'caches': [u'Orthoimagery_Latest_cache']
                }
            }
        }
        self.assertEqual(expected_dict, seed_package_dict)

    def test_write_seed_package_file_multiple_service(self):
        self.create_yaml_model()
        seed_dicts = []
        secondary_yaml_model = self.get_yaml_model_secondary()

        box_and_zoom = {'min_x': 0, 'min_y': 1, 'max_x': 0, 'max_y': 1, 'min_zoom': 2, 'max_zoom': 2}
        seed_dicts.append(package.create_seed_package_dict(
            {layer.id: layer for layer in self.yaml_model.get_layers()}, self.create_task(self.yaml_model,
                                                                                          **box_and_zoom)))

        secondary_seed_dict = package.create_seed_package_dict({layer.id: layer for
                                                                layer in secondary_yaml_model.get_layers()},
                                                               self.create_task(secondary_yaml_model, **box_and_zoom))
        seed_dicts.append(secondary_seed_dict)
        merged_dicts = package.merge_dict_lists(seed_dicts)

        expected_dict = {
                            'coverages': {
                                'app__coverage_Orthoimagery_Latest': {
                                    'srs': 'EPSG:4326',
                                    'bbox': [0.0, 1.0, 0.0, 1.0]
                                },
                                'app_secondary__coverage_World_Topo_Map': {
                                    'srs': 'EPSG:4326',
                                    'bbox': [0.0, 1.0, 0.0, 1.0]
                                }
                            },
                            'seeds': {
                                'Orthoimagery_Latest_package': {
                                    'coverages': ['app__coverage_Orthoimagery_Latest'],
                                    'levels': [2],
                                    'caches': ['Orthoimagery_Latest_cache']
                                },
                                'World_Topo_Map_package': {
                                    'coverages': ['app_secondary__coverage_World_Topo_Map'],
                                    'levels': [2],
                                    'caches': ['World_Topo_Map_WMTS']
                                }
                            }
                        }

        self.assertEquals(expected_dict, merged_dicts)

    def test_create_service_dict_no_caches(self):
        """Tests the create_service_dict function with no caches in the map proxy app."""
        self.create_yaml_model()
        for layer in self.yaml_model.get_layers():
            layer.delete_all_caches()
        self.assertEqual(0, len(self.yaml_model.get_caches()))
        package.create_service_dict(self.yaml_model, "NW", "geopackage", "EPSG:4326",
                                    {layer.id: layer for layer in self.yaml_model.get_layers()},
                                    self.create_task(self.yaml_model), utils.get_unique_filename(self.yaml_model.name))
        self.assertEqual(len(self.yaml_model.get_layers()), len(self.yaml_model.get_caches()))

    def test_package_no_sources(self):
        self.create_yaml_model()
        layers = {layer.id: layer for layer in self.yaml_model.get_layers()}
        for layer in layers.values():
            layer.set_source(None)
        self.yaml_model.get_caches().delete()
        self.assertEqual(0, len(self.yaml_model.get_caches()))
        package.create_service_dict(self.yaml_model, "NW", "EPSG:4326", "geopackage", layers,
                                    self.create_task(self.yaml_model),
                                    utils.get_unique_filename(self.yaml_model.name))
        self.assertEqual(0, len(self.yaml_model.get_caches()))
