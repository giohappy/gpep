"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import os
import pep8
import unittest

from gpep_apps.tests import test_utils

BASE_PATH = os.path.dirname(os.path.dirname(__file__))


class TestPEP8(unittest.TestCase):
    @staticmethod
    def get_pep8_errors(files):
        pep8style = pep8.StyleGuide(config_file=test_utils.PEP8_CONFIG_PATH)
        return pep8style.check_files(files).total_errors

    def test_pep8(self):
        errors = 0
        for dirpath, dirnames, filenames in os.walk(BASE_PATH):
            if 'migrations' not in dirpath and 'static' not in dirpath:
                errors += TestPEP8.get_pep8_errors(os.path.join(dirpath, filename) for filename in filenames
                                                   if filename.endswith('.py'))
        self.assertEqual(0, errors, "Found code style errors (and warnings).")
