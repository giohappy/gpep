"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

from gpep_apps.tests import test_utils
from gpep_apps.api import serializers
from gpep_apps.mpas import models

from rest_framework import serializers as rest_serializers


class SerializersTest(test_utils.CacheManagerTest):
    def test_map_proxy_app_serializer(self):
        """Tests map proxy app serializer."""
        models.MapProxyApp.objects.all().delete()
        self.create_yaml_model()
        data = serializers.MapProxyAppSerializer({'name': self.yaml_model.name}).data
        self.assertEqual(self.yaml_model.name, data['name'])

    def test_map_proxy_app_serializer_bad_name(self):
        """Tests map proxy app serializer with a bad name."""
        models.MapProxyApp.objects.all().delete()
        self.create_yaml_model()
        with self.assertRaises(rest_serializers.ValidationError):
            serializers.MapProxyAppSerializer(data={'name': 'invalid_name'}).is_valid(raise_exception=True)

    def test_add_map_proxy_app_serializer(self):
        """Tests add map proxy app serializer."""
        models.MapProxyApp.objects.all().delete()
        ser = serializers.AddMapProxyAppSerializer(
            data={'name': 'nc', 'external_url': test_utils.NC_URL, "enable_cache": True})
        self.assertTrue(ser.is_valid())
        self.assertEqual(test_utils.NC_URL, ser.data['external_url'])

    def test_add_map_proxy_app_serializer_bad_url(self):
        """Test add map proxy app serializer with a bad url."""
        ser = serializers.AddMapProxyAppSerializer(data={'name': 'nc', 'external_url': "fiko;djoeqhjwpfoie5432tgfds,,"})
        self.assertFalse(ser.is_valid())


class JobSerializerTest(test_utils.JobTest):
    """
    Tests the job serializer class
    Helps to be a separate class so it can use create_job
    """

    def test_job_serializer_to_representation_package(self):
        """Test job serializer to_representation with a package job."""
        job = self.create_job('package')
        out_dict = serializers.JobSerializer(job).data
        self.assertEqual(job.id, out_dict['id'])
        self.assertEqual('package', out_dict['job_type'])

    def test_job_serializer_to_representation_clean(self):
        """Test job serializer to_representation with a clean job."""
        job = self.create_job('clean')
        out_dict = serializers.JobSerializer(job).data
        self.assertEqual(job.id, out_dict['id'])
        self.assertEqual('clean', out_dict['job_type'])

    def test_job_serializer_to_representation_seed(self):
        """Test job serializer to_representation with a seed job."""
        job = self.create_job('seed')
        out_dict = serializers.JobSerializer(job).data
        self.assertEqual(job.id, out_dict['id'])
        self.assertEqual('seed', out_dict['job_type'])

    def test_job_serializer_to_representation_export(self):
        """Test job serializer to_representation with a export job."""
        job = self.create_job('export')
        out_dict = serializers.JobSerializer(job).data
        self.assertEqual(job.id, out_dict['id'])
        self.assertEqual('export', out_dict['job_type'])

    def test_job_serializer_get_status(self):
        """Test job serializer get_status with an in-progress job."""
        task = self.create_task(job_type='seed')
        job = task.job
        job.status = 'inProgress'
        job.save()
        serializer = serializers.JobSerializer(job)
        self.assertEqual('inProgress', serializer.get_status(task))

    @staticmethod
    def get_estimate_serializer_params(layer_ids):
        return {
            'layer_ids': layer_ids, 'get_time': False, 'min_zoom': 1, 'max_zoom': 2,
            'min_x': 0, 'max_x': 1, 'min_y': 1, 'max_y': 2
        }

    def test_estimate_serializer_no_layer_ids(self):
        """Test estimate serializer when there are no layer ids."""
        ser = serializers.EstimateSerializer(data=self.get_estimate_serializer_params([]))
        self.assertFalse(ser.is_valid())

    def test_estimate_serializer_duplicate_layer_ids(self):
        """Test estimate serializer when there are duplicate layer ids."""
        self.create_yaml_model()
        id = self.yaml_model.get_layers()[0].id
        ser = serializers.EstimateSerializer(data=self.get_estimate_serializer_params([id, id]))
        self.assertFalse(ser.is_valid())

    def test_estimate_serializer_nonexistant_layer_ids(self):
        """Test estimate serializer with layer ids that do not exist."""
        self.create_yaml_model()
        id = self.yaml_model.get_layers()[0].id
        large_number = 94910212
        assert models.Layer.objects.filter(id=large_number).first() is None
        ser = serializers.EstimateSerializer(data=self.get_estimate_serializer_params([id, large_number]))
        self.assertFalse(ser.is_valid())
