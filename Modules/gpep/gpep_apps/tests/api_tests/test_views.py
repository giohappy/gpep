"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import json
import mock
import os
import shutil
import zipfile
import nose

from django.test import Client, override_settings

from gpep_apps.tests import test_utils
from gpep_apps.jobs import models, seed
from gpep_apps.mpas.models import Cache, MapProxyApp, SourceCommons
from gpep_apps.status.models import Cache as status_Cache_model

from mapproxy.seed.seeder import SeedInterrupted
from django.core.cache import cache as django_cache


@override_settings(CELERY_ALWAYS_EAGER=True)
class ViewTest(test_utils.JobTest):

    def setUp(self):
        self.default_cache_path = os.path.join(os.getcwd(), 'cache_data')
        self.temp_file_path = os.path.join(os.getcwd(), 'temp')
        if os.path.exists(self.default_cache_path):
            shutil.rmtree(self.default_cache_path)
        if os.path.exists(self.temp_file_path):
            os.remove(self.temp_file_path)
        super(ViewTest, self).setUp()

    def tearDown(self):
        if os.path.exists(self.default_cache_path):
            shutil.rmtree(self.default_cache_path)
        if os.path.exists(self.temp_file_path):
            os.remove(self.temp_file_path)
        super(ViewTest, self).tearDown()

    def assertOKStatus(self, response):
        """
        Checks that the status on the response object is 200
        :param response: object from calling Client().post or Client().get
        """
        self.assertStatusEquals(response, status_code=200)

    def assertBadRequest(self, response):
        """
        Checks that the status on the response object is 400
        :param response: object from calling Client().post or Client().get
        """
        self.assertStatusEquals(response, status_code=400)

    def assertStatusEquals(self, response, status_code):
        """
        Asserts that the given response object has the given status_code
        :param response: object from calling Client().post or Client().get
        :param status_code: html integer status code to check for
        """
        if response.status_code == status_code:
            return True
        try:
            print response.json()
        except ValueError:
            pass
        self.assertEqual(status_code, response.status_code)

    def add_service(self, service_url=None, generated_file=None, enable_cache=True, name='test_name',
                    clean_database=True, create_task=False, expected_status_code=200):
        """
        Creates a service and can do many other things while doing that
        :param service_url: url of the service to add
        :param generated_file: file to have mapproxy seem like its creating when adding a service
        :param enable_cache: if caching should be enabled for the service
        :param name: service name for the new service
        :param clean_database: if the database should be cleaned
        :param create_task: if a task should be created for the created model
        :param expected_status_code: what status code to expect when adding the service
        :return: tuple. created_model, response object
        """
        if clean_database:
            MapProxyApp.objects.all().delete()

        assert service_url or generated_file, "Must either specify a url or "
        params = {
            "name": name,
            "external_url": service_url if service_url else test_utils.NC_URL,
            "enable_cache": enable_cache
        }
        with mock.patch('logging.Logger.debug'):
            with mock.patch('logging.Logger.info'):
                response = Client().post('/add_service', json.dumps(params), content_type='application/json')
        self.assertStatusEquals(response, expected_status_code)
        if not clean_database:
            self.assertEqual(1, MapProxyApp.objects.count())
        model = MapProxyApp.objects.all().first()
        if create_task:
            self.create_task(model)
        return model, response

    @nose.tools.nottest
    def test_create_job_seed(self):
        """Tests creating a seed job."""
        model, folder, params = self._get_test_parameters('nc', 'seed')
        with mock.patch('logging.Logger.debug'):
            response = Client().post('/create_job', json.dumps(params), content_type='application/json')
            self.assertOKStatus(response)
            created_model = MapProxyApp.objects.all().last()
        for cache in created_model.get_caches():
            path = os.path.join(self.config_mock.cache_dir, created_model.name, cache.key)
            self.assertTrue(os.path.exists(path))

    @nose.tools.nottest
    def test_create_job_package(self):
        """Tests creating a packaging job."""
        models.Task.objects.all().delete()
        if os.path.exists(self.config_mock.output_dir):
            shutil.rmtree(self.config_mock.output_dir)
        try:
            os.makedirs(self.config_mock.output_dir)
        except OSError:
            print "Could not create output dir"
        model, folder, params = self._get_test_parameters('nc', 'package')
        cache = model.get_caches()[0]
        cache.cache['directory'] = os.path.join(self.config_mock.cache_dir, model.name, cache.key)
        cache.save()
        params['srs'] = 'EPSG:3395'
        with mock.patch('logging.Logger.debug'):
            response = Client().post('/create_job', json.dumps(params), content_type='application/json')
            self.assertOKStatus(response)
            created_model = MapProxyApp.objects.all().last()
        for cache in created_model.get_caches():
            if cache.cache['type'] == 'geopackage':
                self.assertEqual(self.config_mock.output_dir, cache.cache['directory'])
                self.assertTrue(os.path.exists(os.path.join(self.config_mock.output_dir,
                                                            '{}.gpkg'.format(created_model.name))))
            else:
                path = os.path.join(self.config_mock.cache_dir, created_model.name, cache.key)
                self.assertTrue(os.path.exists(path))
        path = os.path.join(self.config_mock.output_dir, '{}.gpkg'.format(created_model.name))
        self.assertTrue(os.path.exists(path))

    def test_create_job_clean(self):
        """Test creating a clean job."""
        model, folder, params = self._get_test_parameters('nc', 'clean')
        with mock.patch('logging.Logger.debug'):
            response = Client().post('/create_job', json.dumps(params), content_type='application/json')
            self.assertOKStatus(response)

    def test_get_jobs_none(self):
        """Tests get jobs when there are no jobs."""
        models.Job.objects.all().delete()
        response = Client().get('/jobs')
        self.assertEqual('[]', response.content)

    def test_get_jobs_one(self):
        """Tests get jobs endpoint with one initialized job."""
        models.Job.objects.all().delete()
        models.Task.objects.all().delete()
        django_cache.clear()

        model, folder, params = self._get_test_parameters('nc', 'seed')
        with mock.patch('logging.Logger.debug'):
            Client().post('/create_job', json.dumps(params), content_type='application/json')
        django_cache.clear()
        response = Client().get('/jobs')
        self.assertEqual(len(response.data), 1)
        self.assertEqual(sorted(['id', 'created_at', 'job_type', 'tasks', 'status', 'max_zoom', 'min_zoom']),
                         sorted(response.data[0].keys()))
        self.assertEqual(sorted(['id', 'start_date', 'progress', 'name', 'status', 'failure_message']),
                         sorted(response.data[0]['tasks'][0].keys()))
        self.assertEqual('nc', response.data[0]['tasks'][0]['name'])

        django_cache.clear()
        response = Client().get('/jobs/{}/'.format(response.data[0]['id']))
        self.assertEqual(sorted(['id', 'created_at', 'job_type', 'tasks', 'status', 'max_zoom', 'min_zoom']),
                         sorted(response.data.keys()))
        self.assertEqual(sorted(['id', 'start_date', 'progress', 'name', 'layers', 'status', 'failure_message']),
                         sorted(response.data['tasks'][0].keys()))
        self.assertEqual(sorted(['id', 'name', 'title', 'max_x', 'max_y', 'min_x', 'min_y', 'max_zoom', 'min_zoom']),
                         sorted(response.data['tasks'][0]['layers'][0].keys()))
        self.assertEqual('nc', response.data['tasks'][0]['name'])

    def test_get_job_bad_job(self):
        """Tests get jobs when there are no jobs."""
        with mock.patch("logging.Logger.warn"):
            response = Client().get('/jobs/1234567', follow=True)
        self.assertStatusEquals(response, 404)
        self.assertEqual('"This job does not exist in GPEP."', response.content)

    def test_size_estimate_one_app(self):
        """Test get size estimate with a valid request."""
        mpa = self.add_service(enable_cache=True, service_url=test_utils.NC_URL)[0]
        model, folder, params = self._get_test_parameters('nc', 'seed')

        with mock.patch('logging.Logger.debug'):
            response = Client().post('/size_estimate', json.dumps(params), content_type='application/json')
        # as long as we use the same service, this should theoretically be a hard number
        self.assertEqual(response.data['num_tiles'], 2)
        # size should be something around 40,000 bytes
        self.assertTrue(10000 < response.data['size'] < 100000)
        self.assertTrue(0 < response.data['time_sec'] < 1)

    def test_size_estimate_cache_disabled(self):
        """Test getting a size estimate when caching is disabled."""
        mpa = self.add_service(service_url=test_utils.NC_URL, enable_cache=False)[0]
        self.assertEqual(1, len(mpa.get_layers()))
        self.assertTrue(mpa.get_layers()[0].get_source().disable_storage)

        model, folder, params = self._get_test_parameters('nc', 'seed')
        response = Client().post('/size_estimate', json.dumps(params), content_type='application/json')

        # Size estimates should work as long as the tiles can be retrieved from a cache and/or source
        self.assertIsNotNone(response)
        self.assertTrue(0 < response.data['time_sec'] < 1)

    def test_add_map_proxy_app(self):
        """Tests the add_map_proxy_app endpoint with a valid request."""
        model = self.add_service(service_url=test_utils.NC_URL)[0]
        # Should be around 3-4 caches for each layer
        self.assertLess(len(model.get_layers()), len(model.get_caches()))
        for layer in model.get_layers():
            self.assertIsInstance(layer.get_source(), Cache)

        for cache in model.get_caches():
            self.assertIsNotNone(cache.grids)
            if not cache.disable_storage:
                self.assertIsNotNone(cache.cache)
                self.assertIn('directory', cache.cache)
                self.assertEqual(os.path.join(self.config_mock.cache_dir, cache.map_proxy_app.name, cache.key),
                                 cache.cache['directory'])
                self.assertIn('type', cache.cache)
                self.assertEqual(self.config_mock.cache_type, cache.cache['type'])

    def test_add_map_proxy_app_twice(self):
        """Tests the add_map_proxy_app endpoint with a valid request twice with the same service_name."""
        duplicate_name = 'duplicate_name'
        self.add_service(service_url=test_utils.NC_URL, name=duplicate_name)
        self.add_service(service_url=test_utils.NC_URL, name=duplicate_name,
                         clean_database=False, expected_status_code=400)
        self.assertEqual(1, MapProxyApp.objects.count())

    def test_add_map_proxy_app_twice_same_name_different_url(self):
        """Tests the add_map_proxy_app endpoint with a valid request twice with the same service_name."""
        self.add_service(name='test_name', service_url=test_utils.NC_URL)
        response = self.add_service(name='test_name', service_url=test_utils.NRL_URL, clean_database=False,
                                    expected_status_code=400)[1]
        self.assertEqual(u'Service with name test_name already exists.', response.data.get('name')[0])
        self.assertEqual(1, MapProxyApp.objects.count())

    def test_add_map_proxy_app_twice_same_url_different_name(self):
        """Tests the add_map_proxy_app endpoint with a valid request twice with duplicate urls."""
        self.add_service(name="test_name", service_url=test_utils.NC_URL)
        response = self.add_service(name="test_name1", service_url=test_utils.NC_URL,
                                    clean_database=False, expected_status_code=400)[1]
        self.assertEqual(u'A Service with that URL has already been added with the name test_name.',
                         response.data.get('external_url')[0])
        self.assertEqual(1, MapProxyApp.objects.count())

    def test_add_map_proxy_app_no_cache(self):
        """Tests the add_map_proxy_app endpoint with a valid request but without caching."""
        model = self.add_service(enable_cache=False, service_url=test_utils.NC_URL)[0]
        for layer in model.get_layers():
            self.assertTrue(layer.get_source().disable_storage)

    def test_add_map_proxy_app_no_cache_wmts(self):
        """Test adding a wmts service with caching disabled."""
        with mock.patch('logging.Logger.error'):
            response = self.add_service(enable_cache=False, service_url=test_utils.ARCGIS_URL,
                                        expected_status_code=400)[1]
        self.assertBadRequest(response)
        self.assertEqual(response.data, 'WMTS services must have caching enabled.')

    def test_add_map_proxy_app_bad_enable_cache_parameter(self):
        """Tests the add_map_proxy_app endpoint with a invalid enable_cache parameter."""
        self.add_service(service_url=test_utils.NC_URL, enable_cache="not_a_boolean_value", expected_status_code=400)

    def test_add_map_proxy_app_then_seed(self):
        """Ensures that the directory and file get set in cache.cache."""
        MapProxyApp.objects.all().delete()
        model, folder, params = self._get_test_parameters('nc', 'clean', endpoint_params=False)
        for cache in model.get_caches():
            cache.cache = None
            cache.save()
        with mock.patch('gpep_apps.jobs.runner.seed_cache.delay') as seed_cache_mock:
            seed_cache_mock.return_value.id = 0
            seed.start_job(model, model.get_layers(), 2, self.create_task(model))
            args = seed_cache_mock.call_args[0]
        for key, value in args[1]['caches'].iteritems():
            self.assertIn('cache', value)
            self.assertIsNotNone(value['cache'])
            self.assertIn('directory', value['cache'])
            self.assertIn('type', value['cache'])
            self.assertEqual('file', value['cache'].get('type'))

    def test_add_map_proxy_app_bad_url(self):
        """Ensures that a 400 error is returned when a bad external url is passed to add_map_proxy_app."""
        self.add_service(service_url="bad_url_goes_here", expected_status_code=400)

    @mock.patch("celery.app.control.Control.revoke")
    def test_delete_map_proxy_app(self, celery_revoke_mock):
        """Tests deleting of a map_proxy_app by hitting the endpoint."""
        self.add_service(service_url=test_utils.NC_URL)
        params = {
            "name": MapProxyApp.objects.all().first().name,
        }
        with mock.patch('logging.Logger.debug'):
            response = Client().post('/delete_map_proxy_app', json.dumps(params), content_type='application/json')
        self.assertOKStatus(response)

    @mock.patch("celery.app.control.Control.revoke")
    def test_delete_map_proxy_app_after_adding_job(self, celery_revoke_mock):
        """Tests deleting of a map_proxy_app by hitting the endpoint."""
        model, folder, params = self._get_test_parameters('nc', 'seed')
        model_name = model.name
        model.file_location = ''
        model.save()
        with mock.patch('gpep_apps.jobs.runner.seed_cache.delay') as seed_mock:
            seed_mock.return_value.id = 0
            response = Client().post('/create_job', json.dumps(params), content_type='application/json')
        self.assertOKStatus(response)
        with mock.patch('logging.Logger.debug'):
            response = Client().post('/delete_map_proxy_app', json.dumps({'name': model_name}),
                                     content_type='application/json')
        self.assertOKStatus(response)
        self.assertEqual(0, len(MapProxyApp.objects.filter(name=model_name)))

    def test_cache_space_available(self):
        """Test that the right things are being returned when getting cache_size."""
        c = status_Cache_model(used_space=5, free_space=5, runtime_seconds=5)
        c.save()

        response = Client().get('/cache_size')
        self.assertOKStatus(response)

        response_dict = response.content
        self.assertIn('size', response_dict)
        self.assertIn('free_space', response_dict)

    def test_cache_space_available_with_id(self):
        """Tests getting cache space available when specifying an id."""
        c = status_Cache_model(used_space=5, free_space=5, runtime_seconds=5)
        c.save()

        MapProxyApp.objects.all().delete()
        self.create_yaml_model()
        response = Client().get('/cache_size?map_proxy_app_id={}'.format(MapProxyApp.objects.all().first().id))
        self.assertOKStatus(response)

        response_dict = response.content
        self.assertIn('size', response_dict)
        self.assertIn('free_space', response_dict)

    @mock.patch('gpep_apps.jobs.models.Job.get_status')
    def test_package_job_available_for_download(self, status_mock):
        """Ensures that 200 is returned when the job is ready."""
        status_mock.return_value = 'complete'
        self.create_yaml_model()
        job = self.create_job('seed')
        filename = 'test.gpkg'
        job.result_filename = os.path.join(self.config_mock.output_dir, filename)
        job.save()
        print job.get_status()
        with open(job.result_filename, 'w') as target_for_download:
            target_for_download.write('')
        response = Client().get('/job_available_for_download?job_id={}&filename={}'.format(job.id, filename),
                                content_type='application/json')
        self.assertOKStatus(response)
        os.remove(job.result_filename)

    @mock.patch('gpep_apps.jobs.models.Task.get_progress')
    def test_package_task_available_for_download(self, progress_mock):
        """Ensures that 200 is returned when the task is ready."""
        progress_mock.return_value = {'percent': 100, 'eta': ''}
        self.create_yaml_model()
        task = self.create_task(self.yaml_model, 'seed')
        filename = 'test.gpkg'
        task.result_filename = os.path.join(self.config_mock.output_dir, filename)
        task.save()
        with open(task.result_filename, 'w') as target_for_download:
            target_for_download.write('')
        response = Client().get('/task_available_for_download?task_id={}&filename={}'.format(task.id, filename),
                                content_type='application/json')
        self.assertOKStatus(response)
        os.remove(task.result_filename)

    @mock.patch('gpep_apps.jobs.models.Job.get_status')
    def test_package_job_available_for_download_not_finished(self, status_mock):
        """Ensures that a error is returned when the job isn't ready."""
        status_mock.return_value = 'inProgress'
        self.create_yaml_model()
        job = self.create_job('seed')
        response = Client().get('/job_available_for_download?job_id={}&filename=test.gpkg'.format(job.id),
                                content_type='application/json')
        self.assertBadRequest(response)

    @mock.patch('gpep_apps.jobs.models.Task.get_progress')
    def test_package_task_available_for_download_not_finished(self, progress_mock):
        """Ensures that a error is returned when the task isn't ready."""
        progress_mock.return_value = {'percent': 25, 'eta': ''}
        self.create_yaml_model()
        task = self.create_task(self.yaml_model, 'seed')
        response = Client().get('/task_available_for_download?task_id={}&filename=test.gpkg'.format(task.id),
                                content_type='application/json')
        self.assertBadRequest(response)

    def test_package_job_available_for_download_bad_task_id(self):
        """Ensures that a 400 error is returned when the job id doesn't correspond with a real task."""
        response = Client().get('/job_available_for_download?job_id=8912388312&filename=test.gpkg',
                                content_type='application/json')
        self.assertBadRequest(response)

    def test_package_task_available_for_download_bad_task_id(self):
        """Ensures that a 400 error is returned when the task id doesn't correspond with a real task."""
        response = Client().get('/task_available_for_download?task_id=8912388312&filename=test.gpkg',
                                content_type='application/json')
        self.assertBadRequest(response)

    def test_download_job_package(self):
        """Ensures that 200 is returned when the job is ready."""
        self.create_yaml_model()
        job = self.create_job('seed')
        result_filename = os.path.join(self.config_mock.output_dir, 'test.txt')
        job.result_filename = result_filename
        with open(result_filename, mode='a'):
            pass
        job.finished = True
        job.status = 'complete'
        job.save()
        response = Client().get('/download_job?job_id={}&filename=test.gpkg'.format(job.id),
                                content_type='application/json')
        self.assertOKStatus(response)

    def test_download_task_package(self):
        """Ensures that 200 is returned when the task is ready."""
        self.create_yaml_model()
        task = self.create_task(self.yaml_model, 'seed')
        result_filename = os.path.join(self.config_mock.output_dir, 'test.txt')
        task.result_filename = result_filename
        with open(result_filename, mode='a'):
            pass
        task.finished = True
        task.save()
        response = Client().get('/download_task?task_id={}&filename=test.gpkg'.format(task.id),
                                content_type='application/json')
        self.assertOKStatus(response)

    @mock.patch('gpep_apps.jobs.models.Job.get_status')
    def test_download_job_package_not_finished(self, status_mock):
        """Ensures that a error is returned when the job isn't ready."""
        status_mock.return_value = 'inProgress'
        self.create_yaml_model()
        job = self.create_job('seed')
        response = Client().get('/download_job?job_id={}&filename=test.gpkg'.format(job.id),
                                content_type='application/json')
        self.assertBadRequest(response)

    @mock.patch('gpep_apps.jobs.models.Task.get_progress')
    def test_download_task_package_not_finished(self, progress_mock):
        """Ensures that a error is returned when the task isn't ready."""
        progress_mock.return_value = {'percent': 25, 'eta': ''}
        self.create_yaml_model()
        task = self.create_task(self.yaml_model, 'seed')
        response = Client().get('/download_task?task_id={}&filename=test.gpkg'.format(task.id),
                                content_type='application/json')
        self.assertBadRequest(response)

    def test_download_package_bad_job_id(self):
        """Ensures that a 400 error is returned when the task id doesn't correspond with a real job."""
        very_high_number = 8912388312
        # checking that a model doesn't actually exist with that id
        self.assertEqual(0, len(models.Task.objects.filter(id=very_high_number)))
        response = Client().get(
            '/job_available_for_download?job_id={}&filename=test.gpkg'.format(very_high_number),
            content_type='application/json')
        self.assertBadRequest(response)

    def test_download_package_bad_task_id(self):
        """Ensures that a 400 error is returned when the task id doesn't correspond with a real task."""
        very_high_number = 8912388312
        # checking that a model doesn't actually exist with that id
        self.assertEqual(0, len(models.Task.objects.filter(id=very_high_number)))
        response = Client().get(
            '/task_available_for_download?task_id={}&filename=test.gpkg'.format(very_high_number),
            content_type='application/json')
        self.assertBadRequest(response)

    def test_map_proxy_app_list_none(self):
        """Tests getting the map proxy app list when there are none."""
        MapProxyApp.objects.all().delete()
        response = Client().get('/service_list')
        self.assertOKStatus(response)
        self.assertEqual('[]', response.content)

    def test_map_proxy_app_list_one(self):
        """Tests getting the map proxy app list then there is one."""
        MapProxyApp.objects.all().delete()
        self.create_yaml_model()
        django_cache.clear()
        response = Client().get('/service_list')
        self.assertOKStatus(response)
        response_obj = response.json()
        self.assertEqual(1, len(response_obj))
        self.assertEqual(self.yaml_model.name, response_obj[0].get('name'))
        self.assertIsInstance(response_obj[0].get('layers'), list)
        self.assertEqual(1, len(response_obj[0].get('layers')))
        self.assertIsInstance(response_obj[0].get('layers')[0], dict)
        self.assertEqual([-84.446505, 33.702159, -75.406671, 36.634762], response_obj[0].get('layers')[0]['bbox'])

    def test_get_map_proxy_app_valid(self):
        """Test get map proxy app with a valid request."""
        MapProxyApp.objects.all().delete()
        self.create_yaml_model()
        response = Client().get('/get_map_proxy_app?service_name={}'.format(self.yaml_model.name))
        self.assertOKStatus(response)
        self.assert_dicts_equal(self.yaml_model.to_dict(), response.json())

    def test_get_map_proxy_app_invalid_name(self):
        """Test get map proxy app with an invalid name."""
        MapProxyApp.objects.all().delete()
        self.create_yaml_model()
        response = Client().get('/get_map_proxy_app?service_name=invalid_name')
        self.assertBadRequest(response)

    def test_get_map_proxy_app_no_name(self):
        """Test get map proxy app without a name."""
        MapProxyApp.objects.all().delete()
        self.create_yaml_model()
        response = Client().get('/get_map_proxy_app')
        self.assertBadRequest(response)

    def test_create_job_with_layer_without_source(self):
        """Test that when you create a job, all the layers you start the job on have at least one source."""
        model, folder, params = self._get_test_parameters('nc', 'seed')
        layer = model.get_layers()[0]
        layer.set_source(None)
        layer.save()
        response = Client().post('/create_job', json.dumps(params), content_type='application/json')
        self.assertBadRequest(response)

    def setup_export(self, exists=False):
        """
        setup method for tests testing exporting
        creates an app and seeding some tiles to export
        :return: the MapProxyApp model
        """
        MapProxyApp.objects.all().delete()
        models.Job.objects.all().delete()
        self.assertEqual(len(MapProxyApp.objects.all()), 0)
        shutil.copyfile(test_utils.NC_PATH, os.path.join(self.config_mock.app_dir, 'nc.yaml'))
        model, folder, params = self._get_test_parameters('nc', 'seed')
        with mock.patch('logging.Logger.debug'):
            response = Client().post('/create_job', json.dumps(params), content_type='application/json')
            self.assertOKStatus(response)
            export_params = {'job_type': 'export', 'services': [{'id': model.id}]}
            response = Client().post('/create_job', json.dumps(export_params), content_type='application/json')
            self.assertOKStatus(response)
        return model

    @nose.tools.nottest
    def test_export_with_progress(self):
        """Tests the .pep file created from an export job."""
        model = self.setup_export()
        pep_file_path = os.path.join(self.config_mock.output_dir, 'nc.pep')
        self.assertTrue(os.path.exists(pep_file_path))

        layer_paths = [os.path.join('nc', layer_name) for layer_name in
                       model.get_layers().values_list('name', flat=True).distinct()]

        with zipfile.ZipFile(pep_file_path) as zipped_file:
            for path in zipped_file.namelist():
                fixed_path = os.path.relpath(path=path)
                if os.sep in fixed_path:
                    passed = False
                    for layer_path in layer_paths:
                        if layer_path in fixed_path:
                            passed = True
                            break
                    self.assertTrue(passed, "Not expecting folder {}".format(fixed_path))
                else:
                    self.assertEqual('nc.yaml', path)
        job = models.Job.objects.get(job_type='export')
        self.assertEqual(100, job.task_set.all().first().get_progress()['percent'])

    def test_job_download_available_pep(self):
        """Test if pep is available for job download."""
        self.setup_export()
        job = models.Job.objects.get(job_type='export')
        job.result_filename = os.path.join(self.config_mock.output_dir, 'nc.pep')
        job.finished = True  # mark job as finished
        job.status = 'complete'
        job.save()
        self.assertTrue(os.path.isfile(job.result_filename))
        self.assertEqual('complete', job.get_status())
        response = Client().get('/job_available_for_download?job_id={}&filename={}'
                                .format(job.id, job.result_filename),
                                content_type='application/json')
        self.assertOKStatus(response)

    def test_task_download_available_pep(self):
        """Test if pep is available for task download."""
        self.setup_export()
        task = models.Job.objects.get(job_type='export').task_set.all().first()
        task.result_filename = os.path.join(self.config_mock.output_dir, 'nc.pep')
        task.finished = True  # mark task as finished
        task.save()
        self.assertTrue(os.path.isfile(task.result_filename))
        self.assertEqual(100, task.get_progress()['percent'])
        response = Client().get('/task_available_for_download?task_id={}&filename={}'
                                .format(task.id, task.result_filename),
                                content_type='application/json')
        self.assertOKStatus(response)

    @mock.patch("celery.app.control.Control.revoke")
    def test_delete_task(self, celery_revoke_mock):
        """Tests deleting a finished task."""
        MapProxyApp.objects.all().delete()
        self.create_yaml_model()
        target_for_deletion = self.create_task(self.yaml_model, 'seed')
        target_for_deletion.finished = True
        target_for_deletion.save()
        response = Client().post('/delete_task', {'id': target_for_deletion.id})
        self.assertOKStatus(response)
        # the delete_task endpoint returns True upon success
        self.assertTrue(response.data)

    @mock.patch("celery.app.control.Control.revoke")
    def test_delete_task_bad_filename(self, celery_revoke_mock):
        """Tests delete a task when it is pointing to a bad filename."""
        MapProxyApp.objects.all().delete()
        self.create_yaml_model()
        target_for_deletion = self.create_task(self.yaml_model, 'seed')
        target_for_deletion.finished = True
        target_for_deletion.result_filename = 'this is not a real filename'
        target_for_deletion.save()
        response = Client().post('/delete_task', {'id': target_for_deletion.id})
        self.assertOKStatus(response)

    @mock.patch("celery.app.control.Control.revoke")
    def test_delete_job(self, celery_revoke_mock):
        """Tests deleting a job with a valid request."""
        MapProxyApp.objects.all().delete()
        self.create_yaml_model()
        target_for_deletion = self.create_task(self.yaml_model, 'seed')
        target_for_deletion.finished = True
        target_for_deletion.save()
        response = Client().post('/delete_job', {'id': target_for_deletion.job.id})
        self.assertTrue(response.data)
        self.assertOKStatus(response)

    @mock.patch("celery.app.control.Control.revoke")
    def test_delete_job_not_finished(self, celery_revoke_mock):
        """Tests deleting a job when it is not finished."""
        MapProxyApp.objects.all().delete()
        self.create_yaml_model()
        target_for_deletion = self.create_task(self.yaml_model, 'seed')
        target_for_deletion.finished = False
        target_for_deletion.save()
        response = Client().post('/delete_job', {'id': target_for_deletion.job.id})
        self.assertTrue(response.data)
        self.assertOKStatus(response)

    def test_add_geopackage_service(self):
        MapProxyApp.objects.all().delete()
        with open(test_utils.NC_GEOPACKAGE, 'rb') as f:
            response = Client().post('/add_geopackage_service', {'add_geopackage_service': f})
        self.assertOKStatus(response)
        self.assertEqual(1, MapProxyApp.objects.count())
        mpa = MapProxyApp.objects.all().first()
        self.assertEqual(1, len(mpa.get_caches()))
        geopackage_location = os.path.join(
            self.config_mock.cache_dir, 'add_geopackage_service', 'add_geopackage_service.gpkg')
        self.assertTrue(os.path.exists(geopackage_location))
        self.assertFalse(mpa.is_copy)
        for cache in mpa.get_caches():
            self.assertIsNone(cache.get_source())
            self.assertEqual('geopackage', cache.cache.get('type'))
            self.assertEqual('add_geopackage_service.gpkg', cache.cache.get('filename'))
            directory = os.path.join(self.config_mock.cache_dir, 'add_geopackage_service')
            self.assertEqual(directory, cache.cache.get('directory'))
        self.assertEqual(1, len(mpa.get_layers()))
        for layer in mpa.get_layers():
            self.assertIsInstance(layer.get_source(), Cache)

    def test_add_geopackage_service_duplicate_name(self):
        MapProxyApp.objects.all().delete()
        self.create_yaml_model()
        with open(test_utils.NC_GEOPACKAGE, 'rb') as f:
            response = Client().post('/add_geopackage_service', {self.yaml_name: f})
        self.assertBadRequest(response)

    def test_import_pep(self):
        """
        Tests hitting the import pep endpoint.
        By adding a service with the name one as what was specified in the .pep file.
        """
        MapProxyApp.objects.all().delete()
        with open(test_utils.NC_PEP_FILE, 'rb') as pep_file:
            response = Client().post('/import_pep', {'nc': pep_file})
        self.assertEqual(["nc"], response.json())
        self.assertOKStatus(response)
        self.assertEqual(1, MapProxyApp.objects.count())
        self.assertEqual("nc", MapProxyApp.objects.all().first().name)
        self.assertEqual([], os.listdir(self.config_mock.temp_dir))
        self.assertTrue(os.path.exists(os.path.join(self.config_mock.app_dir, 'nc.yaml')))
        self.assertTrue(os.path.exists(os.path.join(self.config_mock.cache_dir, 'nc')))

    def test_import_pep_different_service_name(self):
        """Tests hitting the import pep endpoint with a different service name than what is in the pep file."""
        MapProxyApp.objects.all().delete()
        path = os.path.join(self.config_mock.cache_dir, 'nc')
        if not os.path.exists(path):
            os.makedirs(path)
        with open(test_utils.NC_PEP_FILE, 'rb') as pep_file:
            response = Client().post('/import_pep', {'model_name': pep_file})
        self.assertEqual(["model_name"], response.json())
        self.assertOKStatus(response)
        self.assertEqual(1, MapProxyApp.objects.count())
        self.assertEqual('model_name', MapProxyApp.objects.all().first().name)
        self.assertEqual([], os.listdir(self.config_mock.temp_dir))
        self.assertTrue(os.path.exists(os.path.join(self.config_mock.app_dir, 'model_name.yaml')))
        self.assertTrue(os.path.exists(os.path.join(self.config_mock.cache_dir, 'model_name')))

    def test_import_pep_with_cache_already_exists(self):
        """Tests hitting the import pep endpoint when the cache already exists."""
        MapProxyApp.objects.all().delete()
        with open(test_utils.NC_PEP_FILE, 'rb') as pep_file:
            response = Client().post('/import_pep', {'model_name': pep_file})
        self.assertEqual(["model_name"], response.json())
        self.assertOKStatus(response)
        self.assertEqual(1, MapProxyApp.objects.count())
        self.assertEqual('model_name', MapProxyApp.objects.all().first().name)
        self.assertEqual([], os.listdir(self.config_mock.temp_dir))
        self.assertTrue(os.path.exists(os.path.join(self.config_mock.app_dir, 'model_name.yaml')))
        self.assertTrue(os.path.exists(os.path.join(self.config_mock.cache_dir, 'model_name')))

    def test_import_pep_bad_service_name(self):
        """Tests importing a pep file with a bad service name."""
        MapProxyApp.objects.all().delete()
        with open(test_utils.NC_PEP_FILE, 'rb') as pep_file:
            response = Client().post('/import_pep', {'.pep': pep_file})
        self.assertBadRequest(response)
        self.assertEqual(0, len(MapProxyApp.objects.all()))
        with open(test_utils.NC_PEP_FILE, 'rb') as pep_file:
            response = Client().post('/import_pep', {'': pep_file})
        self.assertEqual(0, len(MapProxyApp.objects.all()))

    def test_import_pep_service_name_already_exists(self):
        """Test importing a .pep when a service with its name already exists."""
        models.MapProxyApp.objects.all().delete()
        self.create_yaml_model()
        self.assertEqual(self.yaml_name, models.MapProxyApp.objects.all().first().name)
        self.assertEqual(1, len(models.MapProxyApp.objects.filter(name=self.yaml_name)))
        with open(test_utils.NC_PEP_FILE, 'rb') as pep_file:
            response = Client().post('/import_pep', {self.yaml_name: pep_file})
        self.assertBadRequest(response)

    @nose.tools.nottest
    @mock.patch('mapproxy.seed.gpep_seed.seed')
    def test_failed_seed_task(self, seed_mock):
        """Test a seed task that fails inside of mapproxy."""
        self.add_service(service_url=test_utils.NC_URL)

        seed_failure_message = 'The seed was interrupted and failed.'
        seed_mock.side_effect = SeedInterrupted(seed_failure_message)
        model, folder, params = self._get_test_parameters('nc', 'seed')
        response = Client().post('/create_job', json.dumps(params), content_type='application/json')
        self.assertOKStatus(response)
        task = models.Task.objects.all().first()
        self.assertTrue(task.failed)
        self.assertEqual(task.failure_message, seed_failure_message)

    @nose.tools.nottest
    @mock.patch('mapproxy.seed.gpep_seed.seed')
    def test_failed_package_task(self, seed_mock):
        """Test a package task that fails inside of mapproxy."""
        self.add_service(service_url=test_utils.NC_URL)

        seed_failure_message = 'The seed was interrupted and failed.'
        seed_mock.side_effect = SeedInterrupted(seed_failure_message)
        model, folder, params = self._get_test_parameters('nc', 'package')
        params['srs'] = 'EPSG:4326'
        response = Client().post('/create_job', json.dumps(params), content_type='application/json')
        self.assertOKStatus(response)
        task = models.Task.objects.all().first()
        self.assertTrue(task.failed)
        self.assertEqual(task.failure_message, seed_failure_message)

    @nose.tools.nottest
    @mock.patch('mapproxy.seed.gpep_seed.cleanup')
    def test_failed_clean_task(self, seed_mock):
        """Test a clean task that fails inside mapproxy."""
        self.add_service(service_url=test_utils.NC_URL)

        seed_failure_message = 'The seed was interrupted and failed.'
        seed_mock.side_effect = SeedInterrupted(seed_failure_message)
        model, folder, params = self._get_test_parameters('nc', 'clean')
        response = Client().post('/create_job', json.dumps(params), content_type='application/json')
        self.assertOKStatus(response)
        task = models.Task.objects.all().first()
        self.assertTrue(task.failed)
        self.assertEqual(task.failure_message, seed_failure_message)

    @mock.patch('zipfile.ZipFile')
    def test_failed_export_task(self, export_mock):
        """Test a export task that fails after starting."""
        models.Task.objects.all().delete()
        self.add_service(service_url=test_utils.NC_URL)

        export_failure_message = 'Error reading zip file.'
        export_mock.side_effect = OSError(export_failure_message)
        export_params = {'job_type': 'export', 'services': [{'id': MapProxyApp.objects.all().first().id}]}
        response = Client().post('/create_job', json.dumps(export_params), content_type='application/json')
        self.assertOKStatus(response)

        task = models.Task.objects.all().first()
        self.assertEqual(len(models.Task.objects.all()), 1)
        self.assertEqual(task.status, 'failed')
        self.assertEqual(task.failure_message, export_failure_message)

    def test_caching_enabled_get_with_cache(self):
        """Tests that cache shows up as enabled when service created with caches."""
        model = self.add_service(service_url=test_utils.NC_URL)[0]
        response = Client().get('/caching_enabled?id={}'.format(model.id))
        self.assertOKStatus(response)
        response_dict = response.json()
        self.assertEqual(True, response_dict['has_cache'])
        self.assertEqual(True, response_dict['able_to_change'])

    def test_caching_enabled_get_with_cache_with_pending_task(self):
        """Tests that cache shows up as enabled when service created with caches."""
        model = self.add_service(service_url=test_utils.NC_URL, create_task=True)[0]
        response = Client().get('/caching_enabled?id={}'.format(model.id))
        self.assertOKStatus(response)
        response_dict = response.json()
        self.assertEqual(True, response_dict['has_cache'])
        self.assertEqual(False, response_dict['able_to_change'])

    def test_caching_enabled_get_no_cache(self):
        """Tests that cache shows up as enabled when service created with caches."""
        model = self.add_service(enable_cache=False, service_url=test_utils.NC_URL)[0]
        response = Client().get('/caching_enabled?id={}'.format(model.id))
        self.assertOKStatus(response)
        response_dict = response.json()
        self.assertEqual(False, response_dict['has_cache'])
        self.assertEqual(True, response_dict['able_to_change'])

    def test_wmts_change_cache_unavailable(self):
        """Tests that the 'able_to_change' is always false if the service is a wmts service."""
        model = self.add_service(service_url=test_utils.ARCGIS_URL)[0]
        response = Client().get('/caching_enabled?id={}'.format(model.id))
        self.assertOKStatus(response)
        response_dict = response.json()
        self.assertEqual(True, response_dict['has_cache'])
        self.assertEqual(False, response_dict['able_to_change'])

    def test_caching_enabled_get_no_cache_with_pending_task(self):
        """Tests that cache shows up as enabled when service created with caches."""
        model = self.add_service(enable_cache=False, service_url=test_utils.NC_URL, create_task=True)[0]
        response = Client().get('/caching_enabled?id={}'.format(model.id))
        self.assertOKStatus(response)
        response_dict = response.json()
        self.assertFalse(response_dict['has_cache'])
        self.assertFalse(response_dict['able_to_change'])

    def test_caching_enabled_post_disable_cache(self):
        """Tests that caches are enabled when hitting the caching enabled endpoint."""
        model = self.add_service(service_url=test_utils.NC_URL)[0]
        response = Client().get('/caching_enabled?id={}'.format(model.id))
        self.assertOKStatus(response)
        response_dict = response.json()
        self.assertTrue(response_dict['has_cache'])
        self.assertTrue(response_dict['able_to_change'])
        response = Client().post('/caching_enabled', json.dumps({'id': model.id, 'enable_cache': False}),
                                 content_type='application/json')
        self.assertOKStatus(response)
        self.assertFalse(model.cache_enabled())
        response = Client().get('/caching_enabled?id={}'.format(model.id))
        self.assertOKStatus(response)
        response_dict = response.json()
        self.assertFalse(response_dict['has_cache'])

    def test_caching_enabled_post_enable_cache(self):
        """Tests that cache shows up as enabled after enabling caches."""
        model = self.add_service(enable_cache=False, service_url=test_utils.NC_URL)[0]
        response = Client().get('/caching_enabled?id={}'.format(model.id))
        self.assertOKStatus(response)
        response_dict = response.json()
        self.assertFalse(response_dict['has_cache'])
        self.assertTrue(response_dict['able_to_change'])
        response = Client().post('/caching_enabled', json.dumps({'id': model.id, 'enable_cache': True}),
                                 content_type='application/json')
        self.assertOKStatus(response)
        response = Client().get('/caching_enabled?id={}'.format(model.id))
        self.assertOKStatus(response)
        response_dict = response.json()
        self.assertTrue(response_dict['has_cache'])

    def test_caching_enabled_post_enable_cache_task_complete(self):
        """Tests enabling a cache with a finished and unfinished task."""
        model = self.add_service(create_task=True, enable_cache=False, service_url=test_utils.NC_URL)[0]
        response = Client().get('/caching_enabled?id={}'.format(model.id))
        self.assertOKStatus(response)
        response_dict = response.json()
        self.assertFalse(response_dict['has_cache'])
        self.assertFalse(response_dict['able_to_change'])
        response = Client().post('/caching_enabled', json.dumps({'id': model.id, 'enable_cache': True}),
                                 content_type='application/json')
        self.assertBadRequest(response)
        task = models.Task.objects.all().first()
        task.finished = True
        task.save()
        response = Client().get('/caching_enabled?id={}'.format(model.id))
        self.assertOKStatus(response)
        response_dict = response.json()
        self.assertFalse(response_dict['has_cache'])
        self.assertTrue(response_dict['able_to_change'])
        response = Client().post('/caching_enabled', json.dumps({'id': model.id, 'enable_cache': True}),
                                 content_type='application/json')
        self.assertOKStatus(response)
        response = Client().get('/caching_enabled?id={}'.format(model.id))
        self.assertOKStatus(response)
        response_dict = response.json()
        self.assertTrue(response_dict['has_cache'])

    def test_caching_enabled_post_enable_cache_with_task(self):
        """Tests that enabling caching fails when in-progress task exists for that service."""
        model = self.add_service(create_task=True, service_url=test_utils.NC_URL)[0]
        response = Client().get('/caching_enabled?id={}'.format(model.id))
        self.assertOKStatus(response)
        response_dict = response.json()
        self.assertTrue(response_dict['has_cache'])
        self.assertFalse(response_dict['able_to_change'])
        response = Client().post('/caching_enabled', json.dumps({'id': model.id, 'enable_cache': False}),
                                 content_type='application/json')
        self.assertBadRequest(response)

    def test_caching_enabled_post_disable_cache_with_task(self):
        """Tests that disabling caching fails when in-progress task exists for that service."""
        model = self.add_service(enable_cache=False, create_task=True, service_url=test_utils.NC_URL)[0]
        response = Client().get('/caching_enabled?id={}'.format(model.id))
        self.assertOKStatus(response)
        response_dict = response.json()
        self.assertFalse(response_dict['has_cache'])
        self.assertFalse(response_dict['able_to_change'])
        response = Client().post('/caching_enabled', json.dumps({'id': model.id, 'enable_cache': True}),
                                 content_type='application/json')
        self.assertBadRequest(response)

    def test_caching_enabled_post_cache_enable_cache(self):
        """Tests setting enable_cache to its existing value with and without task. Both should return 200s."""
        model = self.add_service(enable_cache=True, service_url=test_utils.NC_URL)[0]
        response = Client().get('/caching_enabled?id={}'.format(model.id))
        self.assertOKStatus(response)
        response_dict = response.json()
        self.assertTrue(response_dict['has_cache'])
        response = Client().post('/caching_enabled', json.dumps({'id': model.id, 'enable_cache': True}),
                                 content_type='application/json')
        self.assertOKStatus(response)
        self.assertTrue(model.cache_enabled())
        self.create_task(model)
        response = Client().post('/caching_enabled', json.dumps({'id': model.id, 'enable_cache': True}),
                                 content_type='application/json')
        self.assertOKStatus(response)
        self.assertTrue(model.cache_enabled())

    def test_caching_enabled_get_no_such_id(self):
        """Tests cache enable get when passing in a id that no model has."""
        large_number = 123895321
        self.assertIsNone(MapProxyApp.objects.filter(id=large_number).first())
        response = Client().get('/caching_enabled?id={}'.format(large_number))
        self.assertBadRequest(response)

    def test_caching_enabled_post_no_such_id(self):
        """Tests cache enable post when passing in a id that no model has."""
        large_number = 123895321
        self.assertIsNone(MapProxyApp.objects.filter(id=large_number).first())
        response = Client().post('/caching_enabled', json.dumps({'id': large_number, 'enable_cache': False}),
                                 content_type='application/json')
        self.assertBadRequest(response)

    @mock.patch("celery.app.control.Control.revoke")
    def test_cancel_job_job_not_finished(self, celery_revoke_mock):
        """Test cancel job when it is not finished."""
        models.Job.objects.all().delete()
        task = self.create_task()
        task.finished = False
        task.save()
        job = models.Job.objects.all().first()
        job.finished = False
        job.save()
        with mock.patch('logging.Logger.debug'):
            response = Client().post('/cancel_job', json.dumps({'job_id': job.id}), content_type='application/json')
        self.assertOKStatus(response)
        celery_revoke_mock.assert_called_once_with(task.celery_task_id, None, True)

    def test_cancel_job_completed(self):
        """Tests canceling a completed job."""
        models.Job.objects.all().delete()
        task = self.create_task()
        task.finished = True
        task.save()
        job = models.Job.objects.all().first()
        job.finished = True
        job.save()
        with mock.patch('logging.Logger.debug'):
            response = Client().post('/cancel_job', json.dumps({'job_id': job.id}), content_type='application/json')
        self.assertBadRequest(response)

    def test_cancel_job_bad_job_id(self):
        """Test cancel job with a bad job id and without a job id."""
        with mock.patch('logging.Logger.debug'):
            response = Client().post('/cancel_job', json.dumps({'job_id': 123456789}), content_type='application/json')
        self.assertBadRequest(response)
        with mock.patch('logging.Logger.debug'):
            response = Client().post('/cancel_job', json.dumps({}), content_type='application/json')
        self.assertBadRequest(response)

    def test_get_default_baselayer(self):
        """Tests the fetching of information pertaining to the default baselayer"""

        response = Client().get('/get_default_baselayer')
        self.assertOKStatus(response)

        # make sure the returned information on the default base layer from the GET endpoint matches the data
        # as defined in pep.cfg
        self.assertEqual(self.config_mock.default_basemap_title, response.data['title'])
        self.assertEqual(self.config_mock.default_basemap_url, response.data['url'])
        self.assertEqual(self.config_mock.default_basemap_max_zoom_level, response.data['max_zoom'])
