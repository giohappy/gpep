"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

from gpep_apps.api.gpep_exception import GPEPException
from gpep_apps.mpas.model_creator import create_map_proxy_app_from_file, create_grid
from gpep_apps.tests import test_utils


class TestMPASModels(test_utils.GPEPTest):
    def setUp(self):
        super(TestMPASModels, self).setUp()
        self.yaml_file = create_map_proxy_app_from_file(test_utils.NC_PATH)

    def test_get_first_type(self):
        """Test get_first_type returns the correct value."""
        sources = self.yaml_file.get_sources()
        self.assertEqual(sources[0].type, self.yaml_file.get_first_type())
        self.assertEqual(sources[0].type, self.yaml_file.type)

    def test_get_service_type(self):
        """Test get_service_type returns the correct value."""
        key = None
        for key, value in self.yaml_file.get_services().to_dict().iteritems():
            if value is not None:
                break
        self.assertEqual(key, self.yaml_file.get_service_type())

    def test_get_services_none(self):
        """Test get_service_type doesn't fail when yaml_file has no services."""
        service = self.yaml_file.get_services()
        service.demo = None
        service.kml = None
        service.tms = None
        service.wmts = None
        service.wms = None
        self.assertEqual(None, self.yaml_file.get_service_type())

    def test_get_services_no_services(self):
        """Test get_service_type doesn't fail when yaml_file's services is None."""
        self.yaml_file.services = None
        self.assertEqual(None, self.yaml_file.get_service_type())

    def test_grid_get_epsg_num(self):
        grid = create_grid({'srs': 'EPSG:1234'}, self.yaml_file)
        self.assertEqual(1234, grid.get_epsg_num())

        grid = create_grid({'srs': 'EPSG:1234:'}, self.yaml_file)
        self.assertEqual(1234, grid.get_epsg_num())

        grid = create_grid({'srs': 'EPSG'}, self.yaml_file)
        self.assertEqual(None, grid.get_epsg_num())

        grid = create_grid({}, self.yaml_file)
        self.assertEqual(None, grid.get_epsg_num())

    def test_layer_set_get_source_None(self):
        layer = self.yaml_file.get_layers()[0]
        layer.set_source(None)
        self.assertEqual(None, layer.get_source())
        self.assertEqual(None, layer.get_source_name())

    def test_layer_set_get_source_source(self):
        layer = self.yaml_file.get_layers()[0]
        source = self.yaml_file.get_sources()[0]
        layer.set_source(source)
        self.assertEqual(source, layer.get_source())
        self.assertEqual(source.key, layer.get_source_name())

    def test_layer_set_get_source_cache(self):
        layer = self.yaml_file.get_layers()[0]
        cache = self.yaml_file.get_caches()[0]
        layer.set_source(cache)
        self.assertEqual(cache, layer.get_source())
        self.assertEqual(cache.key, layer.get_source_name())

    def test_layer_set_get_source_self(self):
        layer = self.yaml_file.get_layers()[0]
        with self.assertRaises(GPEPException):
            layer.set_source(layer)

    def test_cache_set_get_source_none(self):
        cache = self.yaml_file.get_caches()[0]
        with self.assertRaises(GPEPException):
            cache.set_source('')

    def test_cache_set_get_source_self(self):
        """Ensures that a cache cannot be set to be its own source."""
        cache = self.yaml_file.get_caches()[0]
        with self.assertRaises(GPEPException):
            cache.set_source(cache.key)
        with self.assertRaises(GPEPException):
            cache.set_source(cache)

    def test_cache_set_get_source(self):
        cache = self.yaml_file.get_caches()[0]
        source = cache.get_source()
        cache.set_source(source)
        self.assertEqual(source, cache.get_source())
        self.assertEqual(source.key, cache.get_source_name())

    def test_layer_get_bbox_no_source(self):
        """Tests get_bbox on a layer without a source."""
        layer = self.yaml_file.get_layers()[0]
        layer.set_source(None)
        self.assertEqual([0, 0, 0, 0], layer.get_bbox())

    def test_cache_get_bbox_no_source(self):
        """Tests get_bbox on a cache without a source."""
        cache = self.yaml_file.get_caches()[0]
        cache.set_source(None)
        self.assertEqual([-180.0, -90.0, 180.0, 90.0], cache.get_bbox())

    def test_source_get_bbox_no_coverage(self):
        """Test get_bbox on a source with a source without a bbox and without a source."""
        source = self.yaml_file.get_sources()[0]
        source.coverage.bbox = None
        self.assertEqual([-180.0, -90.0, 180.0, 90.0], source.get_bbox())
        source.coverage = None
        self.assertEqual([-180.0, -90.0, 180.0, 90.0], source.get_bbox())
