"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import yaml

from gpep_apps.mpas.model_creator import create_map_proxy_app_from_file
from gpep_apps.tests import test_utils

import nose


class YamlReaderTestCase(test_utils.GPEPTest):
    """
    These tests check that yaml file -> dict -> model -> dict = yaml file -> dict
    """

    # TODO: Find WMTS for all tests. Replace 'WMTS_PLACEHOLDER' with the service name.
    @nose.tools.nottest
    def test_WMTS_PLACEHOLDER(self):
        """Test that pulling TEST2_PATH through a model doesn't change it."""
        self.assert_dicts_equal(
            yaml.safe_load(file(test_utils.WMTS_PLACEHOLDER_PATH).read()),
            create_map_proxy_app_from_file(test_utils.WMTS_PLACEHOLDER_PATH).to_dict())

    def test_nc(self):
        """Test that pulling NC_PATH through a model doesn't change it."""
        self.assert_dicts_equal(
            yaml.safe_load(file(test_utils.NC_PATH).read()),
            create_map_proxy_app_from_file(test_utils.NC_PATH).to_dict())
