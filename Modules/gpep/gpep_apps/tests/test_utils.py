"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import cProfile
import logging
import mock
import os
import pprint
import shutil
import unittest
import yaml

try:
    from gpep_apps.mpas import model_creator
except:
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "gpep_apps.pepapi.settings")
    import django
    django.setup()
    from gpep_apps.mpas import model_creator
from gpep_apps.jobs import package
from gpep_apps.jobs.models import Clean, Export, Job, Package, Seed, Task, TaskLayers
from gpep_apps.jobs.utils import get_unique_filename
from gpep_apps.config import gpepconfig

import django
from django.db import models
from mapproxy.script.conf import app


PRINT_GENERATED_FILES = False

DIR_NAME = os.path.dirname(__file__)
TEST_RESOURCES_DIR = os.path.join(DIR_NAME, 'test_resources')
STATIC_RESOURCES_DIR = os.path.join(TEST_RESOURCES_DIR, 'static_resources')
PEP8_CONFIG_PATH = os.path.join(STATIC_RESOURCES_DIR, 'pep8_config.txt')
IN_PROGRESS_LOG_FILE = os.path.join(STATIC_RESOURCES_DIR, 'in_progress_log_file.log')
IN_PROGRESS_LOG_COMPLETE_PERCENTAGE = 57.27
COMPLETED_LOG_FILE = os.path.join(STATIC_RESOURCES_DIR, 'completed_log_file.log')
NC_GEOPACKAGE = os.path.join(STATIC_RESOURCES_DIR, 'nc.gpkg')
NC_PEP_FILE = os.path.join(STATIC_RESOURCES_DIR, 'nc.pep')
NC_NO_BASE = os.path.join(STATIC_RESOURCES_DIR, 'nc_no_base.yaml')
TEMPLATE_FILE = os.path.join(STATIC_RESOURCES_DIR, 'default.yaml')
INVALID_CAP_WT_URL = os.path.join(STATIC_RESOURCES_DIR, 'wt_invalid_wmts_capabilities.xml')

NRL_URL = 'http://geoint.nrlssc.navy.mil/nrltileserver/wms?REQUEST=GetCapabilities&SERVICE=WMS'
NRL_DIR = os.path.join(TEST_RESOURCES_DIR, 'nrl')
NRL_PATH = os.path.join(NRL_DIR, 'nrl.yaml')
NRL_PACKAGE_TEMP_SOLUTION_PATH = os.path.join(NRL_DIR, 'package', 'nrl_temp.yaml')
NRL_PACKAGE_SEED_SOLUTION_PATH = os.path.join(NRL_DIR, 'package', 'nrl_seed.yaml')

# An example WMTS URL that can be used for testing
ARCGIS_URL = ('http://services.arcgisonline.com/arcgis/rest/services/World_Topo_Map/MapServer/WMTS?Service=WMTS')

# TODO: Find WMTS to use for tests, same as in test_seed.py, test_clean.py, etc.
# TODO: Replace 'WMTS_PLACEHOLDER' with the service name.
WMTS_PLACEHOLDER_DIR = os.path.join(TEST_RESOURCES_DIR, 'WMTS_PLACEHOLDER')
WMTS_PLACEHOLDER_PATH = os.path.join(WMTS_PLACEHOLDER_DIR, 'WMTS_PLACEHOLDER.yaml')
WMTS_PLACEHOLDER_PACKAGE_TEMP_SOLUTION_PATH = os.path.join(WMTS_PLACEHOLDER_DIR, 'package',
                                                           'WMTS_PLACEHOLDER_temp.yaml')
WMTS_PLACEHOLDER_PACKAGE_SEED_SOLUTION_PATH = os.path.join(WMTS_PLACEHOLDER_DIR, 'package',
                                                           'WMTS_PLACEHOLDER_seed.yaml')

NC_URL = "https://services.nconemap.gov/secure/services/Imagery/Orthoimagery_Latest/ImageServer/WMSServer"
NC_DIR = os.path.join(TEST_RESOURCES_DIR, 'nc')
NC_PATH = os.path.join(NC_DIR, 'nc.yaml')
NC_PACKAGE_TEMP_SOLUTION_PATH = os.path.join(NC_DIR, 'package', 'nc_temp.yaml')
NC_PACKAGE_SEED_SOLUTION_PATH = os.path.join(NC_DIR, 'package', 'nc_seed.yaml')

RESOURCES_DIR = os.path.join(TEST_RESOURCES_DIR, 'resources_dir')


def generate_map_proxy_app(service_name):
    return model_creator.create_map_proxy_app_from_file(
        os.path.join(TEST_RESOURCES_DIR, service_name, '{}.yaml'.format(service_name)))


def generate_config_mock(default_projection='4326'):
    """
    Generates a mock object to replace the gpepconfig one
    :param default_projection: the projection
    :return: the mock object
    """
    config_mock = mock.Mock()
    config_mock.app_dir = os.path.join(RESOURCES_DIR, 'app')
    config_mock.cache_dir = os.path.join(RESOURCES_DIR, 'map_cache')
    config_mock.log_dir = os.path.join(RESOURCES_DIR, 'log')
    config_mock.clean_seed_dir = os.path.join(RESOURCES_DIR, 'seed')
    config_mock.temp_dir = os.path.join(RESOURCES_DIR, 'temp')
    config_mock.output_dir = os.path.join(RESOURCES_DIR, 'output')
    config_mock.default_projection = default_projection
    config_mock.cache_type = 'file'
    config_mock.compact_cache_version = 1
    config_mock.file_cache_dir_layout = 'tms'
    config_mock.cache_image_format = 'image/png'
    config_mock.template_file = TEMPLATE_FILE
    config_mock.default_basemap_title = "Test Title"
    config_mock.default_basemap_url = "Test URL"
    config_mock.default_basemap_max_zoom_level = 4
    return config_mock


class MockConfigCommand(object):
    """This allows for easy patching out of the mapproxy add service function."""
    CACHED_SERVICES = {}

    def __init__(self, file_path=None, error_message=None):
        self.file_path = file_path
        self.error_message = error_message

    def _get_side_effect_funct(self):
        def config_command_side_effect_funct(**kwargs):
            if self.error_message:
                with open(kwargs['log'], 'w+') as log_file:
                    log_file.write(self.error_message)
                return 1  # return a non-zero number like MapProxy errored out
            if self.file_path:
                with open(self.file_path) as read_from:
                    with open(kwargs['output'], mode='w') as write_to:
                        write_to.write(read_from.read().replace('<TEMPLATE>', gpepconfig.instance().template_file))
                        return 0
            # in this case, we will pull the expected value
            if kwargs['capabilities'] not in MockConfigCommand.CACHED_SERVICES:
                new_dict = {'--{}'.format(key): kwargs[key] for key in kwargs}
                with mock.patch("logging.Logger.warn"):
                    return_val = app.config_command([item for pair in new_dict.iteritems() for item in pair])
                if return_val != 0:  # only want to cache valid responses
                    return return_val
                with open(kwargs['output']) as read_from:
                    MockConfigCommand.CACHED_SERVICES[kwargs['capabilities']] = read_from.read()
                    return 0
            # use the cached value
            with open(kwargs['output'], mode='w') as write_to:
                write_to.write(MockConfigCommand.CACHED_SERVICES[kwargs['capabilities']])
            return 0
        return config_command_side_effect_funct

    def __enter__(self):
        self.patcher = mock.patch("gpep_apps.jobs.djgpep.config_command_caller")
        command_mock = self.patcher.start()
        command_mock.side_effect = self._get_side_effect_funct()
        return command_mock

    def __call__(self, func):
        def do_mapproxy_mock_add_service(*args, **kwargs):
            with mock.patch("gpep_apps.jobs.djgpep.config_command_caller") as command_mock:
                command_mock.side_effect = self._get_side_effect_funct()
                func(command_mock, *args, **kwargs)
        return do_mapproxy_mock_add_service

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.patcher.stop()


# allowed folders with paths relative to resources directory
allowed_folders = ['app', 'log', 'map_cache', 'output', 'seed', 'yaml_files']


class GPEPTest(django.test.TestCase):
    def __init__(self, *args, **kwargs):
        """
        Constructor for GPEPTest (base test class). Sets up logging and verifies that RESOURCES_DIR (essentially the
        test version of GPEP_HOME) exists.
        :param args: Argument list to be given to the parent constructor (unittest.TestCase)
        :param kwargs: Keyword arguments to be given to the parent constructor (unittest.TestCase)
        """
        super(GPEPTest, self).__init__(*args, **kwargs)
        logging.basicConfig()
        self.log = logging.Logger(args[0] if args else __name__)
        if not os.path.exists(RESOURCES_DIR):
            os.mkdir(RESOURCES_DIR)
        for folder in allowed_folders:
            path = os.path.join(RESOURCES_DIR, folder)
            if not os.path.exists(path):
                os.mkdir(path)

    def get_working_directory_diff(self):
        """
        Simply retrieves an array of all directory names and file names in the current working directory.
        :return: An array of all directory names and file names in the current working directory.
        """
        self.starting_files = []
        for dir_path, dir_names, file_names in os.walk(os.getcwd()):
            for dir_name in dir_names:
                self.starting_files.append(os.path.join(dir_path, dir_name))
            for file_name in file_names:
                self.starting_files.append(os.path.join(dir_path, file_name))

    def log_working_directory_diff(self):
        """
        Logs the differences in the working directory between the start and end of the test.
        """
        generated = []
        for dir_path, dir_names, file_names in os.walk(os.getcwd()):
            for dir_name in dir_names:
                path = os.path.join(dir_path, dir_name)
                if path not in self.starting_files:
                    generated.append(path)
            for file_name in file_names:
                path = os.path.join(dir_path, file_name)
                if path not in self.starting_files:
                    generated.append(path)
        deleted = []
        for path in self.starting_files:
            if not os.path.exists(path):
                deleted.append(path)
        if generated or deleted:
            self.print_and_log('---{} DIFF---'.format(self.id()))
            for gen in generated:
                self.print_and_log('+ {}'.format(gen.replace(os.getcwd(), '')))
            for path in deleted:
                self.print_and_log('- {}'.format(path.replace(os.getcwd(), '')))

    def setUp(self):
        self.fake_path = r'C:\path\that\doesnt\exist'
        # Ensure that fake path doesn't actually exist. This would cause tests to fail
        self.assertFalse(os.path.exists(self.fake_path))
        self.log = logging.getLogger(__name__)
        self.starting_files = None
        self.profiling = False

    def start_profiling(self):
        self.profiling = True
        self.profile = cProfile.Profile()
        self.profile.enable()

    def tearDown(self):
        if self.starting_files is not None:
            self.log_working_directory_diff()
        if self.profiling:
            self.profile.disable()
            for info in self.profile.getstats():
                self.log.info(info)

    def print_and_log(self, message, level=logging.DEBUG, log=True, std_out=True):
        if log:
            self.log.log(msg=message, level=level)
        if std_out:
            print message

    @staticmethod
    def format_var(num):
        if isinstance(num, (list, dict)):
            num = sorted(num)
        if isinstance(num, unicode):
            num = str(num)
        return pprint.pformat(num)

    def clean_resources_folder(self, fail_on_find=False):
        """
        Cleans the resources folder
        :param fail_on_find: if an error should be raised when finding something
        """
        removed_files = []
        for dir_path, dir_names, file_names in os.walk(RESOURCES_DIR):
            relative_path = dir_path.replace(RESOURCES_DIR, '')
            for dir_name in dir_names:
                path = os.path.join(relative_path, dir_name)
                if path not in allowed_folders:
                    removed_files.append(path)
                    shutil.rmtree(os.path.join(dir_path, dir_name), ignore_errors=True)
            for file_name in file_names:
                path = os.path.join(relative_path, file_name)
                removed_files.append(path)
                try:
                    os.remove(os.path.join(dir_path, file_name))
                except OSError:
                    pass
        if removed_files and PRINT_GENERATED_FILES:
            self.print_and_log('---{} Generated---'.format(self.id()))
            for file in removed_files:
                self.print_and_log(file)
        if fail_on_find:
            self.assertTrue(removed_files)

    def replace_path(self, in_object, old, new):
        if isinstance(in_object, (unicode, str)):
            if old in in_object:
                return os.path.abspath(os.path.join(new, in_object.replace(old, '')))
        if isinstance(in_object, dict):
            for key, value in in_object.iteritems():
                in_object[key] = self.replace_path(in_object[key], old, new)
            return in_object
        elif isinstance(in_object, list):
            for i in range(len(in_object)):
                in_object[i] = self.replace_path(in_object[i], old, new)
            return in_object
        else:
            return in_object

    def assert_dicts_equal(self, expected, actual):
        """
        Checks to see if two dicts are equal
        :param expected: the dict that sub is a subset of
        :param actual: dict to check if its a subset of whole
        """
        self.replace_path(expected, r'/opt/pep/', RESOURCES_DIR)
        self.replace_path(actual, r'/opt/pep/', RESOURCES_DIR)
        self._assert_dicts_equal(expected, actual, position=[])

    def _assert_dicts_equal(self, expected, actual, position):
        """
        Asserts that the two dicts are recursively equal
        :param expected: what we are expecting to see at this position
        :param actual: what we are actually seeing at this position
        :param position: the current position in the dict
        """
        def print_error():
            """Prints the position in the dict when the error happened."""
            self.print_and_log('-----FAIL-----')
            self.print_and_log('Position in dict: {}'.format(', '.join(position)))
            self.print_and_log('{} != {}'.format(self.format_var(expected), self.format_var(actual)))
        if position is None:
            position = []
        if isinstance(expected, dict):
            try:
                self.assertEquals(len(expected), len(actual))
            except AssertionError:
                print_error()
                raise
            for key, value in actual.iteritems():
                position.append(key)
                try:
                    self.assertIn(key, expected)
                except AssertionError:
                    print_error()
                    print '{} not in {}'.format(key, expected)
                    raise
                self._assert_dicts_equal(expected[key], value, position)
                position.pop()
        elif isinstance(expected, (list, models.QuerySet)):
            self.assertEqual(len(actual), len(expected))
            for index in range(len(actual)):
                self._assert_dicts_equal(expected[index], actual[index], position)
        elif isinstance(expected, models.Model) or isinstance(actual, models.Model):
            self._assert_dicts_equal(
                expected.to_dict() if isinstance(expected, models.Model) else expected,
                actual.to_dict() if isinstance(actual, models.Model) else actual,
                position)
        else:
            try:
                self.assertEquals(str(actual), str(expected))
            except AssertionError:
                print_error()
                raise


class CacheManagerTest(GPEPTest):
    """Test class that redirects output based on gpepconfig"""
    def setUp(self):
        super(CacheManagerTest, self).setUp()
        self.test_resources_dir = TEST_RESOURCES_DIR
        self.resources_dir = RESOURCES_DIR
        self.config_mock = generate_config_mock()
        self.clean_resources_folder()
        os.makedirs(os.path.join(self.resources_dir, 'temp'))
        self.yaml_name = "app"
        self.yaml_path = os.path.join(self.config_mock.app_dir, '{}.yaml'.format(self.yaml_name))
        self.yaml_path_secondary = os.path.join(self.config_mock.app_dir, '{}_secondary.yaml'.format(self.yaml_name))

        self.gpep_config_instance = mock.patch('gpep_apps.config.gpepconfig.instance')
        self.gpep_config_instance_mock = self.gpep_config_instance.start()
        self.assertIsInstance(gpepconfig.instance(), mock.Mock)
        self.gpep_config_instance_mock.return_value = self.config_mock

    def create_yaml_model(self):
        """
        Creates a yaml model and stores it in self.yaml_model
        """
        shutil.copyfile(os.path.join(STATIC_RESOURCES_DIR, '{}_template.yaml'.format(self.yaml_name)),
                        self.yaml_path)
        yaml_dict = model_creator.yaml_path_to_dict(self.yaml_path)
        yaml_dict['base'] = self.config_mock.template_file
        self.yaml_model = model_creator.create_map_proxy_app_from_dict(yaml_dict, name=self.yaml_name,
                                                                       pull_in_base=True, path=self.yaml_path)

    # hacky hard coded test model for doing multilayer tests
    def get_yaml_model_secondary(self):
        """
        Creates a yaml model and stores it in self.yaml_model
        """
        shutil.copyfile(os.path.join(STATIC_RESOURCES_DIR, 'secondary_template.yaml'),
                        self.yaml_path_secondary)
        yaml_dict = model_creator.yaml_path_to_dict(self.yaml_path_secondary)
        yaml_dict['base'] = self.config_mock.template_file
        return model_creator.create_map_proxy_app_from_dict(yaml_dict, name=self.yaml_name + "_secondary",
                                                            pull_in_base=True, path=self.yaml_path_secondary)

    def tearDown(self):
        if os.path.exists(self.yaml_path):
            os.remove(self.yaml_path)
        self.delete_cache_folder()
        if os.path.exists(os.path.join(self.resources_dir, 'temp')):
            shutil.rmtree(os.path.join(self.resources_dir, 'temp'))
        self.clean_resources_folder()
        self.gpep_config_instance.stop()
        super(CacheManagerTest, self).tearDown()

    def delete_cache_folder(self):
        """
        Removes cache folder from cache directory.
        :return: boolean success
        """
        cache_folder = os.path.join(self.config_mock.cache_dir, self.yaml_name)
        if not os.path.exists(cache_folder):
            return True
        try:
            shutil.rmtree(cache_folder)
            return True
        except OSError:
            self.log.exception('The {0} cache folder could not be deleted.'.format(self.yaml_name))
            return False


class JobTest(CacheManagerTest):
    def create_job(self, job_type='package'):
        """
        Creates a job model
        :param job_type: type of job to create
        :return: created job model
        """
        job = Job(job_type=job_type)
        job.save()
        default_params = dict(job=job)
        if job_type == 'package':
            model_type = Package
        elif job_type == 'seed':
            model_type = Seed
        elif job_type == 'clean':
            model_type = Clean
            default_params['date'] = '1/1/2000'
        elif job_type == 'export':
            model_type = Export
            default_params = dict(job=job)
        else:
            model_type = None
        if model_type:
            model_type(**default_params)
        return job

    def create_task(self, mpa=None, job_type='package', min_x=0, min_y=0, max_x=0, max_y=0, min_zoom=0, max_zoom=0):
        """
        Creates a task and the parent job
        :param mpa: map proxy app
        :param job_type: the type of job to create. 'seed', 'package', or 'clean'
        :return: the created task
        """
        if mpa is None:
            self.create_yaml_model()
            mpa = self.yaml_model
        task = Task(job=self.create_job(job_type), map_proxy_app=mpa)
        task.save()

        for layer in mpa.get_layers():
            TaskLayers(layer=layer, task=task, min_x=min_x, min_y=min_y, max_x=max_x, max_y=max_y, min_zoom=min_zoom,
                       max_zoom=max_zoom).save()
        return task

    def _get_test_parameters(self, service_name='nc', test_type='seed', endpoint_params=True):
        """
        Gets the tests parameters for the tests
        :param service_name: name of the service to test
        :param test_type: one of seed, clean, or package
        :param endpoint_params: if true, will return the params as the endpoint expects them. if false, will
            return the params as the internal method expects them
        :return: tuple. created_model, test folder, params for function
        """
        with open(os.path.join(TEST_RESOURCES_DIR, service_name, 'tests.yaml')) as param_file:
            param_dict = yaml.load(param_file.read())
        assert test_type in param_dict
        model = model_creator.create_map_proxy_app_from_file(
            os.path.join(TEST_RESOURCES_DIR, service_name, '{}.yaml'.format(service_name)))
        param_dict = param_dict[test_type]

        boxAndZoom = {
            'min_x': param_dict['min_x'],
            'min_y': param_dict['min_y'],
            'max_x': param_dict['max_x'],
            'max_y': param_dict['max_y'],
            'min_zoom': param_dict['min_zoom'],
            'max_zoom': param_dict['max_zoom'],
        }

        if endpoint_params:
            param_dict['job_type'] = test_type
            param_dict['services'] = [{
                'name': service_name,
                'id': model.id,
                'layers': [{'id': layer.id} for layer in model.get_layers(name__in=param_dict['layers'])],
            }]
            for layer in param_dict['services'][0]['layers']:
                for key, value in boxAndZoom.iteritems():
                    layer[key] = value
        else:
            if 'fetch_tiles' in param_dict:
                del param_dict['fetch_tiles']
            if 'srs' in param_dict and test_type != 'package':
                del param_dict['srs']
            param_dict['layers'] = model.get_layers(name__in=param_dict['layers'])

        del param_dict['min_x'], param_dict['min_y'], param_dict['max_x'], param_dict['max_y'], \
            param_dict['min_zoom'], param_dict['max_zoom']

        return model, os.path.join(TEST_RESOURCES_DIR, service_name, test_type), param_dict

    def _get_box_and_zoom_parameters(self, service_name='nc', test_type='seed'):
        with open(os.path.join(TEST_RESOURCES_DIR, service_name, 'tests.yaml')) as param_file:
            param_dict = yaml.load(param_file.read())
        assert test_type in param_dict
        param_dict = param_dict[test_type]
        return {'min_x': param_dict['min_x'],
                'min_y': param_dict['min_y'],
                'max_x': param_dict['max_x'],
                'max_y': param_dict['max_y'],
                'min_zoom': param_dict['min_zoom'],
                'max_zoom': param_dict['max_zoom']}

    def _package_test(self, service_name):
        """
        Runs a package test
        :param service_name: name of the service to test
        """
        model, folder, params = self._get_test_parameters(service_name, 'package', endpoint_params=False)
        boxAndZoom = self._get_box_and_zoom_parameters(service_name, 'package')

        params['filename'] = service_name + '.gpkg'
        params['cache_format'] = 'geopackage'
        before_dict = model.to_dict(add_temp_caches=False)
        with mock.patch('gpep_apps.jobs.runner.seed_package.delay') as seed_package_mock:
            seed_package_mock.return_value.id = 0
            with mock.patch("logging.Logger.debug"):
                print params
                package.start_job(task=self.create_task(model, **boxAndZoom), **params)
            self.assertTrue(seed_package_mock.called)
            args = seed_package_mock.call_args[0]
        temp_dict = args[0]
        seed_dict = args[1]
        with open(os.path.join(folder, '{}_seed.yaml'.format(service_name))) as seed:
            self.assert_dicts_equal(yaml.safe_load(seed), seed_dict)
        with open(os.path.join(folder, '{}_temp.yaml'.format(service_name))) as seed:
            self.assert_dicts_equal(yaml.safe_load(seed), temp_dict)
        # Verifies that the original model isn't changed
        self.assert_dicts_equal(before_dict, model.to_dict(add_temp_caches=False))

    def _seed_test(self, service_name):
        """
        Runs a seed test
        :param service_name: name of the service to test
        """
        model, folder, params = self._get_test_parameters(service_name, 'seed', endpoint_params=False)
        boxAndZoom = self._get_box_and_zoom_parameters(service_name, 'seed')
        before_dict = model.to_dict()
        with mock.patch('gpep_apps.jobs.runner.seed_cache.delay') as seed_cache_mock:
            seed_cache_mock.return_value.id = 0
            from gpep_apps.jobs import seed
            with mock.patch("logging.Logger.debug"):
                seed.start_job(map_proxy_app=model, task=self.create_task(model, **boxAndZoom), **params)
            self.assertTrue(seed_cache_mock.called)
            args = seed_cache_mock.call_args[0]
        yaml_file = args[2]
        seed_yaml = args[1]
        with open(os.path.join(folder, '{}_seed.yaml'.format(service_name))) as seed:
            self.assert_dicts_equal(yaml_file, yaml.safe_load(seed))
        temp_path = os.path.join(folder, '{}_temp.yaml'.format(service_name))
        if os.path.exists(temp_path):
            with open(temp_path) as seed:
                self.assert_dicts_equal(yaml.safe_load(seed), seed_yaml)
        # Verifies that the original model isn't changed
        self.assert_dicts_equal(before_dict, model.to_dict())

    def _clean_test(self, service_name):
        """
        Runs a clean test
        :param service_name: name of the service to test
        """
        model, folder, params = self._get_test_parameters(service_name, 'clean', endpoint_params=False)
        boxAndZoom = self._get_box_and_zoom_parameters(service_name, 'clean')
        before_dict = model.to_dict()
        with mock.patch('gpep_apps.jobs.runner.clean_cache.delay') as clean_cache_mock:
            clean_cache_mock.return_value.id = 0
            from gpep_apps.jobs import clean
            with mock.patch("logging.Logger.debug"):
                clean.start_job(map_proxy_app=model, task=self.create_task(model, **boxAndZoom), **params)
            self.assertTrue(clean_cache_mock.called)
            args = clean_cache_mock.call_args[0]
        expected_clean_yaml = args[1]
        expected_temp_yaml = args[0]
        clean_yaml_path = os.path.join(folder, '{}_clean.yaml'.format(service_name))
        with open(clean_yaml_path) as actual_clean_yaml:
            self.assert_dicts_equal(yaml.safe_load(actual_clean_yaml), expected_clean_yaml)
        temp_yaml_path = os.path.join(folder, '{}_temp.yaml'.format(service_name))
        if os.path.exists(temp_yaml_path):
            with open(temp_yaml_path) as actual_temp_yaml:
                self.assert_dicts_equal(yaml.safe_load(actual_temp_yaml), expected_temp_yaml)
        # Verifies that the original model isn't changed
        self.assert_dicts_equal(before_dict, model.to_dict())
