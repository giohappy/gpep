"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import os
import scandir
import time

from celery.task.control import inspect
from celery import shared_task
from .models import Cache
from gpep_apps.mpas.models import MapProxyApp
from gpep_apps.config import gpepconfig

import file_utils


@shared_task(name="calculate_cache_size")
def calculate_cache_size():
    """
    Determines available cache size on the computer, as well as free space.
    Stores results in Django model status.models.Cache

    this function designed to be launched as a celery task.  Then, after the task completes, it will start
    itself back up again in the future after some time.

    Rational:  this function is a very CPU intensive operation, and the amount of CPU/time it takes depends on the
    size of the cache and the process speed of the computer.  We can't simply schedule the task periodically, because
    if there is a very large map cache, that time between calls might be too small, and thus you can have multiple
    calculate_cache_size operations running in the background at the same time, chewing up resources.
    """

    # prevents multiple calculate_cache_size tasks from running at the same time
    active_tasks = inspect().active()
    for key, value in active_tasks.iteritems():
        if isinstance(value, list):
            names = [i['name'] for i in value if 'calculate_cache_size' in i['name']]
            if len(names) > 1:
                return

    start_time = time.time()

    services = MapProxyApp.objects.all()
    path = gpepconfig.instance().cache_dir
    total_size = 0

    for service in services:
        size = file_utils.get_size(os.path.join(path, service.name))
        # update each service's cache size
        service.cache_size = size
        service.save()
        total_size += size

    free_space = file_utils.get_available_storage()

    end_time = time.time()
    delta_seconds = end_time - start_time
    count = Cache.objects.count()

    if count:
        c = Cache.objects.all().first()
        c.used_space = total_size
        c.free_space = free_space
        c.runtime = delta_seconds
    else:
        c = Cache(used_space=total_size, free_space=free_space, runtime_seconds=delta_seconds)

    c.save()
    # run get_cache_size again as a celery task, after some time, depending on how long the task just took to run
    calculate_cache_size.apply_async(countdown=max(10*delta_seconds, 30))
