"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import ctypes
import logging
import os
import scandir
import sys

from gpep_apps.config import gpepconfig

log = logging.Logger(name=__name__)


def is_linux():
    """
    Returns true if the current OS is Linux, false if plebeian
    :return: boolean success
    """
    return 'LINUX' in sys.platform.upper()


def get_statvfs(directory='.'):  # pragma: no cover
    """
    Gets the object that can be used for computing free space on linux
    :param directory: the directory to look at for the available space
    :return: the size in bytes of the directory
    """
    return os.statvfs(directory)


def get_windows_free_space(directory='.'):  # pragma: no cover
    """
    Gets the object that can be used for computing free space on non linux OSes
    :param directory: the directory to look at for the available space
    :return: free_bytes, total_bytes
    """
    free_bytes = ctypes.c_ulonglong(0)
    total_bytes = ctypes.c_ulonglong(0)
    ctypes.windll.kernel32.GetDiskFreeSpaceExW(
        ctypes.c_wchar_p(directory), None, ctypes.pointer(total_bytes), ctypes.pointer(free_bytes))
    return free_bytes, total_bytes


def get_available_storage(config=None):
    """
    Returns system available storage.
    :param config: GPEPConfig model object
    :return: available storage in bytes
    """
    config = config if config else gpepconfig.instance()
    if is_linux():
        statvfs = get_statvfs(config.cache_dir)
        free_space = statvfs.f_frsize * statvfs.f_bavail
        return free_space
    else:
        free_bytes, total_bytes = get_windows_free_space(config.cache_dir)
        return free_bytes.value


def get_size(start_path='.'):
    """
    Returns the size of the file or directory at start_path.
    :param start_path: file/directory to return size of
    :return: total file size in bytes
    """
    return _scan(start_path)


def _scan(path):
    """
    Returns the total size of all of the items in the folder
    :param path: the path to the folder to scan
    :return: the size of everything in the folder
    """
    total = 0

    if not os.path.exists(path):
        return 0

    for entry in scandir.scandir(path):
        if entry.is_dir(follow_symlinks=False):
            total += _scan(entry.path)
        else:
            try:
                total += entry.stat(follow_symlinks=False).st_size
            except OSError:
                log.warning('Could not read the size of {}'.format(path))

    return total
