from threading import RLock

# named_locks library provides a centralized place to store threading.Lock objects by name

locks = {}
GET_LOCK = RLock()

most_recent_name = '0'


def get(lock_name):
    """
    Returns a thread lock object associated with the given lock name
    :param lock_name:  String name used to identify the lock.
    :return: a threading.Lock object
    """
    with GET_LOCK:
        lock_name = "custom_" + str(lock_name)
        if lock_name not in locks:
            locks[lock_name] = RLock()
        return locks[lock_name]


def getServiceLock(id):
    """
    Acquire a lock for a service with a particular ID
    :param id: ID of a MapProxyApp
    :return: a threading.Lock object associated with a particular service's ID
    """
    return get("service_" + str(id))


def generate_unique_lock_name():
    """
    Create a new lock with a unique name and return that name. This doesn't get the lock, just returns its name, but
    it will make sure the lock is created.
    :return: A name for a newly created lock.
    """
    global most_recent_name
    global locks

    with GET_LOCK:
        while True:
            if 'custom_{}'.format(most_recent_name) in locks.keys():
                most_recent_name = str(int(most_recent_name) + 1)
            else:
                locks['custom_{}'.format(most_recent_name)] = RLock()
                return most_recent_name
