""" Automatic Django SECRET_KEY generator.

Ideally, when running your Django App for the first time, it should automatically
generate a Django secret key, store it in a secretkey.txt file, then use that same key
for that deployment of your Django App, even when the server is restarted.

This library makes that simple.

Example:
    # in your Django settings.py file:

    import secret_key_generator
    SECRET_KEY = secret_key_generator.get()

License:
     The MIT License (MIT)

     Copyright (c) 2017 Reinventing Geospatial, Inc.

     Permission is hereby granted, free of charge, to any person obtaining a copy
     of this software and associated documentation files (the "Software"), to deal
     in the Software without restriction, including without limitation the rights
     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
     copies of the Software, and to permit persons to whom the Software is
     furnished to do so, subject to the following conditions:

     The above copyright notice and this permission notice shall be included in all
     copies or substantial portions of the Software.

     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
     SOFTWARE.
"""

from django.utils.crypto import get_random_string
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
KEY_PATH = os.path.join(BASE_DIR, '.secret')


def get():
    """
    Creates a new secretkey.txt file if it doesn't exist, then returns
    that secret key as a string.
    """
    if not __key_file_exists():
        __generate_new_key_file()
    return __get_key()


def __generate_new_key_file():
    chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*(-_=+)'
    key_string = get_random_string(50, chars)

    with open(KEY_PATH, "w") as f:
        f.write(key_string)


def __key_file_exists():
    return os.path.isfile(KEY_PATH)


def __get_key():
    with open(KEY_PATH) as f:
        return f.read().strip()
