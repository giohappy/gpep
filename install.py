# GDAL cannot be pip installed normally, a precompiled binary wheel needs to be pip installed.
# Using the custom script install.py, we can handle the special installation process of GDAL, then install the rest of
# the packages normally.  This script will also install GDAL normally via pip if the platform is Linux.

import pip
import os

_all_ = """amqp==1.4.9
anyjson==0.3.3
Babel==2.3.4
backports.ssl-match-hostname==3.5.0.1
billiard==3.3.0.23
celery==3.1.24
certifi==2016.9.26
cffi==1.9.1
CherryPy==7.1.0
coverage==4.2
cryptography==1.7.2
Cython==0.25.2
Django==1.10
django-cors-middleware==1.3.1
django-debug-toolbar==1.8
django-nose==1.4.4
django-silk==1.0.0
djangorestframework==3.4.6
enum34==1.1.6
flower==0.9.1
funcsigs==1.0.2
futures==3.0.5
gprof2dot==2016.10.13
idna==2.2
ipaddress==1.0.18
Jinja2==2.9.6
kombu==3.0.37
lxml==4.0.0
Mako==1.0.4
MarkupSafe==0.23
mock==2.0.0
nose==1.3.7
pbr==1.10.0
pep8==1.7.0
Pillow==3.3.1
psutil==4.3.0
psycopg2==2.7.3
pyasn1==0.1.9
pycodestyle==2.3.1
pycparser==2.17
Pygments==2.2.0
pyOpenSSL==16.2.0
pyproj==1.9.5.1
python-dateutil==2.6.1
pytz==2016.7
PyYAML==3.12
requests==2.11.1
scandir==1.4
six==1.10.0
sqlparse==0.2.3
tornado==4.2
tzlocal==1.4
Werkzeug==0.11.11
whitenoise==3.2.1
"""

_all_ = _all_.split('\n')  # split into list of packages
_all_ = filter(bool, _all_)  # remove empty strings

def install_windows():
    """On Windows, we need to install prebuild"""
    import platform
    print "INSTALLING GDAL FOR WINDOWS"

    architecture = platform.architecture()[0]
    if architecture == '32bit':
        name = 'GDAL-2.2.2-cp27-cp27m-win32.whl'
    elif architecture == '64bit':
        name = 'GDAL-2.2.2-cp27-cp27m-win_amd64.whl'
    else:
        raise RuntimeError('Unknown system architecture \'{}\''.format(architecture))

    # get path to the proper precompiled wheel
    wheel_path = os.path.join(os.path.dirname(__file__), 'BuildTools', 'gdal_precompiled_wheels', name)
    wheel_path = os.path.realpath(wheel_path)

    pip.main(['install', wheel_path])


def install_linux():
    print "INSTALLING GDAL FOR LINUX"
    # FIXME: gdal version hardcoded so it install correctly on our Debian docker image for unit testing on gitlab.
    # for some reason, the version available for gdal on debian is 1.10.1, which means we should then be pip installing
    # GDAL=1.10.1  BUT for some reason pip provides no GDAL 1.10.1, it seems 1.10.0 is the closest version they
    # provide.  So we hardcode 1.10.0.  Ideally we should detect the version of gdal installed on the system
    # and pip install that.
    pip.main(['install', 'GDAL==1.10.0', '--global-option', 'build_ext', '--global-option', '-I/usr/include/gdal/'])


def install_packages(packages):
    """List of packages, pip style, to pip install"""
    for package in packages:
        pip.main(['install', package])

if __name__ == '__main__':
    from sys import platform

    install_packages(_all_)
    if platform.startswith('win'):
        install_windows()
    elif platform.startswith('linux'):
        install_linux()
