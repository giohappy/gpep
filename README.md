Geospatial Performance Enhancing Proxy (GPEP)
=============================================
[![build status](https://gitlab.com/GitLabRGI/nsg/gpep/badges/master/build.svg)](https://gitlab.com/GitLabRGI/pep/commits/master)

[![coverage report](https://gitlab.com/GitLabRGI/nsg/gpep/badges/master/coverage.svg)](https://gitlab.com/GitLabRGI/pep/commits/master)

License
=======

GPEP is available under the MIT License. Detailed license terms can be found in [LICENSE.txt](LICENSE.txt).

What is GPEP?
=============

GPEP, which stands for Geospatial Performance Enhancing Proxy, is designed to
pull in map tiles from a remote server, then serve them back out in an
accelerated fashion.

GPEP's primary behavior can be broken into a few simple steps:
1.	**Connect GPEP to map services.** GPEP is supplied with URL’s (standard OGC web service endpoints) to different map services. This gives GPEP access to map image data from these services.
2.	**GPEP downloads map images from these services**. GPEP is told to download specific geographic regions of map data from these map services. Downloaded map data is “cached”. In GPEP, both “seeding” and “packaging” operations cause GPEP to cache map data. Additionally, if a service has caching enabled, then viewing any data from that service through pep will cache that data automatically
3.	**Perform reprojections/transformations.** (optional) GPEP can perform dynamic reprojections of downloaded map data.
4.	**Serve map data.** GPEP can proxy services (passing through without saving) or serving out cached map data. GPEP acts as a man in the middle, downloading map data, then re-serving (proxying it). It does not need to be connected to the internet to serve map data it already has cached.


View the Repositories in GitLab
===============================

There are two repositories to be concerned with:

1. [gpep](https://gitlab.com/GitLabRGI/nsg/gpep).  This is a convenience/utility wrapper around the map data proxy
server/image engine MapProxy.  It outfits MapProxy with a web API and
a web-based GUI interface, and adds a queuing sytem for MapProxy jobs.

2. [gpep-mapproxy](https://gitlab.com/GitLabRGI/nsg/gpep-mapproxy).  This is the actual map server, which downloads and
stores tile data in caches, then serves that data back out.  It can also convert
the tiles between spatial reference systems.  This is a fork of the main [MapProxy project](https://mapproxy.org/), with our own
custom additions.

Installation Binaries
=====================

We build Windows installers when master is updated, but
these binaries are not currently hosted.  If you have an interest in a binary,
you can contact Reinventing Geospatial.

We also keep updated Docker images, which come with GPEP already set up.


Prerequisites
=============
Note: You should install all prerequisites before proceeding to the Installation Instructions.

* [python 2.7.x](https://www.python.org/downloads/)
* [Erlang](https://www.erlang.org/downloads) - [Refer to RabbitMQ Compatibility Table](https://www.rabbitmq.com/which-erlang.html)
* [RabbitMQ](https://www.rabbitmq.com/download.html)
* [Node.js](https://nodejs.org/en/download/)
* [pip](https://pip.pypa.io/en/stable/installing/)
* [ms visual c++ compiler for python 2.7](https://www.microsoft.com/en-us/download/details.aspx?id=44266)
* virtualenv (pip install virtualenv)

Installation Instructions
=========================

Clone the repositories
----------------------

In a terminal:

```
git clone https://gitlab.com/GitLabRGI/nsg/gpep.git
git clone https://gitlab.com/GitLabRGI/nsg/gpep-mapproxy.git
```
You will see two folders: `gpep`, and `gpep-mapproxy`.

Go into the `gpep` folder. `cd gpep`


## Django backend

```
# create a virtual environement in the root of your gpep directory
# IMPORTANT!!!! You should be at the top level of the GPEP repo
pip install virtualenv
virtualenv env

# activate your virtual environment

# windows users
.\env\scripts\activate.bat

# mac/linux
source ./env/bin/activate

# you should now see (env) to the left of your command line

# install all gpep python dependencies
# make sure you are at the root of the gpep folder, which you cloned earlier
python install.py

# install gpep as an editable dependency

# windows
pip install -e .\Modules\gpep

# linux
pip install -e ./Modules/gpep


# install gpep-mapprxoy as an editable dependency
# cd to the folder where you cloned gpep and gpep-mapproxy
pip install -e gpep-mapproxy
```

Now all the dependencies are installed in the development environment, so it is time to initialize the Django database

```
cd gpep/Modules/gpep
python manage.py cleandb

# optionally, if you want to include existing mapproxy apps into the django db run populatedb
python manage.py populatedb

```
We recommend using an IDE like pycharm to run the backend. In pycharm use the python interpreter found in pep/env/scripts (or pep/env/bin for mac/linux). See instructions [here](https://www.jetbrains.com/help/pycharm/2016.1/configuring-python-interpreter-for-a-project.html) for adding a python interpreter for a project. For the run configuration, run the script pep/Build/scripts/gpepsvc.py

## Initialize Web App
```
cd gpep/Client

# ensures all frontend javascript libraries are downloaded and up to date
npm install

# ensures that all SCSS files are compiled to CSS files for browser consumption
npm run build
```

## Setup RabbitMQ
With admin privileges (i.e 'run as administrator' in windows or 'su'/'sudo' in linux), add a new RabbitMQ user:
```
rabbitmqctl add_user admin admin

rabbitmqctl add_vhost gpep

rabbitmqctl set_permissions -p gpep admin ".*" ".*" ".*"
```

If you run into issues on Windows, then type 'setx HOMEDRIVE "C:\"' in your cmd prompt to make sure Erlang is accessing your C: drive (rather than another default drive like H:).

Right now these values are stored in plain text here, and in the settings.py file. They will need to be encrypted etc before deploying to production.

## IMPORTANT! Add Temp YAMLS (Windows, instructions for Linux coming soon)
* Copy `.\gpep\BuildTools\app_config\config\pep.cfg` to `C:\ProgramData\GPEP\config`
* Copy `.\gpep\BuildTools\app_config\templates` to `C:\ProgramData\GPEP\templates`

# Run GPEP
* First, ensure the virtual environment is activated:

```
# windows users
gpep\env\scripts\activate.bat

# mac/linux
source gpep/env/bin/activate
```
* Run `python gpep\BuildTools\scripts\gpepsvc.py --start`
* Open browser at https://localhost:8080, see the GPEP user interface
* To turn GPEP off, simply control-c the python process at the terminal.

# Debugging Celery
run Celery
`celery -A gpep_apps.pepapi worker -l info`         

see Celery admin http://localhost:5555/
`flower -A gpep_apps.pepapi --port=5555`
