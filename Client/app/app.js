'use strict';

    var app = angular.module('MainApp', [
        'MapApp',
        'UtilitiesApp',
        'ServerApp',
        'ProgressApp',
        'ui.bootstrap',
        'ngAnimate',
        'ngAria',
        'ngRoute',
        'ng-mfb',
        'toastr',
        'dndLists',
        'ngFileUpload',
        'LocalStorageModule',
        'ngMaterial',
        ]);

    app.config(function ($routeProvider, $httpProvider) {

        // http://stackoverflow.com/questions/18156452/django-csrf-token-angularjs
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    });

    app.controller('HeaderController', function($rootScope, $scope, $location, $interval,
       ImportOverlayService, toastr, ServiceFactory) {
        $scope.state = false;

        // TODO factor opening and closing of side nav and settings/job panel into
        // a service, which keeps track of what's open.  currently using rootscope
        $rootScope.sideNavOpen = false;
        $rootScope.settingsPanelOpen = false;
        $rootScope.snapBack = false;

        $scope.toggleState = function () {
            $scope.state = !$scope.state;
        };
        $scope.isActive = function (viewLocation) {
            return viewLocation === $location.path();
        };

        $scope.setURL = function(url) {
            $location.url(url);
            $rootScope.$broadcast("pageChange");
            ImportOverlayService.removeOverlay();
        };

        // expose location to scope so different pages can show themselves
        // according to the current URL
        $rootScope.isCurrentURL = function(url) {
            return url == $location.url()
        }

        // for readability on other parts of the app, because the URL for the
        // map page is nothing
        $rootScope.isMapCurrentView = function() {
            return $location.url() == "/";
        }

        $rootScope.toggleSideNav = function() {
            if($rootScope.isMapCurrentView())
            {
                $rootScope.sideNavOpen = !$rootScope.sideNavOpen;
            }
        }

        $rootScope.toggleSettingsPanel = function() {
            $rootScope.settingsPanelOpen = !$rootScope.settingsPanelOpen;
            $scope.$broadcast('rzSliderForceRender')
        }

    });

    // default settings for toasts
    // check https://github.com/Foxandxss/angular-toastr for info on changing
    // toast settings
    app.config(function(toastrConfig) {
      angular.extend(toastrConfig, {
        allowHtml: true,
        closeButton: false,
        closeHtml: '<button>&times;</button>',
        extendedTimeOut: 10000,
        iconClasses: {
          error: 'toast-error',
          info: 'toast-info',
          success: 'toast-success',
          warning: 'toast-warning'
        },
        messageClass: 'toast-message',
        onHidden: null,
        onShown: null,
        onTap: null,
        progressBar: false,
        tapToDismiss: true,
        templates: {
          toast: 'directives/toast/toast.html',
          progressbar: 'directives/progressbar/progressbar.html'
        },
        timeOut: 10000, // 0 timeout means it never disappears
        titleClass: 'toast-title',
        toastClass: 'toast'
      });
    });

    // default settings for toast container
    app.config(function(toastrConfig) {
      angular.extend(toastrConfig, {
        autoDismiss: true,
        containerId: 'toast-container',
        maxOpened: 2,
        newestOnTop: true,
        positionClass: 'toast-top-right',
        preventDuplicates: false,
        preventOpenDuplicates: false,
        target: 'body'
      });
  });

  app.directive('selectOnClick', function () {
    return {
        restrict: 'A',
        link: function (scope, element) {
            var focusedElement;

            function focus() {
              if (focusedElement != this) {
                  this.select();
                  focusedElement = this;
              }
            }

            element.on('click', function () {
              if (focusedElement != this) {
                  this.select();
                  focusedElement = this;
              }
            });

            element.on('blur', function () {
                focusedElement = null;
            });
        }
    };
  })
