/**
 * The settingsPanel allows the user to adjust parameters like
 * zoom levels and bounding boxes for seeding, packaging, and cleaning jobs.
 *
 * EVENT HOOKS
 * -----------
 * The settingsPanel component provides function hooks which get fired
 *     when the zoom levels are changed by the user:
 *         - onZoomLevelsChange(zoomLevels={min: xxx, max:xxx})
 *     when the bounding box is changed by the user:
 *         - onBboxChange(bounds={north: xxx, south: xxx, east: xxx, west: xxx})
 *
 *
 * ACCEPTING EVENTS
 * ----------------
 * The settingsPanel component will consume these events:
 *    - newSnappedBounds event, with a bounds object as data payload
 *         - bounds object format: {north: xxx, south: xxx, east: xxx, west: xxx}
 *          (latitude and longitude bounds)
 *    - newMaxZoomLevel event, with an integer primitive represent a zoom level
 *      as data payload
 *    - newMinZoomLevel event, with an integer primitive represent a zoom level
 *      as data payload
 */

angular.module('MapApp').component('settingsPanel', {
  templateUrl: 'components/settings-panel/settings-panel.html',
  controller: SettingsPanelController,
  bindings: {
      snapBbox: '<',

      onZoomLevelsChange: '&', // zoomLevels -> {min: xxx, max: xxx}
      onBboxChange: '&'
  }

  // so when the bbox changes from map viewer, how does map viewer tell the job panel
  // to update?
        // 1. use $scope.$broadcast to emit bounds of the new box

});

function SettingsPanelController($scope, $rootScope, $uibModal, $timeout, $interval,
     $location, toastr, ServiceStore, ServiceFactory, ModalFactory, JobFactory, $q,
     $filter, UtilityFactory) {

    var ctrl = this;

    // either set to "fetching", "completed", or "failed"
    ctrl.serverSpaceFetchingStatus = "fetching"

    /********************** Utility / Helper ******************************/

    ctrl.currentTab = "seeding";
    ctrl.JobFactory = JobFactory

    // call whenever zoom levels have been changed.  this triggers the
    // onZoomLevelsChange callback
    //
    // note:  alternative could use $scope.watch to watch the slider values,
    // although the slider has built-in onChange functionality (may be watching
    // internally) so an explicit $scope.watch is probably not necessary
    ctrl.zoomLevelsChanged = function() {
        if (ctrl.onZoomLevelsChange != undefined) {
            ctrl.onZoomLevelsChange( {
                zoomLevels: {
                    min: ctrl.slider.minValue,
                    max: ctrl.slider.maxValue
                }
            });
			$scope.$emit('newMinZoomLevel', ctrl.slider.minValue);
        }
    }

    ctrl.slider = {
      minValue: 0,
      maxValue: 3,
      options: {
        floor: 0,
        ceil: 18,
        id: "sliderA",
        hidePointerLabels: true,
        hideLimitLabels: true,
        onChange: ctrl.zoomLevelsChanged,
      },

    };

    // make sure everyone in the UI has immediate access to zoom values
    ctrl.zoomLevelsChanged();

    ctrl.zoomLevelToScale =  function(level) {
        var scales = new Array(19);

        if(level >= scales.length || level < 0) {
            //console.warn("Level must be between 0 and " + (scales.length - 1) + " inclusive.");
            return undefined;
        }

        scales[18] = 2000;
        for (i=17; i>=0; i--) {
            scales[i] = scales[i+1]*2;
        }
        scales = scales.map(function (scale) { return "~1:" + String(scale); });

        return scales[level];
    };


    ctrl.setCurrentTab = function(tabName){
        ctrl.currentTab=tabName;
        if (tabName == 'cleaning'){
            ctrl.populateCachingSpaceAvailable();
        }

        // update job factory with current select tab
        var jobFactoryJobNames = {
            seeding:   JobFactory.SEED_JOBTYPE,
            packaging: JobFactory.PACKAGE_JOBTYPE,
            cleaning:  JobFactory.CLEAN_JOBTYPE
        }
        JobFactory.currentSettingsPanelTab = jobFactoryJobNames[tabName];
		ctrl.updateDrawnBounds();
        ctrl.refreshSlider();
    }


    ctrl.refreshSlider = function() {
      $scope.$broadcast('rzSliderForceRender')
    }
    ctrl.refreshSlider();

    /************************ Shared Controls *****************************/

    /**
     * The Seeding, Packaging, and Cleaning tabs of the Settings panel
     * share several of the same input parameters.  Namely, these are the
     * zoom level and bounding box parameters.  We markup these forms in
     * the settings_panel_shared_inputs.html file.  When changes to these
     * shared controls happen on one tab, they should also happen to the
     * other two tabs.
     */

    ctrl.sharedSettings = {};
    ctrl.sharedForm = null;
    ctrl.sharedFormValid = false;

    // initialize sharedSettings to min/max

    $scope.$on("slideEnded", function() {
      ctrl.zoomLevelsChanged();
      ctrl.updateSharedSettings();
    });

    ctrl.updateSharedSettings = function() {
        ctrl.sharedSettings.startZoomLevel = ctrl.slider.minValue;
        ctrl.sharedSettings.endZoomLevel = ctrl.slider.maxValue;

        var bounds = $rootScope.snappedBoundsGeneric
        if (bounds) {
          ctrl.sharedSettings.snappedNorth = bounds.north;
          ctrl.sharedSettings.snappedSouth = bounds.south;
          ctrl.sharedSettings.snappedEast = bounds.east;
          ctrl.sharedSettings.snappedWest = bounds.west;
        }
    };
    ctrl.updateSharedSettings();

    // if the snapped grid is recalculated, update shared settings
    $scope.$on('newSnappedBounds', function(evt, snappedBounds) {
        ctrl.updateSharedSettings();
    })
                                      /******* new slider value events *****/

    // make slider listen to broadcasted events for new slider values
    $scope.$on("newMaxZoomLevel", function(evt, newMax) {
        if(newMax < ctrl.slider.minValue) {
            var oldMin = ctrl.slider.minValue;

            setMinZoomLevel(newMax);
            setMaxZoomLevel(oldMin);
        } else {
            setMaxZoomLevel(newMax);
        }
    })

    // make slider listen to broadcasted events for new slider values
    $scope.$on("newMinZoomLevel", function(evt, newMin) {
      if(newMin > ctrl.slider.maxValue) {
          var oldMax = ctrl.slider.maxValue;

          setMinZoomLevel(oldMax);
          setMaxZoomLevel(newMin);
      } else {
          setMinZoomLevel(newMin);
      }
    })

    ///// set zoom level wrappers //////
    // wrapped in calls calls so shared settings are updated and
    // slider widget gets redrawn

    function setMaxZoomLevel(newMax) {
        ctrl.slider.maxValue = newMax;
        ctrl.updateSharedSettings();

        // forces slider widget to be redrawn
        $timeout(function() {
            $scope.$apply();
        })
    }

    function setMinZoomLevel(newMin) {
        ctrl.slider.minValue = newMin;
        ctrl.updateSharedSettings();

        // forces slider widget to be redrawn
        $timeout(function() {
            $scope.$apply();
        })
    }

    ctrl.openZoomLevelsModal = function() {
        var modalInstance = $uibModal.open({
            templateUrl: 'components/zoom-levels-modal/zoom-levels-modal.html',
            controller: 'ZoomLevelsModalController as $ctrl',
            windowClass: 'wide-modal'
        });
    };

                                        /******** Utility ***********/

    ctrl.selectEntireWorld = function() {
        ctrl.sharedSettings.snappedNorth = ctrl.sharedSettings.bboxN = 90;
        ctrl.sharedSettings.snappedEast = ctrl.sharedSettings.bboxE = 180;
        ctrl.sharedSettings.snappedSouth = ctrl.sharedSettings.bboxS = -90;
        ctrl.sharedSettings.snappedWest = ctrl.sharedSettings.bboxW = -180;

        // ctrl new bounding box parameters to map view
        ctrl.updateDrawnBounds();
    };

    // calls the function registered to the onBboxChange component callback, passing
    // it the new bounds parameters for that bbox.
    ctrl.updateDrawnBounds = function() {

        // only activate onBboxChange if the binding for it has been specified
        if (ctrl.onBboxChange == undefined) {
            return;
        }

        var bounds = {
            north : ctrl.sharedSettings.bboxN,
            east  : ctrl.sharedSettings.bboxE,
            south : ctrl.sharedSettings.bboxS,
            west  : ctrl.sharedSettings.bboxW,
        };

        ctrl.onBboxChange({bounds: bounds});
		$scope.$emit('newMinZoomLevel', ctrl.slider.minValue);
		$scope.$emit('updateDrawBounds', bounds);
    };

    /**
     * Allows the user to draw a bounding box via click and drag on the
     * map view.
     */
    ctrl.triggerDrawingMode = function() {
		$scope.$emit('toggleBBoxDrawCondition');
    };

    $scope.$on("drawingModeExited", function(evt, bounds) {
        if ($rootScope.snapBack) {
            $rootScope.snapBack = false;
        }
    })

    $scope.$on("newSelectionBoundsDrawn", function(evt, bounds) {
        if ($rootScope.snapBack) {
            $rootScope.snapBack = false;
            $rootScope.toggleSideNav();
        }
        ctrl.sharedSettings.bboxN = bounds.north;
        ctrl.sharedSettings.bboxE = bounds.east;
        ctrl.sharedSettings.bboxS = bounds.south;
        ctrl.sharedSettings.bboxW = bounds.west;

        // this forces all inputs in the settings panel to update
        // this makes sure that the bounding box coordinates are
        // immediately updated when newly drawn
        $scope.$apply();
    });

    // when the draw bounds tool on the map view is activated, clear the
    // current bounds in the settings panel
    $scope.$on("drawBoundsToolActivated", function(evt) {
        if ($rootScope.sideNavOpen) {
            $rootScope.snapBack = true;
            $rootScope.toggleSideNav();
        }
        ctrl.sharedSettings.bboxN = null;
        ctrl.sharedSettings.bboxE = null;
        ctrl.sharedSettings.bboxS = null;
        ctrl.sharedSettings.bboxW = null;

        // this forces all inputs in the settings panel to update
        // this makes sure that the bounding box coordinates are
        // immediately updated when newly drawn
        $scope.$apply();
    });

                                       /******** Validation ************/

    ctrl.addSharedForm = function(form) {
        ctrl.sharedForm = form;
    };

    ctrl.touchSharedForms = function() {
        angular.forEach(ctrl.sharedForm.$error, function (field) {
            angular.forEach(field, function(errorField){
                errorField.$setTouched();
            });
        });
    };

    // Returns true if "input" is left blank after user has interacted with it
    ctrl.doesInputHaveRequireError = function(input) {
        return  input.$error.required && input.$touched;
    };

    // returns true if "input" is greater or less than specified range bounds
    // these range bounds are attached to the specified "input" as "max" and
    // "min" attributes in the html source (map.html)
    ctrl.doesInputHaveRangeError = function(input) {
        return  (input.$error.max || input.$error.min) && input.$touched;
    };

    // returns true if the user has interacted with "input" and that input
    // is invalid for any reason.  Currently, the only two validity requirements
    // for inputs are range and require (cannot leave blank).
    ctrl.doesInputHaveError = function(input) {
        return ctrl.doesInputHaveRequireError(input) ||
            ctrl.doesInputHaveRangeError(input);
    };

    // must expose this to the ctrl, so the Selected Layers error text can
    // know when to hide/show itself
    ctrl.areAnyLayersSelected = ServiceStore.areAnyLayersSelected;

    // TODO refactor attemptStart*xxxx* into "attemptStartJob" to reduce replication
    // unit tests can be constructed after refator
    /**************************** Seeding ********************************/

    // checks if the form is valid, if so then seed
    ctrl.attemptStartSeeding = function(form) {
        // if no layers are selected, nothing to seed
        if (!ServiceStore.areAnyLayersSelected()) {
            console.log("At least one layer must be selected for seeding.");
            return;
        }

        if(!ServiceFactory.isConnectedToBackend()) {
            UtilityFactory.showCantConnectToBackendError("Can't start seeding job.")
            return;
        }

        // touches all inputs.  this will trigger error messages to
        // display if any inputs are invalid
        if (form.$invalid || ctrl.sharedForm.$invalid) {
            console.log("Seeding form is invalid.  Please try again.");
            ctrl.touchSharedForms();
        } else {
			ctrl.openSeedingModal();
            // sets input fields back to untouched, as if the user has not
            // clicked on them yet.  This will prevent errors from immediately
            // showing when they input information into the modal again, which is
            // default behavior.
            //
            // To rephase, default behavior is for error
            // feedback to be shown for an input after the user has finished typing
            // text into that input
            form.$setUntouched();
        }
    };

    ctrl.openSeedingPackagingModal = function(modal_type) {
        // 'package' or 'seed'
        var busyToast = UtilityFactory.spinnerProgressBarToast(
            "Calculating Space Requirements for " + modal_type + " Job", "Calculating");

        $q.all([
            JobFactory.sizeEstimate(ctrl.sharedSettings, JobFactory.SEED_JOBTYPE),
            ServiceFactory.freeSpace()
        ]).then(function(data) {
            toastr.clear(busyToast)
            var job_size = data[0].data.size
            var free_space = data[1].data

            if (job_size < free_space) {
                if(modal_type == 'package') {
                    var modalInstance = $uibModal.open({
                        templateUrl: 'components/map/views/seeding_modal.html',
                        controller: 'SeedingModalController',
                        resolve: {
                            sharedSettings: function() {
                                return ctrl.sharedSettings;
                            }
                        }
                    });
                } else {
                    var modalInstance = $uibModal.open({
                        templateUrl: 'components/map/views/packaging_modal.html',
                        controller: 'PackagingModalController',
                        resolve: {
                            sharedSettings: function() {
                                return ctrl.sharedSettings;
                            },
                            packageSettings: function() {
                                return ctrl.packageSettings;
                            }
                        }
                    });
                }
            } else {
                ModalFactory.basicError("Not Enough Space", "This job requires " + $filter('filesize')(job_size) + " of space " +
                "but only " + $filter('filesize')(free_space) + " are available. " +
                "Try cleaning or deleting some services, or specify a smaller range of zoom levels.")
            }
        }, function(err) {
            ModalFactor.basicError("Error getting estimate or cache size.", err)
        })
    };

    ctrl.openSeedingModal = function() {
        ctrl.openSeedingPackagingModal('package')
    }


    /***************************** Packaging ********************************/

    ctrl.packageSettings = {};

    /**
     * @summary If packaging form is valid, then start packaging.  Othwerise
     * do nothing.
     */
    ctrl.attemptStartPackaging = function(form) {
        // if no layers are selected, nothing to seed
        if(!ServiceStore.areAnyLayersSelected()) {
            console.log("At least one layer must be selected for seeding.");
            return;
        }

        if(!ServiceFactory.isConnectedToBackend()) {
            UtilityFactory.showCantConnectToBackendError("Can't start packaging job.")
            return;
        }

        // touches all inputs.  this will trigger error messages to
        // display if any inputs are invalid
        if (form.$invalid || ctrl.sharedForm.$invalid) {
            console.log("Packaging form is invalid.  Please try again.");
            ctrl.touchSharedForms();
        } else {
            // settings in packaging tab are valid
            ctrl.openPackagingModal();

            // sets input fields back to untouched, as if the user has not
            // clicked on them yet.  This will prevent errors from immediately
            // showing when they input information into the modal again, which is
            // default behavior.
            //
            // To rephase, default behavior is for error
            // feedback to be shown for an input after the user has finished typing
            // text into that input
            form.$setUntouched();
        }
    };

    ctrl.openPackagingModal = function() {
        ctrl.openSeedingPackagingModal('seed')
    };

    /****************************** Cleaning *****************************/
    ctrl.cleanSettings = {};

    // initialize datepicker to be selected to tomorrow's date
    ctrl.today = function() {
        var date = new Date();
        date.setDate(date.getDate() + 1); // set date to tomorrow
        ctrl.cleanSettings.date = date;
    };
    ctrl.today();

    /**
     * @summary If packaging form is valid, then start packaging.  Othwerise
     * do nothing.
     */
    ctrl.attemptStartCleaning = function(form) {
        // if no layers are selected, nothing to seed
        if(!ServiceStore.areAnyLayersSelected()) {
            console.log("At least one layer must be selected for cleaning.");
            return;
        }

        if(!ServiceFactory.isConnectedToBackend()) {
            UtilityFactory.showCantConnectToBackendError("Can't start cleaning job.")
            return;
        }

        // touches all inputs.  this will trigger error messages to
        // display if any inputs are invalid
        if (form.$invalid || ctrl.sharedForm.$invalid) {
            console.log("Cleaning form is invalid.  Please try again.");
            ctrl.touchSharedForms();
        } else {
			ctrl.openCleaningModal();

            // sets input fields back to untouched, as if the user has not
            // clicked on them yet.  This will prevent errors from immediately
            // showing when they input information into the modal again, which is
            // default behavior.
            //
            // To rephase, default behavior is for error
            // feedback to be shown for an input after the user has finished typing
            // text into that input
            form.$setUntouched();
        }
    };

    ctrl.openCleaningModal = function() {
        var modalInstance = $uibModal.open({
            templateUrl: 'components/map/views/cleaning_modal.html',
            controller: 'CleaningModalController',
            resolve: {
                sharedSettings: function() {
                    return ctrl.sharedSettings;
                },
                cleanSettings: function() {
                    return ctrl.cleanSettings;
                }
            }
        });
    };

    /**
      Populate the cleaning panel with details about how much space is
      available on the GPEP backend server for map caches.
    */
    ctrl.populateCachingSpaceAvailable = function() {
        // guarantees enough progressbar width to cover percentage label
        var MIN_PROGRESSBAR_PERCENTAGE = 2;

        ServiceFactory.cacheSize().then(function(response) {
            var data = response.data

             // if the backend returns a null value, it means it's still thinking and hasn't calculated a size
             if(data == false) {
                 ctrl.serverSpaceFetchingStatus = 'fetching'
                 return;
             }

             ctrl.usedServerSpace = Number(data.size);
             ctrl.freeServerSpace = Number(data.free_space);

            // calculate percentage of space used on the server
            ctrl.usedServerSpacePercentage = 100*( ctrl.usedServerSpace / (ctrl.usedServerSpace + ctrl.freeServerSpace));

            ctrl.serverPercentageMinWidth = ctrl.usedServerSpacePercentage;
            if(ctrl.serverPercentageMinWidth < MIN_PROGRESSBAR_PERCENTAGE) {
                ctrl.serverPercentageMinWidth = MIN_PROGRESSBAR_PERCENTAGE;
            }
            ctrl.serverSpaceFetchingStatus = 'completed'
        }, function(err) {
            console.error(err);
            ctrl.serverSpaceFetchingStatus = "error";
        })
    }
    // fetch available space left on server for caching map images
    ctrl.populateCachingSpaceAvailable()
    // and continue to fetch every few seconds
    $interval(ctrl.populateCachingSpaceAvailable, 10000);
};
