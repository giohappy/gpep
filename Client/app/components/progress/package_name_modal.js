
  angular.module("ProgressApp").controller("PackageNameModalCtrl", function($scope,
    $filter, $uibModalInstance, tasks, job, JobFactory, ModalFactory, $httpParamSerializer,
    DOWNLOAD_TASK_ENDPOINT, NSG_GEOPACKAGE_VERSION, moment, $q) {
    console.log(tasks, job)
    var $ctrl = this;
    $ctrl.tasks = tasks;
    $ctrl.job = job;
    $ctrl.compactVersion = null;

    $ctrl.isGpkg = function() {
      return $ctrl.job.cache_format == 'geopackage'
    }

    $ctrl.isCompact = function() {
      return $ctrl.job.cache_format.startsWith('compact');
    }

    // get version string if compact cache
    if($ctrl.job.cache_format == 'compact1') {
      $ctrl.compactVersion = 'V1';
    } else if($ctrl.job.cache_format == 'compact2') {
      $ctrl.compactVersion = 'V2';
    }

    $ctrl.formData = {
      geoPackageProducer: "AGC",
      geographicCoverageArea: "",
      dataProduct: ""
    }

    $ctrl.cancel = function() {
      $uibModalInstance.dismiss('cancel');
    };

    $ctrl.updateFilename = function() {
      // Nettwarrior geopackages don't use NSG naming conventions
      if ($ctrl.job.package_type == 'NW_3857') {
      var nameArray = [
        $filter('space-to-dash')($ctrl.formData.geoPackageProducer),
        $filter('space-to-dash')($ctrl.formData.dataProduct),
        $filter('space-to-dash')($ctrl.formData.geographicCoverageArea),
        $ctrl.job.min_zoom + "-" + $ctrl.job.max_zoom,
        $ctrl.job.package_type,
        moment().format('DDMMMYY').toUpperCase()
      ];
      } else {
        var nameArray = [
          $filter('space-to-dash')($ctrl.formData.geoPackageProducer),
          $filter('space-to-dash')($ctrl.formData.dataProduct),
          $filter('space-to-dash')($ctrl.formData.geographicCoverageArea),
          $ctrl.job.min_zoom + "-" + $ctrl.job.max_zoom,
          $ctrl.job.package_type + "-" + NSG_GEOPACKAGE_VERSION,
          moment().format('DDMMMYY').toUpperCase()
        ];

        if($ctrl.isCompact()) {
          nameArray.push('ESRICompactCache' + $ctrl.compactVersion)
        }
      }

      nameArray = nameArray.filter(function(item){return item.length})
      formatToExtension = {
        geopackage: '.gpkg',
        compact1: '.zip',
        compact2: '.zip',
      }
      $ctrl.filename = nameArray.join("_") + formatToExtension[$ctrl.job.cache_format];

      return $ctrl.filename;
    };

    $ctrl.updateFilename();

    // attempts to download package file.
    // first verifies that all inputs are correct, and if they aren't
    // triggers errors to show
    $ctrl.attemptToDownload = function() {
      // touch all input fields, triggering errors to show
      angular.forEach($scope.packageNameForm.$error, function (field) {
        angular.forEach(field, function(errorField){
          errorField.$setTouched();
        })
      });

      // if all inputs valid, then download package
      if($scope.packageNameForm.$valid) {
        JobFactory.jobAvailableForDownload($ctrl.job.id, $ctrl.updateFilename()).then(function() {
          JobFactory.downloadJob($ctrl.job.id, $ctrl.updateFilename());
        }, function(err) {
          //console.error("Job with id " + $ctrl.job.id + " not yet available for download. ")
          ModalFactory.basicError("Job Not Available for Download", err)
          console.error(err)
        })

        //$ctrl.createDownloadParams();
      }
    }
});
