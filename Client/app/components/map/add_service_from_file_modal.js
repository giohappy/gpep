"use strict";

/**
 * add_service_modal.js gives form validation to the Add Service Modal, and
 * talks to server.js to add a service to the backend
 */

//controller for add service modal, uses broadcasted event to update map list
var MapApp = angular.module('MapApp');

MapApp.controller('AddServiceFromFileModalController',
function ($scope, $rootScope, UtilityFactory, $uibModalInstance, ModalFactory,
toastr, ServiceStore, ImportOverlayService, Upload, IMPORT_PEP_ENDPOINT,
ADD_PACKAGE_AS_SERVICE_ENDPOINT, IMPORT_COMPACT_CACHE_ZIP_ENDPOINT, ServiceFactory) {

    // keeps track of if the user has attempted to add service
    ImportOverlayService.fileForUpload = null;
    $scope.attemptedToAddService = false;
    $scope.uploadAttempt = 0;

    // expose overlay service to html,
    $scope.ImportOverlayService = ImportOverlayService;

    $scope.formData = {
        serviceName: '' ,
        serviceUrl: '',
    }

    // replaces spaces with underscores on the fly in the input field
    $scope.$watch("formData.serviceName", function() {
        $scope.formData.serviceName =
            UtilityFactory.spacesToUnderscores($scope.formData.serviceName);
    })

    // when a file is dropped onto  the add service modal, remove invalid characters
    // from the files name, then set that to the service name
    $scope.$watch("ImportOverlayService.fileForUpload", function() {
        // when a new file is drag and dropped, default the serviceName to the
        // name of the file
        if($scope.formData.serviceName == "" && $scope.isValidFileSelected()) {
            var name = ImportOverlayService.fileForUpload.name;
            // strip file extension from name, and remove special characters
            name = name.substr(0, name.lastIndexOf('.'));
            name = name.replace(/\s+/g, '_');
            name = name.replace(/[^a-zA-Z0-9_\-]/g, '');
            $scope.formData.serviceName = name;
        }
    })

    $scope.isValidFileSelected = function() {
        // if file hasn't been added yet, return false
        if(!$scope.isFileSelected()) {
            return false;
        }
        var acceptedExtensions = ['gpkg', 'pep', 'zip'];
        var extension = ImportOverlayService.fileForUpload.name.split(".").pop();
        //var extension = $rootScope.addServiceByFile_fileForUpload.name.split(".").pop();

        return acceptedExtensions.some(
            function(acceptedExtension) {
                return extension == acceptedExtension;
            }
        )
    }

    $scope.isFileSelected = function() {
        return !(ImportOverlayService.fileForUpload == null);
    }

    $scope.attemptAddService = function() {
        $scope.uploadAttempt++;

        // store local copy of current file for upload
        // IMPORTANT, as this local copy of "name" is protected and will not
        // change when the user attempts to upload another pep or geopackage file.
        // This allows us to have multiple upload operations at the same time.
        var name = ImportOverlayService.fileForUpload.name;

        console.log("Entering attemptAddService()");
        $scope.attemptedToAddService = true;
        var extension = ImportOverlayService.fileForUpload.name.split(".").pop();

        // if there are any invalid inputs in the add service modal form, then...
        if ($scope.addServiceForm.$invalid) {
            // touch invalid inputs, which causes them to display help blocks
            angular.forEach($scope.addServiceForm.$error, function (field) {
                angular.forEach(field, function(errorField) {
                    errorField.$setTouched();
                });
            });
        // valid form!  add new service to server
        } else {
            if(!ServiceFactory.isConnectedToBackend()) {
                UtilityFactory.showCantConnectToBackendError("Can't start service.")
                return;
            }

            console.log("VALID!  beginning upload attempt.");

            // TODO two different functions? one for .pep files, one for geopackages
            if(extension == 'gpkg') {
                var uploadPayload = {
                    url: ADD_PACKAGE_AS_SERVICE_ENDPOINT,
                    data: {
                    },
                };
                uploadPayload.data[$scope.formData.serviceName] = ImportOverlayService.fileForUpload;

                var busyToast = UtilityFactory.spinnerProgressBarToast(
                    "Uploading Geopackage", ImportOverlayService.fileForUpload.name, "my-toast-id" + $scope.uploadAttempt);

            } else if (extension == 'pep') {
                var uploadPayload = {
                    url: IMPORT_PEP_ENDPOINT,
                    data: {
                    },
                };

                uploadPayload.data[$scope.formData.serviceName] = ImportOverlayService.fileForUpload;

                var busyToast = UtilityFactory.spinnerProgressBarToast(
                    "Uploading PEP File", ImportOverlayService.fileForUpload.name, "my-toast-id" + $scope.uploadAttempt);
            }
            else if (extension == 'zip') {
                var uploadPayload = {
                    url: IMPORT_COMPACT_CACHE_ZIP_ENDPOINT,
                    data: {
                        file: ImportOverlayService.fileForUpload,
                        service_name: $scope.formData.serviceName
                    }
                };

                var busyToast = UtilityFactory.spinnerProgressBarToast(
                    "Uploading Compact Cache", ImportOverlayService.fileForUpload.name, "my-toast-id" + $scope.uploadAttempt);
            }

            Upload.upload(
                uploadPayload

            // on success
            ).then(function(resp) {
                ServiceStore.populateServices();
                console.log('Success ' + resp.data + 'uploaded.');
                toastr.clear(busyToast);

                var extensionToType = {
                    gpkg: "GeoPackage",
                    pep:  "PEP file",
                    zip:  "Compact cache"
                }

                toastr.success(extensionToType[extension] + " uploaded successfully.", name);

            // on error
          }, function(err) {
                console.error("Failed to Upload:")
                console.error(err);
                toastr.clear(busyToast);
                ModalFactory.basicError("Failed to Upload " + name, err);

            // fired periodically no matter what, update upload progress while uploading
            }, function(evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                UtilityFactory.setToastProgressBar('my-toast-id' + $scope.uploadAttempt, progressPercentage);
            });

            $scope.closeModal();
        }
    };

    // remove the added file user has already selected, allow them to select
    // a new file.
    $scope.removeFile = function() {
        // clear file
        ImportOverlayService.fileForUpload = null;

        // A "file required error" is displayed by default if the attempts to press
        // the "Add Service" buttom without having drag and dropped a file in.
        // Reseting this variable ensures that this error will not be displayed
        // when the user clears a file he/she has already added
        $scope.attemptedToAddService = false;

        $scope.formData.serviceName = "";
    };

    $scope.closeModal = function () {
        $uibModalInstance.dismiss('cancel');
    };

});
