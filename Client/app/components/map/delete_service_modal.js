/***************************** Export Modal Controller *********************/

MapApp.controller('DeleteServiceModalController',
  function (service, sideNavController, $scope, $uibModalInstance, toastr, UtilityFactory,
  ModalFactory, JobFactory, ServiceFactory, ServiceStore) {

  $scope.service = service;

  $scope.deleteService = function () {
      var service = angular.copy($scope.service)
      var busyToast = UtilityFactory.spinnerToastWithTitle("Attempting to Delete Service", service.name);
      // save a deep copy of the service and all its layers, so we can properly deselect
      // all of the layers, without having to worry whether or not the layers get modified
      // during the asynch ServiceStore.populateServices call
      ServiceStore.deleteService($scope.service).then(function () {
          toastr.clear(busyToast);

          // when a service is deleted, make sure to deselect all of its layers beforehand
          //   - this will ensure that those layers are cleared from the map viewer,
          //     as well as remove them from the Reorder Selected Layers block.
          service.layers.map( function(layer) {
              sideNavController.deselectLayer(layer);
          })
          toastr.success("Service Deleted", service.name);

      }, function(err) {
          toastr.clear(busyToast);

          ModalFactory.basicError("Error: Service " + service.name + " not deleted.", err);
      });
  };


  $scope.closeModal = function () {
      $uibModalInstance.dismiss('cancel');
  };
});
