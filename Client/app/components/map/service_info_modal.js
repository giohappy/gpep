"use strict";

MapApp.controller('ServiceInfoModalController',
function ($scope, $uibModalInstance, $interval, toastr, UtilityFactory, ServiceFactory, ModalFactory, service, serviceName, serviceID) {
    $scope.serviceName = serviceName;
    $scope.serviceID = serviceID;
    $scope.service = service;
    $scope.wmsUrl = UtilityFactory.getWmsCapabilitiesUrl(serviceName);
    $scope.wmtsUrl = UtilityFactory.getWmtsCapabilitiesUrl(serviceName);

    recheckCachingEnabled(true);

    var checkCachingIntervalID;
    var hasCachingErrorModalShown = false;

    // when modal is opened, continuously check if caching is enabled for
    // the relevant service
    $uibModalInstance.opened.then(function() {
        hasCachingErrorModalShown = false;
        recheckCachingEnabled(false);
        checkCachingStatusContinuously();
    });

    function recheckCachingEnabled(add_start_value) {
        //console.log($scope.isCachingAvailable);
        var promise = ServiceFactory.isCachingEnabled($scope.serviceID);
        promise.then(function (value) {
            $scope.isCachingEnabled = value.data.has_cache;
            $scope.isCachingAvailable = value.data.able_to_change;

            if (add_start_value) {
                $scope.desiresCache = value.data.has_cache;
            }
        }, function(err) {
            // show an error modal one time
            if(!hasCachingErrorModalShown) {
                hasCachingErrorModalShown = true;
                ModalFactory.basicError("Error: Service " + serviceName + "'s caching status could not be retreived.",err);
            }
            // afterwards only show error toasts
            // we don't want to stack a ton of error modals
            else {
                toastr.error("Caching status could not be retreived.", serviceName);
            }
        });
    }

    function checkCachingStatusContinuously() {
        checkCachingIntervalID = $interval(function(){recheckCachingEnabled(false)}, 10000);
    }

    function stopCheckingCachingStatus() {
        $interval.cancel(checkCachingIntervalID);
    }

    $scope.enableCache = function() {
        var en_or_dis = "dis";
        if ($scope.desiresCache) {
            en_or_dis = "en";
        }
        if (($scope.desiresCache && $scope.isCachingEnabled) || (!$scope.desiresCache && !$scope.isCachingEnabled)) {
            toastr.success("Caching is already " + en_or_dis + "abled.", $scope.serviceName);
            return;
        }
        var busyToast = UtilityFactory.spinnerToast("Attempting to " + en_or_dis + "able caching", serviceName);
        var promise = ServiceFactory.enableCaching($scope.serviceID, $scope.desiresCache);
        promise.then(function (response) {
            toastr.clear(busyToast);
            if(response.data == true) {
                // then immediabely update info modal to display new changes
                toastr.success("Caching " + en_or_dis + "abled", $scope.serviceName);
                recheckCachingEnabled();
            }
        }, function(err) {
            toastr.clear(busyToast);

            // if json error (bad connection to GPEP server)
            ModalFactory.basicError("Error: Service " + serviceName + "'s cache could not be enabled.",err);
        });
    };


    $scope.closeModal = function () {
        $uibModalInstance.dismiss('cancel');
        // only update status while modal is open
        stopCheckingCachingStatus();
    };

});
