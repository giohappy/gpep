"use strict";

/**
  * This file contains the declaration for MapApp, which contains all
  * controllers, factories, and directive relating to the Map view
  */

var MapApp = angular.module('MapApp', ['rzModule', 'angularMoment']);


MapApp.filter('duration', function(){

  var seconds_in_day = 86400

  return function(seconds){
      var duration = moment.duration(seconds, 'seconds')
      if(seconds > 86400*3){
        return Math.floor(seconds/86400) + " days"
      } else if (seconds > 86400){
        return duration.days() +  "d " + duration.hours() + "h"
      } else{
        return duration.hours() + "h " + duration.minutes() + "m " + duration.seconds() + "s"
      }
  }
})


MapApp.filter( 'filesize', function () {
  
  var units = [
    'bytes',
    'KB',
    'MB',
    'GB',
    'TB',
    'PB'
  ];

  return function(bytes, precision ) {

    if ( isNaN( parseFloat( bytes )) || ! isFinite( bytes ) ) {
      return '?';
    }

    var unit = 0;

    while ( bytes >= 1024 ) {
      bytes /= 1024;
      unit ++;
    }

    return bytes.toFixed( + precision ) + ' ' + units[ unit ];
  };
});
