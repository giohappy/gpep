"use strict";

/**
 * add_service_modal.js gives form validation to the Add Service Modal, and
 * talks to server.js to add a service to the backend
 */

//controller for add service modal, uses broadcasted event to update map list
var MapApp = angular.module('MapApp');

MapApp.controller('AddServiceModalController',
function ($scope, UtilityFactory, $uibModalInstance, ModalFactory, toastr, ServiceStore,
ServiceFactory) {

    $scope.cache_type = undefined;

    $scope.formData = {
        serviceName: '' ,
        serviceUrl: '',
        enableCache: true
    }

    function init() {
        ServiceFactory.getConfigOptions().then(function(response) {
            if(response.data.cache_type == "compact") {
                $scope.cache_type = "ESRI Compact Cache Version " + response.data.compact_cache_version;
            } else {
                $scope.cache_type = response.data.cache_type;
            }
        })
    }
    init();

    // replaces spaces with underscores on the fly in the input field
    $scope.$watch("formData.serviceName", function() {
        $scope.formData.serviceName =
            UtilityFactory.spacesToUnderscores($scope.formData.serviceName);
    })

    $scope.attemptAddService = function() {
        // if there are any invalid inputs in the add service modal form, then...
        if ($scope.addServiceForm.$invalid) {
            // touch invalid inputs, which causes them to display help blocks
            angular.forEach($scope.addServiceForm.$error, function (field) {
                angular.forEach(field, function(errorField){
                    errorField.$setTouched();
                });
            });

        // valid form!  add new service to server
        } else {

            if(!ServiceFactory.isConnectedToBackend()) {
                UtilityFactory.showCantConnectToBackendError("Can't start cleaning job.")
                return;
            }

            var newService = {
                service_name: $scope.formData.serviceName,
                service_url: $scope.formData.serviceUrl,
                enable_cache: $scope.formData.enableCache,
                portal_add: false
            };

            var busyToast = UtilityFactory.spinnerToastWithTitle('Adding Service ', $scope.formData.serviceName);

            ServiceStore.addService(newService).then(function() {
                toastr.clear(busyToast);
                toastr.success("Service successfully added.", $scope.formData.serviceName);
            }, function(err){
		        toastr.clear(busyToast);
                ModalFactory.basicError("Service could not be added.", err);
            })


            $scope.closeModal();
        }
    };

    $scope.closeModal = function () {
        $uibModalInstance.dismiss('cancel');
    };

});
