"use strict";

/**
 * openlayers_controller.js manages the OpenLayers 3 map view.  It does things like
 * initialize the map view, add/remove tile layers, and provide drawing tools
 * for drawing bounding boxes.
 *
 * SIDE NAV
 * It also provides function-hooks through bindings to the side-nav, so when layers
 * are selected or deselected, they are added or removed from the map view.
 *
 * JOB PANEL
 * Settings in the job panel are also reflected in the map view, like the
 * bounding box and zoom levels.
 */

// TODO button for base layers toggle visibility.

var MapApp = angular.module('MapApp');

MapApp.controller('MapController', function ($scope, $rootScope, ServiceStore, $timeout,
   $interval, $http, ServiceFactory, JobFactory, LayersGroupedByMaxZooms, GET_VECTOR_LAYER_ENDPOINT, GEOJSON,
   $location) {


	/* ===================== Overview ====================== */

    $scope.ServiceStore = ServiceStore;
    $scope.LayersGroupedByMaxZooms = LayersGroupedByMaxZooms;
	$scope.JobFactory = JobFactory;
	$rootScope.snappedBoundsGeneric = {					// see init()
		north: -10000,
		east:  -10000,
		south: -10000,
		west:  -10000
	};

	var hiddenCoordinates;								// coordinates used to hide bboxs.
	var drawnBbox;											// ol.geom.Polygon represents the user drawn bbox.
	var snappedBbox;									// ol.geom.Polygon represents the green snapped bbox tied to current minZoom.
	var minZoom;										// current minZoom, used for updating snappedBbox.
	var map;											// ol.Map Open Layers map object
	var controls;										// ol.Collection of map's controls.
	var graticule;										// ol.Graticule used to show grids on map.
	var bboxs_group;									// ol.layer.Group group of all snapped bboxs and the user drawn bbox layer.
	var drawnBboxLayer;									// ol.layer.Vector the user drawn bbox map layer.
	var snappedBboxLayer;								// ol.layer.Vector the green snapped bbox map layer.
    var layersByID;										// see init()
    var lastZIndex;										// see init()
	var isDrawingDrawnBbox;								// true when the user is drawing the user drawn bbox, false otherwise.
	var dragBox;										// ol.interaction.DragBox used to allow user to draw user drawn bbox.

  $scope.$location = $location;

	$scope.showBaseLayers = true;							// true when the base layers widget is being shown on the map, false otherwise.
	$scope.currentBaseLayer;							// ol.layer.[SOME_LAYER_TYPE] the current base layer.
	$scope.baseLayers = undefined;						// an array of all possible base layers (ol.layer.[LAYER_TYPES]). each layer is shown in the base layers widget (map.html).
	$scope.selectBaseLayer;								// give scope access to selectBaseLayer function for map.html.
	var previousBaseLayer;
	$scope.updateBaseLayer;
	$scope.selectLayer;
	$scope.deselectLayer;
	$scope.reorderLayers;

	$scope.drawBounds;
	$scope.toggleVisibility;
	$scope.acceptNewZoomLevels;



	/* initialize module. */
	init();



	/* ===================== Initialization ====================== */

	/**
		Initializes the Open Layers map module.
		This function should only be called once.
	*/
	function init()
	{
		hiddenCoordinates = [[-10000, -100000]];
		drawnBbox = new ol.geom.Polygon([hiddenCoordinates]);
		snappedBbox = new ol.geom.Polygon([hiddenCoordinates]);
		minZoom = 0;

		// when new snapped bounds are calculated, the settings panel needs
		// to know about it.  we make sure to broadcast a generic
		// non-openlayers-specific bounds object, which our jobs panel can then
		// consume.
		$rootScope.snappedBoundsGeneric = {
			north: -10000,
			east:  -10000,
			south: -10000,
			west:  -10000
		};

		map = new ol.Map({
			target: 'map',
			controls: new ol.Collection(),
			view: new ol.View({
				center: [0, 0],
				projection: 'EPSG:4326',
				zoom: 4,
			})
		});

		graticule = new ol.Graticule({
			strokeStyle: new ol.style.Stroke({
				color: 'rgba(0,0,0,0.5)',
				width: 2,
				lineDash: [0.5, 4]
			}),
			showLabels: true
		});

		// Used to keep track of all layers currently added to the map.
		// Makes it easy to change the depth ordering of the layers.
		//
		// It is a <key, value> dictionary of <ID, LAYER> where
		//    ID is a layer ID
		//    VALUE is an instantiated ol.layer.Tile corresponding to that layer ID
		layersByID = {};

		// used to keep track of depth of most recently added layer
		// note: larger z-values render over lower z-values
		lastZIndex = 0;

		addControls();
		addDefaultBaseLayers();
		addBboxLayers();

		isDrawingDrawnBbox = false;
		dragBox = new ol.interaction.DragBox({
			condition: getBBoxDrawCondition
		});
		map.addInteraction(dragBox);

		setInteractive($rootScope.isMapCurrentView());
	}

	/**
		Creates the custom Open Layers controls for the map.
		Adds those controls to the map.
		Also sets up map overlay.
		This function should only be called once. (called from init())
	*/
	function addControls()
	{
		map.addControl(new ol.control.Zoom());

		/* draw buttons div */
		var drawDiv = document.createElement('div');
		drawDiv.className = 'draw-div ol-unselectable ol-control';

		var drawButton = document.createElement('button');
		drawButton.innerHTML = 'Draw';
		drawButton.setAttribute("id", "map-draw-button");
		drawButton.setAttribute("title", "Draw Bounds");
		drawDiv.appendChild(drawButton);
		var handleDraw = function(e) {
			toggleBBoxDrawCondition();
		};
		drawButton.addEventListener('click', handleDraw, false);
		drawButton.addEventListener('touchstart', handleDraw, false);

		var resetButton = document.createElement('button');
		resetButton.innerHTML = 'Reset';
		resetButton.setAttribute("id", "map-reset-button");
		resetButton.setAttribute("title", "Reset Bounds");
		drawDiv.appendChild(resetButton);
		var handleReset = function(e) {
			resetBBoxs();
		};
		resetButton.addEventListener('click', handleReset, false);
		resetButton.addEventListener('touchstart', handleReset, false);

		var drawControl = new ol.control.Control({
			className: 'draw-control',
			element: drawDiv,
			target: document.getElementById("draw-div"),
		});
		map.addControl(drawControl);


		/* current zoom div */
		var currentZoomDiv = document.createElement('div');
		currentZoomDiv.className = 'current-zoom-div ol-unselectable ol-control';
		var currentZoomButton = document.createElement('button'); // decided to keep things consistent even though this button will do nothing.
		currentZoomButton.innerHTML = Math.trunc(map.getView().getZoom());
		currentZoomButton.setAttribute("id", "map-current-zoom-button");
		currentZoomButton.setAttribute("title", "Zoom Level");
		currentZoomDiv.appendChild(currentZoomButton);
		var handleCurrentZoom = function(e) {
			// intentionally blank. (put some test hacky stuff here?)
		};
		currentZoomButton.addEventListener('click', handleCurrentZoom, false);
		currentZoomButton.addEventListener('touchstart', handleCurrentZoom, false);
		var currentZoomControl = new ol.control.Control({
			className: 'current-zoom-control',
			element: currentZoomDiv,
			target: document.getElementById("current-zoom-div"),
		});
		map.addControl(currentZoomControl);

		/* grid toggle button */
		var gridDiv = document.createElement('div');
		gridDiv.className = 'grid-div ol-unselectable ol-control';
		var gridButton = document.createElement('button');
		gridButton.innerHTML = 'Grid';
		gridButton.setAttribute("id", "map-grid-button");
		gridButton.setAttribute("title", "Toggle Map Grid");
		gridDiv.appendChild(gridButton);
		var handleGrid = function(e) {
			// toggle
			if(graticule.getMap() == null)
				graticule.setMap(map);
			else
				graticule.setMap(null);
		};
		gridButton.addEventListener('click', handleGrid, false);
		gridButton.addEventListener('touchstart', handleGrid, false);
		var gridControl = new ol.control.Control({
			className: 'grid-control',
			element: gridDiv,
			target: document.getElementById("grid-div"),
		});
		map.addControl(gridControl);

		/* popup */
		var popupDiv = document.createElement('div');
		popupDiv.setAttribute("id", "popup-div");
		popupDiv.className = 'ol-unselectable ol-control';
		popupDiv.style.display = "none";
		var popupText = document.createElement('button');
		popupText.setAttribute("id", "popup-button");
		popupText.innerHTML = "Click and drag to draw bounds";
		popupDiv.appendChild(popupText);
		var popupControl = new ol.control.Control({
			className: 'popup-control',
			element: popupDiv,
			target: document.getElementById("popup-div"),
		});
		map.addControl(popupControl);


		/* base layers */

		// var baseLayersButtonDiv = document.createElement('div');
		// baseLayersButtonDiv.setAttribute("id", "base-layers-button-div");
		// baseLayersButtonDiv.className = 'ol-unselectable ol-control';
		// var baseLayersButton = document.createElement('button');
		// baseLayersButton.setAttribute("id", "base-layers-button");
		// baseLayersButton.innerHTML = "Base Layers";
		// baseLayersButtonDiv.appendChild(baseLayersButton);
		// var handleBaseLayersButton = function(e) {
		// 	// toggle
		// 	showBaseLayers = !showBaseLayers;
		// 	var baseLayerDiv = document.getElementById('baselayer-wrapper-div');
		// 	if(showBaseLayers)
		// 		baseLayerDiv.style.display = "block";
		// 	else
		// 		baseLayerDiv.style.display = "none";
		// };
		// baseLayersButton.addEventListener('click', handleBaseLayersButton, false);
		// baseLayersButton.addEventListener('touchstart', handleBaseLayersButton, false);
		// var baseLayersButtonControl = new ol.control.Control({
		// 	className: 'base-layers-button-control',
		// 	element: baseLayersButtonDiv,
		// 	target: document.getElementById("base-layers-button-div"),
		// });
		// map.addControl(baseLayersButtonControl);
    //
		// showBaseLayers = false;
		// var baseLayerDiv = document.getElementById('baselayer-wrapper-div');
		// baseLayerDiv.style.display = "none";

    $scope.showBaseLayers = true;

		controls = map.getControls();
	}

	/**
	 * Adds the default base layers to the map.
	 * This function should only be called once. (called from init())
	 */
	function addDefaultBaseLayers()
	{
		$scope.selectBaseLayer = selectBaseLayer;
		$scope.baseLayers = {};
		$scope.currentBaseLayer = null;
		previousBaseLayer = null;

		// TileWMS vs XYZ
		var openStreetMapBlackAndWhiteLayer = new ol.layer.Tile({
			id: 'BASELAYER_1',
			layer_name: "Open Street Map Black And White",
			source: new ol.source.XYZ({
				url: 'https://tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png',
				wrapX: false,
			})
		});
		var openStreetMapMapnikLayer = new ol.layer.Tile({
			id: 'BASELAYER_2',
			layer_name: "Open Street Map Mapnik",
			source: new ol.source.XYZ({
				url: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
				wrapX: false,
			})
		});

		addOLSLayerToMap(openStreetMapBlackAndWhiteLayer);
		addOLSLayerToMap(openStreetMapMapnikLayer);
		setLayerAsBaseLayer(openStreetMapBlackAndWhiteLayer, true);
		setLayerAsBaseLayer(openStreetMapMapnikLayer, true);
		$scope.selectBaseLayer(openStreetMapMapnikLayer.id);

		ServiceFactory.getDefaultBaselayer().then(function(response) {
			var baselayer = response.data;
			var gpepDefaultBaseLayer = new ol.layer.Tile({
				id: 'BASELAYER_3',
				layer_name: baselayer.title,
				source: new ol.source.XYZ({
					url: baselayer.url,
					maxZoom: baselayer.max_zoom,
					wrapX: false,
				})
			});
			addOLSLayerToMap(gpepDefaultBaseLayer);
			setLayerAsBaseLayer(gpepDefaultBaseLayer, true);
			$scope.selectBaseLayer(gpepDefaultBaseLayer.id);

		}, function(error) {
			console.error("Failed to retrieve default baselayer.");
            console.error(error);
		});
	}

	/**
	 * Adds layers used to display user drawn bbox and green snapping bbox to the map.
	 * This function should only be called once. (called from init())
	 */
	function addBboxLayers()
	{
		var bbox_feature = new ol.Feature(drawnBbox);
		// polygon.transform('EPSG:4326', 'EPSG:3857');
		var vectorSource = new ol.source.Vector({wrapX: false});
		vectorSource.addFeature(bbox_feature);
		drawnBboxLayer = new ol.layer.Vector({
			source: vectorSource,
			style: new ol.style.Style({
				fill: new ol.style.Fill({
					color: 'rgba(255, 255, 255, 0.2)'
				}),
			   stroke: new ol.style.Stroke({	// if you ever change this be sure to also change .ol-box in openlayers.scss
					color: '#ff0000',
					width: 10
				}),
				image: new ol.style.Circle({
					radius: 7,
					fill: new ol.style.Fill({
						color: '#ffcc33'
					})
				})
			}),
		});

		var snapped_bbox_feature = new ol.Feature(snappedBbox);
		// polygon.transform('EPSG:4326', 'EPSG:3857');
		var vectorSource2 = new ol.source.Vector({wrapX: false});
		vectorSource2.addFeature(snapped_bbox_feature);
		snappedBboxLayer = new ol.layer.Vector({
			source: vectorSource2,
			style: new ol.style.Style({
				fill: new ol.style.Fill({
					color: 'rgba(255, 255, 255, 0.2)'
				}),
			   stroke: new ol.style.Stroke({
					color: '#00ff00',
					width: 10,
					lineDash: [50, 30],
				}),
				image: new ol.style.Circle({
					radius: 7,
					fill: new ol.style.Fill({
						color: '#ff0000'
					})
				})
			}),
		});

		bboxs_group = new ol.layer.Group();
		bboxs_group.getLayers().push(drawnBboxLayer);
		bboxs_group.getLayers().push(snappedBboxLayer);
		map.addLayer(bboxs_group);
		//addTestWFSLayer();

		//  addGeojsonVectorLayer Test
		//var layer = addGeojsonVectorLayer({title: "hello", id: 111}, {url: "service url"});
		//map.addLayer(layer);
		//layer.setZIndex(100000);
	}

	/**
		Toggle the map's interactive features (controls, bbox layers, base layers widget) and update layers visually.
		@parameter interactive - true if map should be interactive, false otherwise.
	*/
	function setInteractive(interactive)
	{
		var i;
		var interactions = map.getInteractions().getArray();
		for(i = 0; i < interactions.length; i++)
		{
			interactions[i].setActive(interactive);
		}

		var mapLayers = map.getLayers().getArray();
		for(i = 0; i < mapLayers.length; i++)
		{
			if(interactive)
				mapLayers[i].setOpacity(1);
			else
				mapLayers[i].setOpacity(.5);
		}
		bboxs_group.setVisible(interactive);

		var m = null;
		if(interactive)
			m = map;
		var ctrls = controls.getArray();
		for(i = 0; i < ctrls.length; i++)
		{
			ctrls[i].setMap(m);
		}

		var baseLayerDiv = document.getElementById('baselayer-wrapper-div');
		if(interactive)
			baseLayerDiv.style.display = "block";
		else
			baseLayerDiv.style.display = "none";
	}


	/* ===================== Events ====================== */

	/**
		Sync popup control with cursor.
	*/
	map.on('pointermove', function(e){
		var popupDiv = document.getElementById('popup-div');
		if(popupDiv == null)
		{
			// control is not in map
			return;
		}

		var coordinate = e.coordinate;
		var pixel = map.getPixelFromCoordinate(coordinate);

		var style_width = 180;	 // if you change this be sure to also change #popup-div in openlayers.scss
		var vertical_adjust = 40 /* chosen to center popup with cursor.
								    if the width of the div changes this will probably need to be changed.
								 */

		popupDiv.style.left = "" + (pixel[0] - style_width / 2)  + "px";
		popupDiv.style.top = "" + (pixel[1] - vertical_adjust) + "px";
	});

	/**
		Update zoom control when map zooms.
	*/
	map.getView().on('propertychange', function(){
		 var zoom = map.getView().getZoom();
		 var currentZoomButton = document.getElementById('map-current-zoom-button');
		 currentZoomButton.innerHTML = Math.round(zoom);
	});

	// when layers are selected or deselected, update grouped layers
    $scope.$watch('ServiceStore.selectedLayersKeys', function(newValue, oldValue) {
        LayersGroupedByMaxZooms.updateGroupedLayers()
		updateSnappedBBoxs();
    }, true)

	// when users toggle set individual layers, update grouped layers
    $scope.$watch('JobFactory.setLevelsPerLayer', function(newValue, oldValue) {
		LayersGroupedByMaxZooms.updateGroupedLayers()
		updateSnappedBBoxs();
    }, true)

	// when users move individual layers sliders, update grouped layers
	$scope.$watch('ServiceStore.getZoomLevelModalSettings()', function(newVal, oldVal) {
		LayersGroupedByMaxZooms.updateGroupedLayers()
		updateSnappedBBoxs();
    }, true);

	$scope.$on('pageChange', function(){
		var interactive = $rootScope.isMapCurrentView();
		setInteractive(interactive);
	})

	$scope.$on('toggleBBoxDrawCondition', function(){
		toggleBBoxDrawCondition();
	})

	$scope.$on('newMinZoomLevel', function(event, zoom){
		minZoom = zoom;
		updateSnappedBBoxs();
	})

	$scope.$on('updateDrawBounds', function(event, bounds){
		$scope.drawBounds(bounds);
	});

    /* ===================== Box Selection Interaction ====================== */

	dragBox.on('boxend', function() {
		var coordinates = dragBox.getGeometry().getCoordinates();
		drawnBbox.setCoordinates(coordinates);

		var extent = dragBox.getGeometry().getExtent();
		var bounds = extentToBounds(extent);

		updateSnappedBBoxs();
		$rootScope.$broadcast('newSelectionBoundsDrawn', bounds);
		$scope.$broadcast("drawingModeExited");
	});

	dragBox.on('boxstart', function() {
		resetBBoxs();
		setBBoxDrawCondition(false);
		$scope.$broadcast("drawBoundsToolActivated");
	});

	function getBBoxDrawCondition()
	{
		return isDrawingDrawnBbox;
	}

	function setBBoxDrawCondition(value)
	{
		isDrawingDrawnBbox = value;
		var popupDiv = document.getElementById('popup-div');
		if(value)
		{
			popupDiv.style.display = "block";
		}
		else
		{
			popupDiv.style.display = "none";
		}
	}

	function toggleBBoxDrawCondition()
	{
		setBBoxDrawCondition(!isDrawingDrawnBbox);
	}


	 /* ===================== Bbox and Snapped Bbox Updating ====================== */

	/**
		Hides all bbox layers.
	*/
	function resetBBoxs()
	{
		drawnBbox.setCoordinates([hiddenCoordinates]);
		bboxs_group.getLayers().clear();

		// clear bounding box text inputs.
		$scope.$broadcast("drawBoundsToolActivated");
	}

	/**
		Calculates snapped bounds for a snapped bbox.
		@parameter bounds - the lat-lon bounds.
		@parameter zoom - the min zoom level.
		@return snapped lat-lon bounds.
	*/
	function getSnappingBounds(bounds, zoom)
	{
		var widthInTiles = Math.pow(2, zoom + 1);
		var heightInTiles = Math.pow(2, zoom);

		var worldWidthLon = 360;
		var worldHeightLat = 180;
		var tileWidth = worldWidthLon / widthInTiles;
		var tileHeight = worldHeightLat / heightInTiles;

		var bounds_normal = [bounds.north + 90, bounds.south + 90, bounds.east + 180, bounds.west + 180];
		var box = [Math.floor(bounds_normal[0] / tileHeight) * tileHeight + tileHeight,
		Math.floor(bounds_normal[1] / tileHeight) * tileHeight,
		Math.floor(bounds_normal[2] / tileWidth) * tileWidth + tileWidth,
		Math.floor(bounds_normal[3] / tileWidth) * tileWidth];

		var snappingBounds = {};
		snappingBounds.north = box[0] - 90;
		snappingBounds.south = box[1] - 90;
		snappingBounds.east = box[2] - 180;
		snappingBounds.west = box[3] - 180;
		correctBbox(snappingBounds);

		return snappingBounds;
	}

	/**
	 * Normalizes the bounds of bbox not to exceed maximum values.
	 */
	function correctBbox(bbox)
	{
		// console.log("correcting bbox: ", bbox);
		if(bbox.north > 90)
			bbox.north = 90;
		else if(bbox.north < -90)
			bbox.noth = -90;
		if(bbox.south > 90)
			bbox.south = 90;
		else if(bbox.south < -90)
			bbox.south = -90;
		if(bbox.east > 180)
			bbox.east = 180;
		else if(bbox.east < -180)
			bbox.east = -180;
		if(bbox.west > 180)
			bbox.west = 180;
		else if(bbox.west < -180)
			bbox.west = -180;
	}

	/**
		Updates the snapped bbox layers to reflect changes.
		Colors the largest snapped bbox for visual selection effect.
	*/
	function updateSnappedBBoxs()
	{
		var bbox_extent = drawnBbox.getExtent();
		var bounds = extentToBounds(bbox_extent);

		updateSnappedBboxsLayers(bounds);
		var largest = getLargestSnappedBboxLayer();
		largest.getStyle().getFill().setColor('rgba(255, 255, 255, 0.2)');
	}

	/**
		Updates the snapped bbox layers to reflect changes. (called from updateSnappedBBoxs())
		Adds bbox layers when user is selecting zoom levels for individual layers.
	*/
	function updateSnappedBboxsLayers(bounds)
	{
		bboxs_group.getLayers().clear();
		bboxs_group.getLayers().push(drawnBboxLayer); // add back bbox layer
		if(JobFactory.setLevelsPerLayer) {
			var dash = 30; // attempt to not have total overlap of layer boxes.
            LayersGroupedByMaxZooms.groupedLayers.forEach(function(group) {
				var x = JobFactory.snappedBoundsAllLevels[group.minZoom];
                JobFactory.snappedBoundsAllLevels[group.minZoom] = getSnappingBounds(bounds, group.minZoom);
				var indiv_bbox = new ol.geom.Polygon([hiddenCoordinates]);
				var indiv_bbox_feature = new ol.Feature(indiv_bbox);
				// polygon.transform('EPSG:4326', 'EPSG:3857');
				var indiv_vectorSource = new ol.source.Vector({wrapX: false});
				indiv_vectorSource.addFeature(indiv_bbox_feature);
				var indiv_vectorLayer = new ol.layer.Vector({
					source: indiv_vectorSource,
					style: new ol.style.Style({
						fill: new ol.style.Fill({
							color: 'rgba(255, 255, 255, 0.0)'
						}),
					   stroke: new ol.style.Stroke({
							color: group.color,
							width: 10,
							lineDash: [50, dash],
						}),
						image: new ol.style.Circle({
							radius: 7,
							fill: new ol.style.Fill({
								color: '#ffcc33'
							})
						})
					}),
				});
				bboxs_group.getLayers().push(indiv_vectorLayer);
				updateLayerZIndices();
				dash = dash + 10;

				var b = JobFactory.snappedBoundsAllLevels[group.minZoom]; // alias ... to many words
				var coordinates = [[b.west, b.north], [b.west, b.south], [b.east, b.south],
					[b.east, b.north], [b.west, b.north]];
				indiv_bbox.setCoordinates([coordinates]);

				if(JobFactory.currentSettingsPanelTab != JobFactory.PACKAGE_JOBTYPE) {
					indiv_bbox.setCoordinates([hiddenCoordinates]);
					return;
				}

				// job factory expects these functions ...
				JobFactory.snappedBoundsAllLevels[group.minZoom].getNorth = function(){
					return JobFactory.snappedBoundsAllLevels[group.minZoom].north;
				}
				JobFactory.snappedBoundsAllLevels[group.minZoom].getSouth = function(){
					return JobFactory.snappedBoundsAllLevels[group.minZoom].south;
				}
				JobFactory.snappedBoundsAllLevels[group.minZoom].getEast = function(){
					return JobFactory.snappedBoundsAllLevels[group.minZoom].east;
				}
				JobFactory.snappedBoundsAllLevels[group.minZoom].getWest = function(){
					return JobFactory.snappedBoundsAllLevels[group.minZoom].west;
				}
            })
        }
		else {
			bboxs_group.getLayers().push(snappedBboxLayer);

			if(JobFactory.currentSettingsPanelTab != JobFactory.PACKAGE_JOBTYPE) {
				snappedBbox.setCoordinates([hiddenCoordinates]);
				return;
			}

			var snappedBounds = getSnappingBounds(bounds, minZoom);
			var coordinates = [[snappedBounds.west, snappedBounds.north],
				[snappedBounds.west, snappedBounds.south],
				[snappedBounds.east, snappedBounds.south],
				[snappedBounds.east, snappedBounds.north],
				[snappedBounds.west, snappedBounds.north]];

			snappedBbox.setCoordinates([coordinates]);

			$rootScope.snappedBoundsGeneric.north = snappedBounds.north;
			$rootScope.snappedBoundsGeneric.south = snappedBounds.south;
			$rootScope.snappedBoundsGeneric.east = snappedBounds.east;
			$rootScope.snappedBoundsGeneric.west = snappedBounds.west;
			$scope.$broadcast('newSnappedBounds', $rootScope.snappedBoundsGeneric);
		}
	}

	/**
		Returns the largest snapped bbox layer in the map.
	*/
	function getLargestSnappedBboxLayer()
	{
		var bbox_layers = bboxs_group.getLayers().getArray();
		var largest_bbox = bbox_layers[0];

		var i;
		for(i = 0; i < bbox_layers.length; i++)
		{
			var bbox_polygon = bbox_layers[i].getSource().getFeatures()[0].getGeometry();
			var largest_bbox_polygon = largest_bbox.getSource().getFeatures()[0].getGeometry();
			var size = ol.extent.getSize(bbox_polygon.getExtent());
			var size2 = ol.extent.getSize(largest_bbox_polygon.getExtent());

			if(size[0] * size[1] > size2[0] * size2[1])
				largest_bbox = bbox_layers[i];
		}

		return largest_bbox;
	}

    /* ========================== Bindings to SideNav ==========================*/

    /**
      Called when a layer is selected in the side-nav.  Add that layer to the map.
    */
    $scope.selectLayer = function(layer, service) {

		// TODO check if layer is raster or vector
		// use make...Layer functions
		// SAMPLE
		/*
		if(layer.type == 'raster')
		{
			addWMTSLayer(layer, service);
		}
		else
		{
			addGeojsonVectorLayer(layer, service);
		}
		...
		*/
		if(layer.is_base_layer)
		{
			if(layersByID[layer.id] == undefined || layersByID[layer.id] == null)
			{
				var tileLayer2 = makeTileWMSLayer(layer, service);
				addOLSLayerToMap(tileLayer2);
				updateLayerZIndices();
				setLayerAsBaseLayer(tileLayer2, true);
			}
			else if($scope.baseLayers[layer.id] == undefined || $scope.baseLayers[layer.id] == null)
			{
				setLayerAsBaseLayer(layersByID[layer.id], true);
			}
			selectBaseLayer(layer.id);
		}
		else
		{
			var tileLayer = makeTileWMSLayer(layer, service);
			addOLSLayerToMap(tileLayer);
			updateLayerZIndices();

			// Center map on layer.
			// This happens to work. Be careful?
			var bounds = extentToBounds(layer.bbox); // This happens to work. Be careful?
			correctBbox(bounds);	// should not have to do this...? why does the back end return bad bounds sometimes?
			var extent = [bounds.west, bounds.south, bounds.east, bounds.north];
			map.getView().fit(extent);
		}
    }

    /**
      Called when a layer is deselected in the side-nav.  Remove that layer
      from the map.
    */
    $scope.deselectLayer = function(layer) {
		if(layer.is_base_layer)
		{
			if(layer.id == $scope.currentBaseLayer.id)
			{
				selectBaseLayer("BASELAYER_1");
			}
			else
			{
				selectBaseLayer($scope.currentBaseLayer.id);
			}
		}
		else
		{
			removeLayerByID(layer.id);
			updateLayerZIndices();

			if($scope.baseLayers[layer.id] != undefined)
			{
				delete $scope.baseLayers[layer.id];
			}
		}
    }

    /**
      Called when layers are reordered in the "View Selected Layers" block in
      the side-nav.  Sets the layer depths in the map view, where the first
      layer in the "View Selected Layers" section is rendered above every other
      layer, and the last layer is rendered on the bottom.
    */
    $scope.reorderLayers = function(orderedLayerIds) {
        setLayerDepthOrder(orderedLayerIds);
		updateLayerZIndices();
    }

	// When the bounding box is modified from the settings panel, update
    // it in the openlayers map view
    $scope.drawBounds = function(bounds) {
        if(typeof bounds.north == "undefined" ||
           typeof bounds.south == "undefined" ||
           typeof bounds.east == "undefined" ||
           typeof bounds.west == "undefined")
        {
          return;
        }
		var coordinates = [[bounds.west, bounds.north],
			[bounds.west, bounds.south],
			[bounds.east, bounds.south],
			[bounds.east, bounds.north],
			[bounds.west, bounds.north]];

		drawnBbox.setCoordinates([coordinates]);
		updateSnappedBBoxs();

    };

	$scope.toggleVisibility = function(layer){
		var toVisibility = !layersByID[layer.id].getVisible();
		layersByID[layer.id].setVisible(toVisibility);
	};

	// ?
	$scope.acceptNewZoomLevels = function(newZoomLevels) {
		$scope.minZoom = newZoomLevels.min;
		$scope.maxZoom = newZoomLevels.max;
	}

	/**
      Calls periodically, passing in the current default base layer
      (not openlayers layer, just the regular layer object that the sidenav uses.

      This function will keep track of which base layer to keep in the base layer widget.
    */
    $scope.updateBaseLayer = function(baseLayer) {
		if(baseLayer == undefined || $scope.baseLayers == undefined)
		{
			// chill out, let the stuff initialize....
			return;
		}
		else if(previousBaseLayer == null)
		{
			setLayerAsBaseLayer(layersByID[baseLayer.id], true);
		}
		else if(previousBaseLayer.id != baseLayer.id)
		{
			setLayerAsBaseLayer(layersByID[baseLayer.id], true);
			setLayerAsBaseLayer(layersByID[previousBaseLayer.id], false);
		}

		if($scope.baseLayers[baseLayer.id] == undefined) // my goodness, keep trying this
		{
			setLayerAsBaseLayer(layersByID[baseLayer.id], true);
		}

		previousBaseLayer = baseLayer;
	}

	// send a message to the sidenav letting it know that the baselayer has
    // been switched
    function tellSidenavAboutBaselayerChange(oldBaseLayerId, newBaseLayerId) {
        var data = {
            oldBaseLayerId: oldBaseLayerId,
            newBaseLayerId: newBaseLayerId
        };
        $scope.$broadcast("baseLayerChanged", data);
    }

	/* ======================== Helpers =========================== */

	// TODO use
	function addLayer(layer, service)
	{
		if(layer == undefined || layer == null)
		{
			console.log("ERROR: adding undefined/null layer.");
			return null;
		}

		if(layer.layer_type == undefined || layer.layer_type == null)
		{
			console.log("ERROR: cannot determine layer type.");
			return null;
		}

		if(layer.layer_type == 'raster')
		{
			return addWMTSLayer(layer, service);
		}
		else if(layer.layer_type == 'vector')
		{
			return addGeojsonVectorLayer(layer, service);
		}
		else
		{
			console.log("ERROR: adding layer with bad layer type.");
			return null;
		}
	}

	function addWMTSLayer(layer, service)
	{
		var url;
		if(service == null || service == undefined)
		{
			url = layer.url;
		}
		else
		{
			url = service.url;
		}

		var tileLayer = new ol.layer.Tile({
			id: layer.id, //custom parameters we add to keep track of layers
			layer_name: layer.title,

            source: new ol.source.TileWMS({
                url: url,
                params: {
                    'LAYERS': layer.name,
                },
				wrapX: false,
            })
		});

		addOLSLayerToMap(tileLayer);
		updateLayerZIndices();
		return tileLayer;
	}

	function addGeojsonVectorLayer(layer, service)
	{
		// layer styling
		var fill = new ol.style.Fill({color: 'blue'});
		var stroke = new ol.style.Stroke({color: 'blue', width: 2,});
		var image = new ol.style.Circle({radius: 12, fill: fill,});
		var styles = {};
		styles['Point'] = new ol.style.Style({image: image,});
		styles['LineString'] = new ol.style.Style({stroke: stroke,});
		styles['MultiLineString'] = new ol.style.Style({stroke: stroke,});
		styles['MultiPoint'] = new ol.style.Style({image: image,});
		styles['MultiPolygon'] = new ol.style.Style({stroke: stroke,});
		styles['Polygon'] = new ol.style.Style({stroke: stroke,});
		styles['Circle'] = new ol.style.Style({stroke: stroke,});
		styles['LinearRing'] = new ol.style.Style({stroke: stroke,});				// needed?
		styles['GeometryCollection'] = new ol.style.Style({stroke: stroke,});		// needed?

		var styleFunction = function(feature){
			// In case someone wants to use this later for displaying feature properties.
			var t = "";
			for(var property in feature.getProperties())
			{
				t += property + " : " + feature.getProperties()[property];
			}

			var geometry = feature.getGeometry();
			return styles[geometry.getType()];
		};

		var format = new ol.format.GeoJSON();
		var vectorLoader = function(extent, resolution, projection) {
			var sourceObject = this;
			if(!sourceObject.shouldLoad)
			{
				sourceObject.shouldLoad = true;
				return; // avoid nested load / infinite clear-load loop
			}

			var url = GET_VECTOR_LAYER_ENDPOINT;
			var data = {
				layerID: layer.id,
				bbox: extentToBounds(extent),
				outputType: GEOJSON,
			};

			$http.post(url, data).then(function(response){
				var geojson = response.data;
				var features = format.readFeatures(geojson);
				sourceObject.clear();
				sourceObject.addFeatures(features);
			}, function(err){
				console.log(":( Could not load vector features.");
			});
		};

		var source = new ol.source.Vector({
			shouldLoad: true, // custom parameter to prevent infinite loop when calling source.clear() loop is clear->vectorLoader->clear->vectorLoader->...
						   	  // dunno why it happens

			format: format,
			loader: vectorLoader,
			strategy: ol.loadingstrategy.bbox,
			wrapX: false,
		});

		source.on('clear', function(event){
			source.shouldLoad = !source.shouldLoad;
		});

		var vectorLayer = new ol.layer.Vector({
			id: layer.id, //custom parameters we add to keep track of layers
			layer_name: layer.title,
			service_url: service.url,

			source: source,
			style: styleFunction,
		});

		addOLSLayerToMap(vectorLayer);
		updateLayerZIndices();
		return vectorLayer;
	}

    /**
      Provide a list of layer id's, in order of depth, which will be used
      to reset the z-index's of all the layers.
    */
    function setLayerDepthOrder(layerIDs) {
        lastZIndex = 0; // reset starting depth before reordering

        // reverse layer IDs for readability, so we don't need to make a
        // backwards for-loop
        var reversedLayerIDs = angular.copy(layerIDs).reverse()

        for (var i = 0; i < reversedLayerIDs.length; i++) {
            var ID = reversedLayerIDs[i]
            var layer = layersByID[ID];
            layer.setZIndex(lastZIndex);
            lastZIndex++;
        }
    }

    function removeLayerByID(layerID) {
        var layer = layersByID[layerID];
        map.removeLayer(layer); // remove from map view
		delete layersByID[layerID]; // remove from stored layer list
    }

    /**
      Takes as input a "layer" object and "service" object provided by the backend,
      and turns them into an OLS layer.
      @returns OpenLayers TileWMS layer, which can be added to the map.
    */
    function makeTileWMSLayer(layer, service) {

		var url;
		if(service == null || service == undefined)
		{
			url = layer.url;
		}
		else
		{
			url = service.url;
		}

        return new ol.layer.Tile({
			id: layer.id, //custom parameters we add to keep track of layers
			layer_name: layer.title,

            source: new ol.source.TileWMS({
                url: url,
                params: {
                    'LAYERS': layer.name,
                },
				wrapX: false,
            })
		})
	}

    /**
      Properly adds an OpenLayers layer to the map, by also updating the
      layersByID object.
    */
    function addOLSLayerToMap(layer) {
        map.addLayer(layer);
		layer.id = layer.values_.id;
		layer.layer_name = layer.values_.layer_name;
        layersByID[layer.values_.id] = layer;
    }

	/**
	 * Updates the map to use the specified base layer. Hides all other base layers.
	 * if the layerId is not a base layer, the corresponding layer is made a base layer.
	 */
	function selectBaseLayer(layerId)
	{
		if($scope.baseLayers[layerId] == undefined)
		{
			// bruh
			//console.log("ERROR SELECTBASELAYER");
			//console.log(layerId);
			//console.log($scope.baseLayers);
			return;
		}
		else if($scope.currentBaseLayer != null && $scope.currentBaseLayer.id == layerId)
		{
			return;
		}

		var property;
		var oldBaseLayer = $scope.currentBaseLayer;
		for(property in $scope.baseLayers)
		{
			if($scope.baseLayers.hasOwnProperty(property))
			{
				var baselayer = $scope.baseLayers[property];
				if(baselayer == undefined)
				{
					continue; // :(
				}

				if(property == layerId)
				{
					if(!baselayer.getVisible())
					{
						baselayer.setVisible(true);
					}

					$scope.currentBaseLayer = baselayer;
				}
				else
				{
					if(baselayer.getVisible())
					{
						baselayer.setVisible(false);
					}
				}
			}
		}

		var oldId = null;
		if(oldBaseLayer != null)
		{
			oldId = oldBaseLayer.id;
		}
		tellSidenavAboutBaselayerChange(oldId, $scope.currentBaseLayer.id);
	}

	function setLayerAsBaseLayer(olLayer, isBaseLayer)
	{
		if(olLayer == undefined)
		{
			return;
		}

		if(isBaseLayer)
		{
			$scope.baseLayers[olLayer.id] = olLayer;
		}
		else
		{
			delete $scope.baseLayers[olLayer.id];
		}
	}

	/*
		Determines if a layer is contained in the map.
	*/
	function mapContainsLayer(layer)
	{
		var layers = map.getLayers().getArray();
		var i;
		for(i = 0; i < layers.length; i++)
		{
			if(layers[i].id == layer.id)
				return true;
		}

		return false;
	}

	/*
		Takes an openlayers extent (or an array set up the same way as one) as converts it
		to a bounds object with north, south, east, west properties.
	*/
	function extentToBounds(extent)
	{
		var bounds = {};
		bounds.north = extent[3];
		bounds.south = extent[1];
		bounds.east = extent[2];
		bounds.west = extent[0];
		return bounds;
	}

	function updateLayerZIndices()
	{
		drawnBboxLayer.setZIndex(map.getLayers().getLength());
		snappedBboxLayer.setZIndex(map.getLayers().getLength() - 1);

		var groupLayers = bboxs_group.getLayers().getArray();
		var i;
		for(i = 0; i < groupLayers.length; i++)
		{
			var layer = groupLayers[i];
			layer.setZIndex(map.getLayers().getLength() - 1);
		}
	}


	/*
	function addTestWFSLayer()
	{

		var vectorSource = new ol.source.Vector({
			format: new ol.format.GeoJSON(),
			url: function(extent) {
				return 'https://ahocevar.com/geoserver/wfs?service=WFS&' +
				'version=1.1.0&request=GetFeature&typename=osm:water_areas&' +
				'outputFormat=application/json&srsname=EPSG:3857&' +
				'bbox=' + extent.join(',') + ',EPSG:4326';
			},
			strategy: ol.loadingstrategy.bbox
		});

		var vector = new ol.layer.Vector({
			source: vectorSource,
			style: new ol.style.Style({
				stroke: new ol.style.Stroke({
				color: 'rgba(0, 0, 255, 1.0)',
				width: 2
				})
			})
		});

		//map.addLayer(vector);

		// ImageVector seems to perform a lot better when there is a lot of data to display.
		var layer = new ol.layer.Image({
            source: new ol.source.ImageVector({
              source: //new ol.source.Vector({
                //url: 'https://openlayers.org/en/v4.4.2/examples/data/geojson/countries.geojson',
                //format: new ol.format.GeoJSON()
			  //})
			  vectorSource,
              style: new ol.style.Style({
                fill: new ol.style.Fill({
                  color: 'rgba(255, 255, 255, 1.0)',
                }),
                stroke: new ol.style.Stroke({
                  color: '#319FD3',
                  width: 1
                })
              })
            })
		  });

		  map.addLayer(layer);
		  layer.setZIndex(map.getLayers().getLength());

	}
	*/



	    // source for where vector information concerning the drawn bounding box
    // will be stored
    //var boxSelectionSource = new ol.source.Vector({
    //    wrapX: false
    //});

    // layer where drawn box will reside
    // includes the vector source information, as well as style details
    /*var boxSelectionLayer = new ol.layer.Vector({

        // store source data for vectors
        source: boxSelectionSource,

        // line and fill styles
        style: new ol.style.Style({
            fill: new ol.style.Fill({
                color: 'rgba(255, 255, 255, 0.2)'
            }),
           stroke: new ol.style.Stroke({
                color: '#ffcc33',
                width: 2
            }),
            image: new ol.style.Circle({
                radius: 7,
                fill: new ol.style.Fill({
                    color: '#ffcc33'
                })
            })
        })
    }); */


    /* ======================= WFS Test Initialization ======================= */

    // var vectorSource = new ol.source.Vector({
    //    format: new ol.format.GeoJSON(),
    //    url: function(extent) {
    //      return 'https://ahocevar.com/geoserver/wfs?service=WFS&' +
    //          'version=1.1.0&request=GetFeature&typename=osm:water_areas&' +
    //          'outputFormat=application/json&srsname=EPSG:3857&' +
    //          'bbox=' + extent.join(',') + ',EPSG:3857';
    //    },
    //    strategy: ol.loadingstrategy.bbox
    //  });

    //  var vectorSource = new ol.source.Vector({
    //     format: new ol.format.GML(),
    //     url: function(extent) {
    //       return 'https://ahocevar.com/geoserver/wfs?service=WFS&' +
    //           'version=1.1.0&request=GetFeature&typename=osm:water_areas&' +
    //           'srsname=EPSG:3857&'
    //           // + 'bbox=' + extent.join(',') + ',EPSG:3857';
    //     },
    //     //strategy: ol.loadingstrategy.bbox
    //   });

    //  var vectorSource = new ol.source.Vector({
    //     format: new ol.format.GML(),
    //     url: 'http://geoint.nrlssc.navy.mil/usembassy/wfs/embassydata/feature?version=1.1.0&request=GetFeature&typename=USEmbassies',
    //     projection: 'EPSG:3857'
    //   });

      // var vectorSource = new ol.source.Vector({
      //    format: new ol.format.GML(),
      //    url: 'https://ahocevar.com/geoserver/wfs?service=WFS&outputFormat=application/json&srsname=EPSG:3857',
      //    projection: 'EPSG:3857'
      //  });

      // var vectorSource = new ol.source.Vector({
      //    format: new ol.format.GML(),
      //    url: 'http://geoint.nrlssc.navy.mil/dnc/wfs/DNC-WORLD/feature/merged?REQUEST=getFeature&typename=DNC_APPROACH_cul_buildnga&VERSION=1.1.0&SERVICE=WFS',
      //    strategy: ol.loadingstrategy.all
      //  });

      // original wfs layer

    //  var vector = new ol.layer.Vector({
    //    source: vectorSource,
    //    style: new ol.style.Style({
    //     //  Point: new ol.style.Circle({
    //     //      radius: 5,
    //     //      fill: null,
    //     //      stroke: new ol.style.Stroke({color: 'red', width: 1})
    //     //  }),
    //      stroke: new ol.style.Stroke({
    //        color: 'rgba(0, 0, 255, 1.0)',
    //        width: 2
    //      })
    //    })
    //  });

    /* =================== Demo Code to add GML WFS to Map ================= */

    // var xmlhttp = new XMLHttpRequest();
    //
    // xmlhttp.onload = function() {
    //
    //     console.log("ON LOAD");
    //     var format = new ol.format.GML3();
    //
    //     var xmlDoc = xmlhttp.responseXML;
    //
    //     var vector = new ol.layer.Vector({
    //         source: new ol.source.Vector({
    //             format: format
    //         })
    //     });
    //
    //     // Read and parse all features in XML document
    //     for (var i = 1; i < xmlDoc.children[0].children.length; i++) {
    //         var features = format.readFeatures(xmlDoc.children[0].children[i], {
    //             featureProjection: 'EPSG:4326'
    //                 //dataProjection: 'EPSG:3857'
    //         });
    //
    //         console.log(features);
    //         features.getGeometry().transform('EPSG:4326', 'EPSG:3857');
    //
    //         vector.getSource().addFeature(features);
    //     }
    //     map.addLayer(vector);
    // };
    //
    // xmlhttp.open("GET", "http://geoint.nrlssc.navy.mil/dnc/wfs/DNC-WORLD/feature/merged?version=1.1.0&request=GetFeature&typename=DNC_APPROACH_LIBRARY_BOUNDARIES&srsname=3857",
    //  true);
	// xmlhttp.send();
});
