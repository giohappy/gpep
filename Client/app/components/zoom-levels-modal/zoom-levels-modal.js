"use strict";

/**
 * zoom-levels-modal.js allows the user to set zoom level ranges on a per-level
 * basis for seed, package, and clean jobs.
 */

var MapApp = angular.module('MapApp');

MapApp.controller('ZoomLevelsModalController',
function ($scope, UtilityFactory, $uibModalInstance, ModalFactory, ServiceStore,
ServiceFactory, JobFactory, LayersGroupedByMaxZooms, $timeout ) {
    var ctrl = this;
    $scope.ServiceStore = ServiceStore

    ctrl.formData = {}
    ctrl.formData.services = ServiceStore.zoomLevelsModalServices;
    ctrl.relevantServiceLayers = ServiceStore.getSelectedServicesLayers();
    ctrl.JobFactory = JobFactory;

    // keeps track of which layer has been most recently changed
    $scope.activeServiceID = -1;
    $scope.activeLayerID = -1;
    var initFinished = false;

    // when one slider moves, other selected sliders must match values.
    // this tells the system that this update is in process, so the other
    // sliders moving to match don't cause THOSE sliders to update other sliders.
    // we don't want cascading madness of sliders triggering.
    var updateInProgress = false;

    $scope.hello = function() {
        console.log("hello");
        $scope.activeLayerID += 1;
    }

    ctrl.slider = {
      minValue: 0,
      maxValue: 3,
      options: {
        interval: 0,
        floor: 0,
        ceil: 18,
        id: "sliderA",
        hidePointerLabels: true,
        hideLimitLabels: true,
      },
    }

    var previousClickedCheckboxIndex = undefined;

    ServiceStore.synchZoomLevelModalSettings()
    initFinished = true;

    // sliders need to be rerendered after modal opens
    $scope.rerenderSliders = function() {
        $scope.$broadcast('reCalcViewDimensions');
        $scope.$broadcast('rzSliderForceRender');
    }
    setTimeout($scope.rerenderSliders, 10)
    setTimeout($scope.rerenderSliders, 100)

    // when user changes one layer, other selected layers also change
    $scope.$watch('ServiceStore.getZoomLevelModalSettings()', function(newVal, oldVal) {
        if(angular.equals(newVal, oldVal)) {
            return;
        }

        LayersGroupedByMaxZooms.updateGroupedLayers()

        if(initFinished && !updateInProgress) {
          $scope.getJustChanged(newVal, oldVal);
          var newlyChangedLayer = ServiceStore.zoomLevelModalSettings[$scope.activeServiceID][$scope.activeLayerID]
          var newMinValue = newlyChangedLayer.minValue;
          var newMaxValue = newlyChangedLayer.maxValue;

          updateInProgress = true;
          for(var serviceID in ServiceStore.zoomLevelModalSettings) {
              var service = ServiceStore.zoomLevelModalSettings[serviceID]
              for(var layerID in service) {
                  var layer = service[layerID]
                  if(layer.$isSelected) {
                      ServiceStore.zoomLevelModalSettings[serviceID][layerID].minValue = newMinValue;
                      ServiceStore.zoomLevelModalSettings[serviceID][layerID].maxValue = newMaxValue;
                  }
              }
          }
          updateInProgress = false;
        }
    }, true);

    // gets the layer and service id of the changed object, between oldVal
    // and newVal
    $scope.getJustChanged = function(newVal, oldVal) {
        for(var serviceID in newVal) {
            var service = newVal[serviceID]
            for(var layerID in service) {
                var newLayer = newVal[serviceID][layerID]
                var oldLayer = oldVal[serviceID][layerID]
                if(newLayer.minValue != oldLayer.minValue ||
                   newLayer.maxValue != oldLayer.maxValue) {
                     $scope.activeLayerID = layerID;
                     $scope.activeServiceID = serviceID;
                   }
            }
        }
    }

    // makes inputed service and layer ACTIVE, meaning the user just clicked
    // it
    $scope.setActive = function(service, layer) {
        $scope.activeLayerID = layer.id;
        $scope.activeServiceID = service.id;
    }

    // set to true to check all boxes, false to uncheck all boxes
    $scope.setAllCheckBoxes = function(trueOrFalse) {
        var settings = ServiceStore.zoomLevelModalSettings
        for(var serviceID in settings) {
            var service = settings[serviceID]
            for(var layerID in service) {
                var layer = service[layerID]
                ServiceStore.zoomLevelModalSettings[serviceID][layerID].$isSelected = trueOrFalse;
            }
        }
    }

    // return the index of the inputed layer in the flat array of layers in
    // activeZoomLevelLayers
    $scope.getIndex = function(service, layer) {
        return ServiceStore.zoomLevelModalSettings[service.id][layer.id].index
    }

    // when a layer is clicked, toggle it.
    //
    // alse, if shift is held when clicking, toggle all layers
    // between the previously clicked layer and the layer now being clicked
    $scope.onClickedLayer = function(index, $event) {
        ServiceStore.activeZoomLevelLayers[index].$isSelected = !ServiceStore.activeZoomLevelLayers[index].$isSelected

        var start, end, i, checking;
        if ($event.shiftKey && previousClickedCheckboxIndex != undefined) {
          checking = ServiceStore.activeZoomLevelLayers[index].$isSelected;
          start = previousClickedCheckboxIndex;
          end = index;
          if (start > end) {
            start = start + end;
            end = start - end;
            start = start - end;
          }
          for (i = start; i <= end; i++) {
            ServiceStore.activeZoomLevelLayers[i].$isSelected = checking
           }
        }
        previousClickedCheckboxIndex = index;
    }

    $scope.closeModal = function () {
        $uibModalInstance.dismiss('cancel');
    };
})
