'use strict';

describe('Job Factory', function() {

  var UtilityFactory;
  var CREATE_JOB_ENDPOINT;
  var GET_JOBS_ENDPOINT;
  var DELETE_JOB_ENDPOINT;
  var DOWNLOAD_FILE_ENDPOINT;
  var FILE_AVAILABLE_FOR_DOWNLOAD_ENDPOINT;
  var toastr;
  var ServiceStore;
  var JobFactory;
  var $http;

  var params;
  var data;

  beforeEach(angular.mock.module('UtilitiesApp'));
  beforeEach(angular.mock.module('toastr'));
  beforeEach(angular.mock.module('ServerApp'));

  beforeEach(inject(function(_UtilityFactory_, _CREATE_JOB_ENDPOINT_, _GET_JOBS_ENDPOINT_,
  _DELETE_JOB_ENDPOINT_, _DOWNLOAD_FILE_ENDPOINT_,
  _toastr_, _JobFactory_, _ServiceStore_, _FILE_AVAILABLE_FOR_DOWNLOAD_ENDPOINT_, _$http_) {
      UtilityFactory = _UtilityFactory_;
      CREATE_JOB_ENDPOINT = _CREATE_JOB_ENDPOINT_;
      GET_JOBS_ENDPOINT = _GET_JOBS_ENDPOINT_;
      DELETE_JOB_ENDPOINT = _DELETE_JOB_ENDPOINT_;
      DOWNLOAD_FILE_ENDPOINT = _DOWNLOAD_FILE_ENDPOINT_;
      FILE_AVAILABLE_FOR_DOWNLOAD_ENDPOINT = _FILE_AVAILABLE_FOR_DOWNLOAD_ENDPOINT_;
      toastr = _toastr_;
      JobFactory = _JobFactory_;

      // hardcodes services for purposes of unit testing.
      // we can't actually make a call to the backend in our unit test, because we
      // only want to test the frontend, not the backend too
      ServiceStore = _ServiceStore_;
      ServiceStore.services = [{
          "id": 2,
          "name": "digital_globe",
          "url": "http://localhost:8080/gpep/mapproxy/digital_globe/service",
          "layers": [{
              "id": 2,
              "name": null,
              "title": "DigitalGlobe Web Map Tile Service",
              "bbox": [-180, -90,
                  180,
                  90
              ],
              "$$hashKey": "object:93"
          },
          // FoundationGEOINT is selected!
          {
              "id": 3,
              "name": "DigitalGlobe_FoundationGEOINT",
              "title": "DigitalGlobe:FoundationGEOINT",
              "bbox": [-180, -90,
                  180,
                  90
              ],
              "$$hashKey": "object:94",
              "selected": true
          },
          // ImageInMosaicFootprint is selected!
          {
              "id": 4,
              "name": "DigitalGlobe_ImageInMosaicFootprint",
              "title": "DigitalGlobe:ImageInMosaicFootprint",
              "bbox": [-180, -90,
                  180,
                  90
              ],
              "$$hashKey": "object:95",
              "selected": true
          }, {
              "id": 5,
              "name": "DigitalGlobe_ImageStrip",
              "title": "DigitalGlobe:ImageStrip",
              "bbox": [-180, -90,
                  180,
                  90
              ],
              "$$hashKey": "object:96"
          }, {
              "id": 6,
              "name": "DigitalGlobe_ImageryTileService",
              "title": "DigitalGlobe:ImageryTileService",
              "bbox": [-180, -90,
                  180,
                  90
              ],
              "$$hashKey": "object:97"
          }, {
              "id": 7,
              "name": "DigitalGlobe_NGAOtherProducts",
              "title": "DigitalGlobe:NGAOtherProducts",
              "bbox": [-180, -90,
                  180,
                  90
              ],
              "$$hashKey": "object:98"
          }],
          "$$hashKey": "object:81"
      }, {
          "id": 3,
          "name": "north_carolina",
          "url": "http://localhost:8080/gpep/mapproxy/north_carolina/service",
          // Orthoimagery_Latest is selected!
          "layers": [{
              "id": 8,
              "name": "Orthoimagery_Latest",
              "title": "Orthoimagery_Latest",
              "bbox": [-180, -90,
                  180,
                  90
              ],
              "$$hashKey": "object:132",
              "selected": true
          }],
          "$$hashKey": "object:82"
      }]

      $http = _$http_;

      // hardcoded user input parameters for purposes of unit testing
      params = {
          "startZoomLevel": 0,
          "endZoomLevel": 3,
          "snappedNorth": 79.17133464081945,
          "snappedSouth": -40.97989806962013,
          "snappedEast": 0,
          "snappedWest": -135,
          "bboxN": 66.51326044311185,
          "bboxE": -42.1875,
          "bboxS": -11.867350911459294,
          "bboxW": -103.35937499999999
      }

      // we expected our JobFactory.startJob to call $http.post with this
      // data. we use a Jasmine spy on $http.post to check this
      data = {
          "job_type": "seed",
          "services": [{
              "id": 2,
              "layer_ids": [3,4]
          }, {
              "id": 3,
              "layer_ids": [8]
          }],
          "min_zoom": 0,
          "max_zoom": 3,
          "min_x": -135,
          "min_y": -40.97989806962013,
          "max_x": 0,
          "max_y": 79.17133464081945,
      }

      // no need to actually make any "real" http calls
      spyOn($http, "post").and.returnValue(null);
      spyOn($http, "get").and.returnValue(null);
    }));

    // NOTE the hardcoded variables ServiceStore.sercies, params, and data are
    // all tweaked from unit test to unit test, to satisify the requirements of that
    // particular test

  /**
    JobFactory.startJob has dependencies on:
        params (which gets passed to it as a parameter, includes bbox, zoom levels, etc...)
        ServiceStore.services (list of all services and layers: these show in the side-nav)
    startJob takes those and merges them into a single variable called
    "data" in preparation to be sent to the backend.

    "params", "ServiceStore.services", and "data" have all been mocked with
    pre-generated values for the purposes of this unit test.  This test ensures
    that startJob properly transforms "params" and "ServiceStore.services" into "data".
  */
  it("Ensure startJob properly prepares data for backend", function() {
      data.num_cache_processes = 2; // seeding specific parameter
      JobFactory.startJob(params, JobFactory.SEED_JOBTYPE);
      expect($http.post).toHaveBeenCalledWith(CREATE_JOB_ENDPOINT, data);

  })

  it("Ensure startJob properly prepares NSG Geodetic package parameters", function() {
      params.exportType = "NSG_4326_GEOPACKAGE";

      // if job type is "package" and export type is NSG_GEO, the package_type
      // and srs should then be set accordingly
      data.job_type = "package";
      data.package_type = "NSG_4326";
      data.srs = "EPSG:4326";
      data.fetch_tiles = true;

      JobFactory.startJob(params, JobFactory.PACKAGE_JOBTYPE);
      expect($http.post).toHaveBeenCalledWith(CREATE_JOB_ENDPOINT, data);

  })

  it("Ensure startJob properly prepares NSG Mercator parameters", function() {
      params.exportType = "NSG_3395_GEOPACKAGE";

      // if job type is "package" and export type is NSG_MERC, the package_type
      // and srs should then be set accordingly
      data.job_type = "package";
      data.package_type = "NSG_3395";
      data.srs = "EPSG:3395";
      data.fetch_tiles = true;

      JobFactory.startJob(params, JobFactory.PACKAGE_JOBTYPE);
      expect($http.post).toHaveBeenCalledWith(CREATE_JOB_ENDPOINT, data);
  })

  it("Ensure startJob properly prepares Nettwarrior parameters", function() {
      params.exportType = "NW_3857_GEOPACKAGE";

      // if job type is "package" and export type is WARRIOR, the package_type
      // and srs should then be set accordingly
      data.job_type = "package";
      data.package_type = "NW_3857";
      data.srs = "EPSG:3857";
      data.fetch_tiles = true;

      JobFactory.startJob(params, JobFactory.PACKAGE_JOBTYPE);
      expect($http.post).toHaveBeenCalledWith(CREATE_JOB_ENDPOINT, data);
  })

  // it("Ensure startJob properly prepares parameters for cleaning", function() {
  //     fail("Test not yet implemented");
  // })

  describe("boilerplate wrappers around API calls to backend", function() {
      it("getJobs", function() {
          JobFactory.getJobs();
          expect($http.get).toHaveBeenCalledWith(GET_JOBS_ENDPOINT);
      })

      it("deleteJob", function() {
          JobFactory.deleteJob(25);
          expect($http.get).toHaveBeenCalledWith(DELETE_JOB_ENDPOINT, {params: {job_id: 25}});
      })

      it("fileAvailableForDownload", function() {
          JobFactory.fileAvailableForDownload(25, "testfile.gpkg");
          expect($http.get).toHaveBeenCalledWith(FILE_AVAILABLE_FOR_DOWNLOAD_ENDPOINT, {
              params: {
                  filename: "testfile.gpkg",
                  task_id: 25
              }
          });
      })

      it("", function() {

      })
  })

  it("", function() {

  })

  it("", function() {

  })

});
