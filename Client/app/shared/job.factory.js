
  var ServerApp = angular.module('ServerApp');

  ServerApp.factory('JobFactory', function (
    $http, $rootScope, ServiceStore, $httpParamSerializer, UtilityFactory, CREATE_JOB_ENDPOINT, GET_JOBS_ENDPOINT,
    DELETE_JOB_ENDPOINT, DOWNLOAD_TASK_ENDPOINT, DOWNLOAD_JOB_ENDPOINT,
    TASK_AVAILABLE_FOR_DOWNLOAD_ENDPOINT, JOB_AVAILABLE_FOR_DOWNLOAD_ENDPOINT,
    SIZE_ESTIMATE_ENDPOINT, CANCEL_JOB_ENDPOINT, $q, ServiceFactory) {

      var o = {
        Job: Job,
        SEED_JOBTYPE: "seed",
        PACKAGE_JOBTYPE: "package",
        CLEAN_JOBTYPE: "clean",
        EXPORT_JOBTYPE: "export",
        currentSettingsPanelTab: "seed", // should match selected tab on UI
        setLevelsPerLayer: false,
        startJob: startJob,
        startExportJob: startExportJob,
        getJobs: getJobs,
        getJobDetails: getJobDetails,
        deleteJob: deleteJob,
        cancelJob: cancelJob,
        fileAvailableForDownload: fileAvailableForDownload,
        jobAvailableForDownload: jobAvailableForDownload,
        downloadFile: downloadFile,
        downloadJob: downloadJob,
        sizeEstimate: sizeEstimate,
        isJobTypeSupportedForService: isJobTypeSupportedForService,
        getSupportedServices: getSupportedServices,
        getUnsupportedSelectedServices: getUnsupportedSelectedServices,
		getModalWord: getModalWord,
		getModalWord2: getModalWord2,
		getUnsupportedReason: getUnsupportedReason,
        snappedBoundsAllLevels: [],
      };

      return o

      // pulls jobs from GPEP
      function Job(object) {
        angular.copy(object, this)
        this.created_at = new Date(this.created_at);
        this.percent = this.tasks.reduce(function(total,task){
            return total + task.progress.percent
        },0) / this.tasks.length;

        this.tasks = this.tasks.map(function(task) {
            task.started_at = new Date(task.start_date);
            if(task.progress.eta.length){
              task.progress.eta = new Date(task.progress.eta);
            }
            if (task.layers){
              task.layerNames = task.layers.map(function(layer) {return layer.name}).join(", ")
            }
            return task;
        })
      }

      function formatDate(date) {
          var d = new Date(date),
              month = '' + (d.getMonth() + 1),
              day = '' + d.getDate(),
              year = d.getFullYear();

          if (month.length < 2) month = '0' + month;
          if (day.length < 2) day = '0' + day;

          return [year, month, day].join('-');
      }

      // adds proper min/max x and y values, based on job type
      // "data" is an arbitrary object passed in
      // "params" contains snapped bounds AND actual bounds.  the proper
      // bounds will be appened and returned to the "data" object based
      // on job type
      function extendWithBbox(data, params, jobType) {
        // only used snapped bounds for packaging, other jobtypes use
        // actual bounds specified by the user
        if (jobType == o.PACKAGE_JOBTYPE) {
          data = angular.extend(data, {
            min_x: params.snappedWest,
            min_y: params.snappedSouth,
            max_x: params.snappedEast,
            max_y: params.snappedNorth,
          })
        } else {
          data = angular.extend(data, {
            min_x: params.bboxW,
            min_y: params.bboxS,
            max_x: params.bboxE,
            max_y: params.bboxN,
          })
        }

        return data;
      }

      function getProperBbox(params, jobType, zoom) {
        // only used snapped bounds for packaging, other jobtypes use
        // actual bounds specified by the user
        if (jobType == o.PACKAGE_JOBTYPE && o.setLevelsPerLayer) {
          return {
            min_x: o.snappedBoundsAllLevels[zoom].getWest(),
            min_y: o.snappedBoundsAllLevels[zoom].getSouth(),
            max_x: o.snappedBoundsAllLevels[zoom].getEast(),
            max_y: o.snappedBoundsAllLevels[zoom].getNorth(),
          }
        } else if (jobType == o.PACKAGE_JOBTYPE) {
          return {
            min_x: params.snappedWest,
            min_y: params.snappedSouth,
            max_x: params.snappedEast,
            max_y: params.snappedNorth,
          }
        } else {
          return {
          min_x: params.bboxW,
          min_y: params.bboxS,
          max_x: params.bboxE,
          max_y: params.bboxN,
          }
        }
      }

      function sizeEstimate(params, jobType){
        // get all layer IDs that are currently selected
        var data = createStartJobRequest(params, jobType)

        return $http.post(SIZE_ESTIMATE_ENDPOINT, data)
      }

      /**
        Return true if jobType is supported for specified service, false otherwise
      */
      function isJobTypeSupportedForService(jobType, service) {
        // seed jobs only supported if a service has a source to pull tiles from
        return service.has_sources || jobType != o.SEED_JOBTYPE;
      }

      function getSelectedServices() {
        return ServiceStore.services.filter(function(service){
          return ServiceStore.numLayersSelected(service) > 0
        });
      }

      /**
        Returns only services for which at least one layer is selected,
        and that are supported for the specified job type.
      */
      function getSupportedServices(jobType) {
        return getSelectedServices().filter(function(service){
          return isJobTypeSupportedForService(jobType, service);
        })
      }

      /**
        Returns selected services not supported for specified job type
      */
      function getUnsupportedSelectedServices(jobType) {
        return getSelectedServices().filter(function(service){
          return !isJobTypeSupportedForService(jobType, service);
        })
      }

	  function getModalWord(jobType){
		if(jobType == o.SEED_JOBTYPE)
		  return "seeded";
		else if (jobType == o.PACKAGE_JOBTYPE)
		  return "packaged";
		else
		  return "cleaned";
	  }

	  function getModalWord2(jobType){
		if(jobType == o.SEED_JOBTYPE)
		  return "seeding";
		else if (jobType == o.PACKAGE_JOBTYPE)
		  return "packaging";
		else
		  return "cleaning";
	  }

	  function getUnsupportedReason(jobType, service)
	  {
		if(jobType == o.SEED_JOBTYPE && !service.has_sources)
		  return "Service has no sources.";
		else if(jobType == o.CLEAN_JOBTYPE && service.cache_type == 'compact')
		  return "Not currently supported. (defrag)"
		else
		  return "Service is not supported."
	  }

      function createStartJobRequest(params, jobType) {
        // only keeps services that have at least one layer selected
        // and are supported for the specified jobType
        var services = getSupportedServices(jobType).map(function(service) {
          return {
            id: service.id,
            layers: service.layers.filter(function(layer) {
            return ServiceStore.isSelected(layer);
          }).map(function(layer) {
              var layerJSON = {
                id: layer.id
              }

              if (o.setLevelsPerLayer) {
                // for each layer object, if the Set Individual Levels toggle
                // is true on the Job Panel, then the zoom level range of each layer
                // can be unique, as specified in the zoom level modal...
                layerJSON.min_zoom = ServiceStore.zoomLevelModalSettings[service.id][layer.id].minValue
                layerJSON.max_zoom = ServiceStore.zoomLevelModalSettings[service.id][layer.id].maxValue
              } else {
                // ...else, the Set Individual Levels toggle is not set, and all layers
                // will be given the zoom levels supplied by the range slider in the
                // Job Panel
                layerJSON.min_zoom = params.startZoomLevel
                layerJSON.max_zoom = params.endZoomLevel
              }

              // add bbox parameters to the layer json.
              // currently, all layers will be coded with the same bbox.  However, the
              // backend supports different bounding boxes to be applied to different
              // layers.
              layerJSON = angular.extend(layerJSON, getProperBbox(params, jobType, layerJSON.min_zoom))
              return layerJSON;
              })
          }
        })

        var data = {
          job_type: jobType,
          services: services,
        }

        switch (jobType) {
          case o.SEED_JOBTYPE:
           data.num_cache_processes= 2;
           break;
          case o.CLEAN_JOBTYPE:
            data.date = formatDate(params.date);
            break;
          case o.PACKAGE_JOBTYPE:
            // Determine why this null check must be done. For some reason when launching a packaging job,
            // an additional factory call is made before the packaging modal is shown. Ideally, that request
            // wouldn't get to this point in the code. This updated implementation mimics the previous code flow.
            if (params.exportType)
            {
              var exportInfo = params.exportType.split('_');
              data.cache_format = exportInfo[2].toLowerCase();
              data.package_type = exportInfo[0] + '_' + exportInfo[1];
              data.srs = "EPSG:" + exportInfo[1];
            }

            data.fetch_tiles = true;
            break;
        }

        return data;
      }

      function startJob(params, jobType) {
        var data = createStartJobRequest(params, jobType);
        return $http.post(CREATE_JOB_ENDPOINT, data);
      }

      function startExportJob(map_proxy_app_id){
        return $http.post(CREATE_JOB_ENDPOINT, {
          job_type: o.EXPORT_JOBTYPE,
          services: [{
            id: map_proxy_app_id
          }],
        })
      }

      function getJobs() {
        if(!ServiceFactory.isConnectedToBackend()) return

        var deferred = $q.defer();
        $http.get(GET_JOBS_ENDPOINT).then(function(response) {
          var jobs = response.data.map(function(job){
            return new o.Job(job)
          });
          deferred.resolve(jobs)
        },function(err) {
          deferred.reject(err);
        })

        return deferred.promise
      }

      function getJobDetails(job_id) {
        var deferred = $q.defer();
        $http.get(GET_JOBS_ENDPOINT+ "/"+ job_id + "/").then(function(response){
          deferred.resolve(new o.Job(response.data))
        },function(err){
          deferred.reject(err);
        })

        return deferred.promise
      }

      function deleteJob(job_id) {
        return $http.post(DELETE_JOB_ENDPOINT, {id: job_id});
      }

      function fileAvailableForDownload(task_id, filename) {
        return $http.get(TASK_AVAILABLE_FOR_DOWNLOAD_ENDPOINT,{params: {task_id: task_id, filename: filename}})
      }

      function jobAvailableForDownload(job_id, filename) {
        return $http.get(JOB_AVAILABLE_FOR_DOWNLOAD_ENDPOINT,{params: {job_id: job_id, filename: filename}})
      }

      function downloadFile(task_id, filename) {
        var params = {task_id:task_id, filename: filename}
        var qs = $httpParamSerializer(params);
        var download_endpoint = DOWNLOAD_TASK_ENDPOINT + "?" + qs;
        var anchor = angular.element('<a/>');
        angular.element(document.body).append(anchor);
        anchor.attr({
            href: download_endpoint,
            target: '_self',
            download: true
        })[0].click();
      }

      function downloadJob(job_id, filename) {
        var params = {job_id:job_id, filename: filename}
        var qs = $httpParamSerializer(params);
        var download_endpoint = DOWNLOAD_JOB_ENDPOINT + "?" + qs;
        var anchor = angular.element('<a/>');
        angular.element(document.body).append(anchor);
        anchor.attr({
            href: download_endpoint,
            target: '_self',
            download: true
        })[0].click();
      }

      function cancelJob(job_id) {
        return $http.post(CANCEL_JOB_ENDPOINT, {'job_id': job_id});
      }
  });
