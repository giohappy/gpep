/**
    TODO DOC
 */

var ServerApp = angular.module('ServerApp');

ServerApp.factory('LayersGroupedByMaxZooms', function (ServiceStore) {

    var o = {
        /**
         Layers will be stored in this format:
         [
            {
               minZoom: 1,
               color: #qw45sdf,
               layers: [gebco layer 1, digi layer 2]
            },

            {
               minZoom: 4,
               color: #sdfasdf,
               layers: [gebco layer 2, digi layer 4]
            },

            ...
         ]
        */
        groupedLayers: [],

        /**
         Takes as input a services layers object containing only selected layers.
         Creates and stores the groupedLayers object as a result.
        */
        updateGroupedLayers: updateGroupedLayers,
    };

    // TODO DOC
    function updateGroupedLayers() {
        var tmp = {}
        ServiceStore.getSelectedServicesLayers().forEach(function(service) {
            service.layers.forEach(function(layer) {
                var minZoom = ServiceStore.zoomLevelModalSettings[service.id][layer.id].minValue;
                if(!tmp[minZoom]) {
                    tmp[minZoom] = []
                }
                tmp[minZoom].push(layer.title)
            })
        })

        var colors = palette('tol-rainbow', Object.keys(tmp).length + 1)
        o.groupedLayers = Object.keys(tmp).map(function(minZoom, i) {
            return {
                minZoom: parseInt(minZoom),
                layers:  tmp[minZoom],
                color:   '#' + colors[i]
            }
        })
    }

    return o;
  });
