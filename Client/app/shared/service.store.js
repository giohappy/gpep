/**
  service.store.js
      - retreives, adds, and deletes deletes map services from the backend via $http
      - is used to keep track of which services are selected in the side-nav

  In summary, service.store.js glues together many parts of our app, all which
  need access to the different map services and layers in those services available
  on our GPEP server.
 */

var ServerApp = angular.module('ServerApp');

ServerApp.factory('ServiceStore', function ($http, $q, ADD_SERVICE_ENDPOINT,
    SERVICE_LIST_ENDPOINT, DELETE_SERVICE_ENDPOINT, ServiceFactory) {

    var o = {
        services: [],
        // similar to openServices:  an object with key values, where the key
        // is a layer id, and its value is whether or not that layer is selected
        selectedLayersKeys: {},
        isSelected: isSelected,
        addService: addService,
        deleteService: deleteService,
        populateServices: populateServices,
        areAnyLayersSelected: areAnyLayersSelected,
        numLayersSelected: numLayersSelected,
        getSelectedLayerIDs: getSelectedLayerIDs,
        getSelectedServicesLayers: getSelectedServicesLayers,

        zoomLevelModalSettings: {},
        getZoomLevelModalSettings: getZoomLevelModalSettings,
        synchZoomLevelModalSettings: synchZoomLevelModalSettings,

        // active zoom level settings layer objects, an array.
        // this is necessary for shift clicking multiple checkboxes, as a flat
        // non-nested array of the objects is needed to more easily calculated the
        // in between checkboxes between shift clicks
        activeZoomLevelLayers: [],
    };

    return o;

    // returns true if any layers are selected at all, for all services
    function areAnyLayersSelected() {
        return Object.keys(o.selectedLayersKeys).some(function(layerKey) {
            return o.selectedLayersKeys[layerKey];
        });
    }

    // returns the number of layers selected for the given service
    function numLayersSelected(service) {
        return service.layers.filter(function(layer) {
            return o.selectedLayersKeys[layer.id];
        }).length;
    }

    // input layer object, returns if layer is selected in ui or not
    function isSelected(layer) {
        return o.selectedLayersKeys[layer.id]
    }

    function getSelectedLayerIDs() {
        return Object.keys(o.selectedLayersKeys).filter(function(layerKey) {
            return o.selectedLayersKeys[layerKey];
        });
    }

    /*
       Returns a new object, which is a replica of ServiceStore.services (keeps
       a structure containing all services and layers) but which only contains
       layers which are selected, and services which contain selected layers.

       So, if you have a ServiceStore.services structure like this:

           service 1
              layer a   SELECTED
              layer b   NOT SELECTED
              layer c   SELECTED
           service 2
              layer a   NOT SELECTED
              layer b   NOT SELECTED

        You will get an output of this:

           service 1
              layer a
              layer c
     */
    function getSelectedServicesLayers() {
        function serviceHasSelectedLayers(service) {
            for(var i=0; i < service.layers.length; i++) {
                var layer = service.layers[i]
                if(o.isSelected(layer)) {
                    return true;
                }
            }
            return false
        }

        function getSelectedLayers(service) {
            return service.layers.filter(function(layer) {
                return o.isSelected(layer);
            })
        }

        selectedServiceLayers = []
        o.services.forEach(function(service) {
            if(serviceHasSelectedLayers(service)) {
                serviceWithSelectedLayers = angular.copy(service)
                serviceWithSelectedLayers.layers = getSelectedLayers(service)
                selectedServiceLayers.push(serviceWithSelectedLayers)
            }
        })

        return selectedServiceLayers
    }

    function getZoomLevelModalSettings() {
        return o.zoomLevelModalSettings;
    }

    function synchZoomLevelModalSettings() {
        // ensures zoomLevelModalSettings is in synch with all currently available
        // services and layers in GPEP
        o.services.forEach(function(service){
            if(o.zoomLevelModalSettings[service.id] == undefined) {
                o.zoomLevelModalSettings[service.id] = {}
                service.layers.forEach(function(layer) {
                    o.zoomLevelModalSettings[service.id][layer.id] = {
                        $isSelected: false,
                        minValue: 0,
                        maxValue: 3,
                    }
                })
            }
        })

        var index = 0
        for(var serviceID in o.zoomLevelModalSettings) {
            var service = o.zoomLevelModalSettings[serviceID];
            for(var layerID in service) {
                var layer = service[layerID]
                o.zoomLevelModalSettings[serviceID][layerID].index = index;
                o.activeZoomLevelLayers.push(layer)
                index++;
            }
        }
        initFinished = true;
    }

    function addService(params) {
        var deferred = $q.defer();

        var data =  {
            name: params.service_name,
            external_url: params.service_url,
            enable_cache: params.enable_cache,
        }

        $http.post(ADD_SERVICE_ENDPOINT, data).then(function(res) {
            populateServices();
            deferred.resolve(data)

        },function(err){
            console.log("Error: ", err)
            deferred.reject(err);
        })

        return deferred.promise;
    }

    function deleteService(service){

        var deferred = $q.defer();

        data = {name: service.name};
        $http.post(DELETE_SERVICE_ENDPOINT, data).then(function(res){
            var index = o.services.indexOf(service);
            o.services.splice(index, 1)
            deferred.resolve();
        }, function(err){
            console.log("Error: ", err)
            deferred.reject(err);
        })

        return deferred.promise;
    }

    function populateServices(){
        if(!ServiceFactory.isConnectedToBackend()) return

        return $http.get(SERVICE_LIST_ENDPOINT).then(function(resp){
            var services = resp.data.map(function(service){
                service.url = MAPPROXY_PREFIX + service.url
                return service;
            })
            angular.copy(services, o.services)
        },function(err){
            console.log("Error: ", err)
        })
    }

  });
