"use strict";

/** Some utility functions
 */

    var UtilitiesApp = angular.module('UtilitiesApp', []);

    /***************************** Constants *********************************/

    // get the BASE URL just in case gpep isn't mounted at the url root
    var index = location.href.indexOf('/#');
    if (index >= 0){
        var GPEP_BASE = location.href.substring(0,index);
    }else{
        var GPEP_BASE = location.href;
    }

    var GPEP_REST_PREFIX = GPEP_BASE + '/gpep/api/';
    var MAPPROXY_PREFIX  = GPEP_BASE + '/gpep/mapproxy/';
    UtilitiesApp.constant('MAPPROXY_PREFIX', MAPPROXY_PREFIX);

    UtilitiesApp.constant("PING_ENDPOINT", GPEP_REST_PREFIX + "ping");

    UtilitiesApp.constant("SEED_SERVICE_ENDPOINT",     GPEP_REST_PREFIX + "seed_service");
    UtilitiesApp.constant("PACKAGE_SERVICE_ENDPOINT",  GPEP_REST_PREFIX + "package");
    UtilitiesApp.constant("CLEAN_SERVICE_ENDPOINT",    GPEP_REST_PREFIX + "clean_cache");
    UtilitiesApp.constant("EXPORT_SERVICE_ENDPOINT",   GPEP_REST_PREFIX + "export_service");

    // check if caching is enabled
    UtilitiesApp.constant("CACHING_ENABLED_ENDPOINT", GPEP_REST_PREFIX + "caching_enabled");
    UtilitiesApp.constant("CACHE_SIZE_ENDPOINT",      GPEP_REST_PREFIX + "cache_size");
    UtilitiesApp.constant("FREE_SPACE_ENDPOINT",      GPEP_REST_PREFIX + "get_free_space");


    // service api endpoints
    UtilitiesApp.constant("SERVICE_LIST_ENDPOINT",          GPEP_REST_PREFIX + "service_list");
    UtilitiesApp.constant("ADD_SERVICE_ENDPOINT",           GPEP_REST_PREFIX + "add_service");
    UtilitiesApp.constant("DELETE_SERVICE_ENDPOINT",        GPEP_REST_PREFIX + "delete_map_proxy_app");
    UtilitiesApp.constant("GET_JOBS_ENDPOINT",              GPEP_REST_PREFIX + "jobs");
    UtilitiesApp.constant("DELETE_JOB_ENDPOINT",            GPEP_REST_PREFIX + "delete_job");
    UtilitiesApp.constant("DOWNLOAD_TASK_ENDPOINT", GPEP_REST_PREFIX + "download_task");
    UtilitiesApp.constant("DOWNLOAD_JOB_ENDPOINT", GPEP_REST_PREFIX + "download_job");
    UtilitiesApp.constant("TASK_AVAILABLE_FOR_DOWNLOAD_ENDPOINT", GPEP_REST_PREFIX + "task_available_for_download");
    UtilitiesApp.constant("JOB_AVAILABLE_FOR_DOWNLOAD_ENDPOINT", GPEP_REST_PREFIX + "job_available_for_download");
    UtilitiesApp.constant("SIZE_ESTIMATE_ENDPOINT", GPEP_REST_PREFIX + "size_estimate");
    UtilitiesApp.constant("CANCEL_JOB_ENDPOINT", GPEP_REST_PREFIX + "cancel_job");

    UtilitiesApp.constant("NSG_GEOPACKAGE_VERSION", "V1-0");
    UtilitiesApp.constant("CREATE_JOB_ENDPOINT", GPEP_REST_PREFIX + "create_job");
    UtilitiesApp.constant("IMPORT_PEP_ENDPOINT", GPEP_REST_PREFIX + "import_pep");
    UtilitiesApp.constant("IMPORT_COMPACT_CACHE_ZIP_ENDPOINT", GPEP_REST_PREFIX + "import_compact_cache_zip");
    UtilitiesApp.constant("ADD_PACKAGE_AS_SERVICE_ENDPOINT", GPEP_REST_PREFIX + "add_geopackage_service");

    UtilitiesApp.constant("SET_BASE_LAYER_ENDPOINT", GPEP_REST_PREFIX + "set_base_layer");
    UtilitiesApp.constant("GET_DEFAULT_BASELAYER_ENDPOINT", GPEP_REST_PREFIX + "get_default_baselayer");

    UtilitiesApp.constant("CONFIG_OPTIONS_ENDPOINT", GPEP_REST_PREFIX + "get_config_options");

    UtilitiesApp.constant("GET_VECTOR_LAYER_ENDPOINT", GPEP_REST_PREFIX + "get_vector_layer");
    UtilitiesApp.constant("GEOJSON", "geojson")





    /***************************** Helper Functions ***************************/

    /**
     * Factory with basic utility functions.
     * Helpers:
          urlEncode(object) -> input object with key-value pairs representing
              parameters, and function returns a string representing the object
     */
    UtilitiesApp.factory('UtilityFactory', ['toastr', 'ModalFactory', function(toastr, ModalFactory) {
        var utilityFactory = {};

        /**
          @param input A string
          @return the input string, but with all spaces replaced with underscores
        */
        utilityFactory.spacesToUnderscores = function(input) {
            if(input) {
              return input.replace(/\s+/g,'_');
            }
        }

        utilityFactory.getWmsCapabilitiesUrl = function(serviceName) {
            return MAPPROXY_PREFIX + serviceName + '/service?REQUEST=GetCapabilities&SERVICE=WMS';
        };

        utilityFactory.getWmtsCapabilitiesUrl = function(serviceName) {
            return MAPPROXY_PREFIX + serviceName + '/service?REQUEST=GetCapabilities&SERVICE=WMTS';
        };

        /**
         * Generates an array of incremented numbers, begining at startZoomLevel and
         * ending with EndZoomLevel.
         *
         * @example Calling urlZoomLevels(3, 6) returns [3, 4, 5, 6]
         */
        utilityFactory.urlZoomLevels = function(startZoomLevel, endZoomLevel) {
            // by default values are read in as strings.  need ints for maths
            var startZoom = parseInt(startZoomLevel);
            var endZoom =   parseInt(endZoomLevel);
            // build array of levels
            // for example, if seeding levels 4 - 7, then build array [4,5,6,7]
            var levels = [];
            for (var i = startZoom; i <= endZoom; i++) {
                levels.push(i);
            }
            return levels;
        };

        /** Converts a standart javascript date object into a string formatted
         * as YYYY-MM-DD, where months and days are zero padded until they are
         * two digits long
           @example var myDate = new Date()
                    utilityFactory.dateToYMD(date) --> "2016-08-27"
         */
        utilityFactory.dateToYMDString = function(date) {
            function zeroPad(number) {
                if (number < 10) {
                    return "0" + number;
                } else {
                    return "" + number;
                }
            }

            return date.getFullYear() + '-' +
                 zeroPad(date.getMonth() + 1) + '-' +
                 zeroPad(date.getDate());
        };

        // created and return a blue toast (if toastClass not specifiefd) with an indeterminant progress.
        // spinner on it.  this toast must be cleared.  it will not dismiss itself.
        // note:  you can specify a custom class for the toast via toastClass
        utilityFactory.spinnerToastWithTitle = function(body, title, toastClass) {

            // if no value is provided for toastClass, default to a blue color
            if(toastClass == undefined) {
                toastClass = 'toast-service-adding';
            }

            var busyToast = toastr.success(body, '<i class="toast-icon fa fa-circle-o-notch fa-spin fa-2x fa-fw fa-inverse"></i>' + title, {
                iconClass: toastClass,
                allowHtml: true,
                timeOut: 0,
                tapToDismiss: false,
                extendedTimeOut: 0
            });
            return busyToast;
        };

        // created and return a blue toast with an indeterminant progress
        // spinner on it.  this toast must be cleared.  it will not dismiss itself.
        utilityFactory.spinnerToast = function(body) {
            var busyToast = toastr.success('<i class="toast-icon-body-only fa fa-circle-o-notch fa-spin fa-2x fa-fw fa-inverse"></i>' + body, {
                iconClass: 'toast-service-adding',
                allowHtml: true,
                timeOut: 0,
                tapToDismiss: false,
                extendedTimeOut: 0,
            });
            return busyToast;
        };

        // created and return a blue toast with an indeterminant progress
        // spinner on it.  this toast must be cleared.  it will not dismiss itself.
        utilityFactory.spinnerProgressBarToast = function(body, title, id) {
            var busyToast = toastr.success(body,
                '<div> <i class="toast-icon fa fa-circle-o-notch fa-spin fa-2x fa-fw fa-inverse"></i> </div>' + title, {
                iconClass: 'toast-service-adding',
                allowHtml: true,
                timeOut: 0,
                tapToDismiss: false,
                extendedTimeOut: 0,
                progressBar: true,
                toastClass: 'toast ' + id,
            })
            return busyToast;
        };

        /**
        */
        utilityFactory.setToastProgressBar = function(id, percentage) {
            var queryString = '#toast-container .' + id + ' progress-bar';
            var bar = document.querySelector(queryString);
            bar = angular.element(bar);
            bar.css('width', percentage + '%');
            bar.css('background-color', 'white');
        }

          utilityFactory.showCantConnectToBackendError = function(errorMsg) {
              ModalFactory.basicError("Cannot connect to backend.",
               "A connection with the GPEP server cannot be established.  " + errorMsg);
          }

        return utilityFactory;
    }]);


    /** The modal factory allows commonly used types of modals to easily be
        summoned with arbitrary messages

        @example
        // This displays an error message
        ModalFactory.basicError("Error: The Boogie Man Is Here", "Turn around.  It's just me.");
    */
    UtilitiesApp.factory('ModalFactory', function($uibModal) {
        var modalFactory = {};

        /** Summon a typical error modal with given title and text strings.

          @param title String message which will show in the header of the modal
          @param message String message which will show in the body of the modal
              The message parameter does except newlines "\n""
          @return the $uibModalInstance representing the newly created modal
        */
        modalFactory.basicError = function(title, err) {
            var modalInstance = $uibModal.open({
              templateUrl: 'components/map/views/basic_error_modal.html',
              controller: BasicErrorModalController,
              resolve: {
                title: function() {
                  return title;
                },
                err: function () {
                  return err;
                }
              }
            });

            return modalInstance;
        };

        var BasicErrorModalController = function(title, err, $uibModalInstance, $scope) {

            $scope.title = title;
            $scope.err = err;
            var data = err.data

            // make sure raw error gets dumped to console
            console.error(err);

            // if the error is just a string, push it into the data field of the error,
            // so the html erro modal properly displays the error
            if(typeof err == "string") {
                $scope.err = {
                    data: err
                }
                $scope.errorType = 'customString';
            } else if(err.status == -1) {
                $scope.errorType = "noConnection";
            } else if (err.status == 500) {
                // in dev mode, django dumps stack trace on python error
                // on frontend, do not show trace
                $scope.errorType = 'internal';
            // django validation error if it's a 400 error, and a regular object (not a string, not an array)
            } else if(err.status == 400 && !Array.isArray(data) && !(typeof err.data == "string")) {
                // if invalid keys are sent to backend, django specifies which
                // keys are invalid and why
                $scope.errorType = 'djangoValidation';
            } else if(err.status == "404"){
                $scope.errorType = "notFound";
            } else if(typeof err.data == "string"){
                // if request is valid (all keys and values appear to be sound)
                // but some other kind of error occurs on the backend which the
                // backend successfully catches, it will return a human-written
                // string description of the error
                $scope.errorType = 'customString';
            } else if (Array.isArray(data)) {
                $scope.errorType = 'customArray';
            } else {
                $scope.errorType = 'unknown';
                console.warn("An unknown type of error has been sent from the backend" +
                " to the frontend.  Dumping the raw error to the frontend.");
                console.warn(err);
            }

            $scope.errors = [];
            for (var key in data) {
                if (data.hasOwnProperty(key)) {
                    $scope.errors.push({field: key, error: data[key][0]})
                }
            }

            $scope.closeModal = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };

        /////////////////////  PLAIN ERROR ////////////////////////////////
        modalFactory.plainError = function(title, err) {
            var modalInstance = $uibModal.open({
              templateUrl: 'components/map/views/plain_error_modal.html',
              controller: PlainErrorModalController,
              resolve: {
                title: function() {
                  return title;
                },
                err: function () {
                  return err;
                }
              }
            });

            return modalInstance;
        };

        var PlainErrorModalController = function(title, err, $uibModalInstance, $scope) {

            $scope.title = title;
            $scope.err = err;

            $scope.closeModal = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };

        // insert more modal types here

        return modalFactory;
    });
