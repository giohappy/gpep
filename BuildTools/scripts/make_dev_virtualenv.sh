#!bin/bash
echo "Setting Variables.."
python_dir='/usr/bin/python2.7'
base="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
root="${base}/../.."
venv="${root}/.local/gpep_env"
requirements="${root}/requirements.txt"
modules="${root}/Modules"
echo "Creating Virtual Environment.."
virtualenv --python=$python_dir $venv > /dev/null 
echo "Virtual Environment created, activating.."
source "${venv}/bin/activate"  > /dev/null 
echo "Adding python dependencies.."
pip2 install -r $requirements > /dev/null 
pip2 install -e "${modules}/gpep" > /dev/null 
pip2 install -e "${root}/../gpep-mapproxy" > /dev/null
echo "Complete!"
