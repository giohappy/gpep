#!/bin/bash
mkdir -p /opt/pep/conf /opt/pep/map_cache /opt/pep/log /opt/pep/templates /opt/pep/temp /opt/pep/export /opt/pep/seed/ /opt/pep/uploads /opt/pep/apps/
cp -r BuildTools/app_config/config/* /opt/pep/conf/
cp -r BuildTools/app_config/templates/* /opt/pep/templates/

source /gpep/bin/activate
pip install -e ../gpep-mapproxy
pip install -e Modules/gpep
pip install -U pip setuptools
python install.py
pip install tzlocal  # for some reason, this package does not install with install.py, must manually pip install here
deactivate