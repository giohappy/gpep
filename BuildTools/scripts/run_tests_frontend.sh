#!/bin/bash
cd Client
LOG=$(mktemp)
npm install
bower install --allow-root
npm run test-single-run 2>&1 >$LOG
cat $LOG
if [[ $LOG == *FAILED* ]]
then
	exit -1
fi