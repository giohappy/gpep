#!/bin/bash
source /gpep/bin/activate

echo "FREEZE STARTED"
pip freeze
echo "FREEZE ENDED"

export DJANGO_SECRET_KEY='sample_key_value'

cd Modules/gpep
LOG=$(mktemp)
if python gpep_apps/jobs/management/commands/test_with_coverage.py 2>&1 >$LOG
then
    cat $LOG
else
    cat $LOG
    exit -1
fi
grep TOTAL $LOG | awk '{ print "TOTAL: "$6; }'
rm $LOG
