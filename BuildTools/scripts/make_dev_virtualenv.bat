

echo "Cleaning and Setting Variables.."
cd %~dp0
SET python_dir=C:\python27\python.exe
SET root=..\..
SET venv=%root%\local\gpep_env
SET requirements=%root%\requirements.txt
SET modules=%root%\Modules
echo "attempting to remove virtual environment"
rmdir /s /q %venv%
echo "Creating Virtual Environment.."
virtualenv --python=%python_dir% %venv%
echo "Virtual Environment created, activating.."
call %venv%\Scripts\activate.bat
echo "Adding python dependencies.."
pip install -r %requirements% >nul 2>&1
cd %modules%
pip install -e ".\gpep" >nul 2>&1
pip install -e ".\nsg-mapproxy" >nul 2>&1
echo "Complete!"

pause
