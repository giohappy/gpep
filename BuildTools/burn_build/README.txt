This build folder is where our overall final distributable installer will be built.
We use wix burn to bundle several prerequisite applications with our standalone GPEP installer..

Bundled apps
============

RabbitMQ                    (message broker)
Erlang                      (dependency for RabbitMQ)
Visual C++ for Windows      (dependency for WinPython)
Visual C++ for Python 2.7   (dependency for WinPython)
GPEP                        (standalone installer for GPEP)