# current location of script
$script_dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Definition

# actual python distribution included with the winpython
$pyexe = "$script_dir\winpython\python-2.7.12.amd64\python.exe"

$env:PYTHONHOME = "$script_dir\winpython\python-2.7.12.amd64"

& $pyexe "$script_dir\gpepsvc.py" --cleandb