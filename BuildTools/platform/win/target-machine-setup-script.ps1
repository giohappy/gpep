# this script should be triggered by the installer on the target installation machine
# it installs the Windows service for GPEP
# it sets up rabbitmq, and cleans the django database

#################### install windows service for GPEP #####################
$nssm = "C:\Program Files\GPEP\nssm.exe"
& $nssm install GPEP Powershell.exe "-executionpolicy remotesigned -File `"`"C:\Program Files\GPEP\run-gpep.ps1`"`"" | Out-Null
& $nssm set GPEP AppExit Default Exit
& $nssm set GPEP AppStopMethodSkip 1


#################### ensures no cookie mismatch for Erlang #####################
$system_cookie = "C:\Windows\.erlang.cookie"
$user_cookie = $env:HOMEDRIVE + $env:HOMEPATH + "\.erlang.cookie"

if(Compare-Object (gc $system_cookie) (gc $user_cookie)) {
    # files are different, so copy system cookie to replace user cookie
    Write-Output "Erlang cookie mismatch found.  Overriding user cookie."
    Copy-Item $system_cookie $user_cookie -force | Out-Null
} else {
    # files are the same, so do nothing
    Write-Output "Erlang cookies match. Good."
}

################## RabbitMQ Setup ##########################
# sets up a user, vhost, and sets permissions for RabbitMQ
$rabbitmqctl = "C:\Program Files\RabbitMQ Server\rabbitmq_server-3.6.6\sbin\rabbitmqctl.bat"
& "$rabbitmqctl" add_user admin admin
& "$rabbitmqctl" add_vhost gpep
& "$rabbitmqctl" set_permissions -p gpep admin ".*" ".*" ".*"

################## Python/GPEP ##########################

# current location of script
$script_dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Definition

# actual python distribution included with the winpython
$pyexe = "$script_dir\winpython\python-2.7.12.amd64\python.exe"

$env:PYTHONHOME = "$script_dir\winpython\python-2.7.12.amd64"

# cleans the django database
# assumes that gpepsvc.py is in the same directory as this powershell script
& "$pyexe" "$script_dir\gpepsvc.py" --cleandb | Out-Null

# wait a bit, make sure database fully cleans
#Start-Sleep -s 5

# now that the GPEP's databases have been cleaned, restart GPEP
Restart-Service GPEP

# give some time for the Windows service to restart
#Start-Sleep -s 5

######################### Open Browser #########################

# build GPEP url.  default 8080, unless GPEP_PORT is specified
if(Test-Path Env:\GPEP_PORT) {
    $GPEP_PORT = Get-Item Env:GPEP_PORT
} else {
    $GPEP_PORT = "8080"
}

$local_url = "https://localhost:$GPEP_PORT"
$hostname = $env:computername
$local_ip = ( Test-Connection -ComputerName (hostname) -Count 1  | Select -ExpandProperty IPV4Address).IPAddressToString
$general_url = "https://" + $local_ip + ":$GPEP_PORT"
$readme = "$script_dir\README.txt"

$readme_string = @"
Geosptial Performance Enhancing Proxy
=====================================
The GPEP Windows service is started automatically at login.
It can be stopped stopped and started via Task Manager or the Windows Service manager.

By default, the GPEP server serves over port 8080.  However, this can be changed by
setting the system environmental variable GPEP_PORT to a number, then restarting
the GPEP server (restart the GPEP Windows Service).

GPEP serves a web app through the following web address:
https://${hostname}:GPEP_PORT

From another computer, the hostname would be replaced with the ip of this computer.
The exact url to plug into a browser from another computer at the time of the installation of
GPEP would be:

$general_url

From the same computer as GPEP resides on, the URL would be:

$local_url

Note:  using http instead of https will result in the browser not being able to connect.
"@

Add-Content $readme $readme_string | Out-Null

# open a readme file once GPEP is installed
Start-Process notepad $readme