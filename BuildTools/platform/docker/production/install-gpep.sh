#!/bin/bash

# NOTE: replace <repo_path> with the path to your repo

#################################################################
# Dockerfile for building GPEP backend container
#################################################################
echo "installing node and things"
apt-get update && apt-get install -y
apt-get install python-software-properties
curl -sL https://deb.nodesource.com/setup_7.x | bash -
apt-get install -y nodejs
#Clone sources
echo "cloning repos"
git clone https://$GITLAB_USERNAME:$GITLAB_BUILD_TOKEN@<repo_path>
git clone https://$GITLAB_USERNAME:$GITLAB_BUILD_TOKEN@<repo_path>
#Build frontend and move to static dir
echo "building client code"
pushd ./pep/Client
npm install bower -g && npm install && npm run build-app-css
echo "copying client code to static"
cp -r ./app /gpep/pep/Modules/gpep/gpep_apps/static/
popd
#Checkout release branch
echo "installing pep"
pip install -r /gpep/pep/requirements.txt
pip install cherrypy #we are running on cherrypy but its not a dependency of the project, so install
pip install /gpep/pep/Modules/gpep/
#Install mapproxy and dependencies
echo "installing mapproxy"
pip install /gpep/nsg-mapproxy/

#Set up /opt/pep
echo "making pep dirs and copying files"
mkdir -p /opt/pep/apps \
    && mkdir    /opt/pep/export \
    && mkdir    /opt/pep/map_cache \
    && mkdir    /opt/pep/temp \
    && mkdir    /opt/pep/uploads \
    && mkdir    /opt/pep/conf \
    && mkdir    /opt/pep/log \
    && mkdir    /opt/pep/seed \
    && mkdir    /opt/pep/templates \
    && cp /gpep/pep/BuildTools/app_config/config/pep.cfg /opt/pep/conf/ \
    && cp /gpep/pep/BuildTools/app_config/config/dbconnfile.cfg /opt/pep/conf/ \
    && cp /gpep/pep/BuildTools/app_config/config/management_file /opt/pep/conf/ \
    && cp /gpep/pep/BuildTools/app_config/templates/default.yaml /opt/pep/templates/ 
cp /gpep/pep/BuildTools/scripts/gpepsvc.py /gpep/ 
echo "cleanup"
rm -rf /gpep/pep
rm -rf /gpep/nsg-mapproxy
npm uninstall -g bower
apt-get remove --purge -y nodejs python-software-properties
apt-get autoremove -y
echo "setting up databases.."
python gpepsvc.py -c
echo "done"
